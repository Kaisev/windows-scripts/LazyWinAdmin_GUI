# LazyWinAdmin_GUI
LazyWinAdmin is a project released in 2012, a PowerShell Script that generates a GUI/WinForms loaded with tons of functions.

The original repository is located [here](https://github.com/lazywinadmin/LazyWinAdmin_GUI).

## Usage
Download the files from the repository, or download the latest release from [kaisev.net/resources/LazyWinAdmin_Release.zip](https://kaisev.net/resources/LazyWinAdmin_Release.zip), and run LazyWinAdmin.ps1 in PowerShell.

## What's Different?

This is a fork of a [fork](https://github.com/theidiotyouyellat/LazyWinAdmin_GUI) of a [fork](https://github.com/lvkv/LazyWinAdmin_GUI) designed to be able to uninstall applications remotely based on add/remove program registry entries.

![alt text](https://kaisev.net/resources/0.5.0_Main.png "LazyWinAdmin")

This fork also contains various other improvements, [click here for a list of enhancements](https://gitlab.com/kaisev/LazyWinAdmin_GUI/issues?label_name[]=enhancement)

## Why PowerShell?

PowerShell is accessible and easy to modify. With PowerShell any administrator can modify this tool to better suit their environment in a matter of minutes.

## What systems are supported?

Windows 7 and up. You are free to attempt to use this tool on or with older versions of Windows but it is entirely untested. I have no intention of actively resolving issues that effect such systems.