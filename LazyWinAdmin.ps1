#========================================================================
# Name		: LazyWinAdmin
# Author 	: Francois-Xavier Cat
# Website	: http://lazyWinAdmin.com
# Twitter	: @LazyWinAdm
#========================================================================
[CmdletBinding()]
param (
	[Parameter()]
	[switch]
	$NoHide
)

#region To be modified by pipeline
$Version = "1.2.0"
$LastUpdate = "Unknown"
#endregion

#region Import Assemblies
[void][Reflection.Assembly]::Load("System, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")
[void][Reflection.Assembly]::Load("System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")
[void][Reflection.Assembly]::Load("System.Drawing, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")
[void][Reflection.Assembly]::Load("mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")
[void][Reflection.Assembly]::Load("System.Data, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")
#endregion Import Assemblies

function Main {
	Param ([String]$Commandline)
	#Note: This function starts the application
	#Note: $Commandline contains the complete argument string passed to the packager
	#Note: $Args contains the parsed arguments passed to the packager (Type: System.Array) 
	#Note: To get the script directory in the Packager use: Split-Path $hostinvocation.MyCommand.path
	#Note: To get the console output in the Packager (Windows Mode) use: $ConsoleOutput (Type: System.Collections.ArrayList)
	#TODO: Initialize and add Function calls to forms
	
	if(Start-MainForm_pff -eq "OK")
	{
		
	}
	
	$script:ExitCode = 0 #Set the exit code for the Packager
}


#region Call-Global_ps1
	#--------------------------------------------
	# Declare Global Variables and Functions here
	#--------------------------------------------

	#region Get-ComputerTxtBox
	function Get-ComputerTxtBox {
		$global:ComputerName = $textbox_computername.Text
	}
	#endregion
	
	#region Add-RichTextBox
	# Function - Add Text to RichTextBox
	function Add-RichTextBox {
		[CmdletBinding()]
		param ($text)
		#$richtextbox_output.Text += "`tCOMPUTERNAME: $ComputerName`n"
		$richtextbox_output.Text += "$text"
		$richtextbox_output.Text += "`n# # # # # # # # # #`n"
	}
	#Set-Alias artb Add-RichTextBox -Description "Add content to the RichTextBox"
	#endregion
	
	#region Get-DateSortable
	function Get-datesortable {
		$global:datesortable = Get-Date -Format "yyyyMMdd-HH':'mm':'ss"
		return $global:datesortable
	}
	#endregion Get-DateSortable
	
	#region Add-Logs
	function Add-Logs {
		[CmdletBinding()]
		param ($text)
		Get-datesortable
		$richtextbox_logs.Text += "[$global:datesortable] - $text`r"
		Set-Alias alogs Add-Logs -Description "Add content to the RichTextBoxLogs"
		Set-Alias Add-Log Add-Logs -Description "Add content to the RichTextBoxLogs"
	}
	#endregion Add Logs
	
	#region Clear-RichTextBox
	# Function - Clear the RichTextBox
	function Clear-RichTextBox {$richtextbox_output.Text = ""}
	#endregion
	
	#region Add-ClipBoard
	function Add-ClipBoard ($text){
			Add-Type -AssemblyName System.Windows.Forms
		    $tb = New-Object System.Windows.Forms.TextBox
		    $tb.Multiline = $true
		    $tb.Text = $text
		    $tb.SelectAll()
		    $tb.Copy()	
		}
	#endregion
	
	#region Test-TcpPort
	function Test-TcpPort ($ComputerName,[int]$port = 80) {
			$socket = new-object Net.Sockets.TcpClient
			$socket.Connect($ComputerName, $port)
			if ($socket.Connected) {
			$status = "Open"
			$socket.Close()
			}
			else {
			$status = "Closed / Filtered"
			}
			$socket = $null
			Add-RichTextBox "ComputerName:$ComputerName`nPort:$port`nStatus:$status"
	 	}
	#endregion
	
	#region Set-RDPEnable
	# Function RDP Enable
	function Set-RDPEnable ($ComputerName = '.') {
		    $regKey = [Microsoft.Win32.RegistryKey]::OpenRemoteBaseKey([Microsoft.Win32.RegistryHive]::LocalMachine, $ComputerName)
		    $regKey = $regKey.OpenSubKey("SYSTEM\\CurrentControlSet\\Control\\Terminal Server" ,$True)
		    $regkey.SetValue("fDenyTSConnections",0)
		    $regKey.flush()
		    $regKey.Close()
			}
	#endregion
	
	#region Set-RDPDisable
	# Function RDP Disable
	function Set-RDPDisable ($ComputerName = '.') {
		    $regKey = [Microsoft.Win32.RegistryKey]::OpenRemoteBaseKey([Microsoft.Win32.RegistryHive]::LocalMachine, $ComputerName)
		    $regKey = $regKey.OpenSubKey("SYSTEM\\CurrentControlSet\\Control\\Terminal Server" ,$True)
		    $regkey.SetValue("fDenyTSConnections",1)
		    $regKey.flush()
		    $regKey.Close()
			}
	#endregion
	
	#region Start-Proc
	function Start-Proc {
		param (
			[string]$exe = $(Throw "An executable must be specified"),
			[string]$arguments,
			[switch]$hidden,
			[switch]$waitforexit
		)
		# Build Startinfo and set options according to parameters
		$startinfo = new-object System.Diagnostics.ProcessStartInfo 
		$startinfo.FileName = $exe
		$startinfo.Arguments = $arguments
		if ($hidden){
			$startinfo.WindowStyle = "Hidden"
			$startinfo.CreateNoWindow = $TRUE
		}
		$process = [System.Diagnostics.Process]::Start($startinfo)
		if ($waitforexit) {$process.WaitForExit()}
	}
	#endregion
	
    #region Get-DomainUser
    function Get-DomainUser {
        param($UserName)
        $Search = [adsisearcher]"(&(objectCategory=person)(objectClass=User)(samaccountname=$UserName))"
            foreach ($user in $($Search.FindAll())){
            New-Object -TypeName PSObject -Property @{
                "DisplayName" = $user.properties.displayname
                "Email" = $user.properties.mail
            }
        }
    }
    #endregion Get-DomainUser
        
	#region Invoke-GPUpdate
	function Invoke-GPUpdate(){
		param($ComputerName = ".")
		$targetOSInfo = Get-WmiObject -ComputerName $ComputerName -Class Win32_OperatingSystem -ErrorAction SilentlyContinue
		
		If ($null -eq $targetOSInfo) {return "Unable to connect to $ComputerName"}
		Else {
			If ($targetOSInfo.version -ge 5.1){Invoke-WmiMethod -ComputerName $ComputerName -Path win32_process -Name create -ArgumentList "gpupdate /target:Computer /force /wait:0"}
			Else{Invoke-WmiMethod -ComputerName $ComputerName -Path win32_process -Name create -ArgumentList "secedit /refreshpolicy machine_policy /enforce"}
		}
	}
	#endregion
		
	#region Get-DiskPartition 
	
	function Get-DiskPartition
	{
	        
	    <#
	        .Synopsis 
	            Gets the disk partition info for specified host
	            
	        .Description
	            Gets the disk partition info for specified host
	            
	        .Parameter ComputerName
	            Name of the Computer to get the disk partition info from (Default is localhost.)
	            
	        .Example
	            Get-DiskPartition
	            Description
	            -----------
	            Gets Disk Partitions from local machine
	    
	        .Example
	            Get-DiskPartition -ComputerName MyServer
	            Description
	            -----------
	            Gets Disk Partitions from MyServer
	            
	        .Example
	            $Servers | Get-DiskPartition
	            Description
	            -----------
	            Gets Disk Partitions for each machine in the pipeline
	            
	        .OUTPUTS
	            PSObject
	            
	        .Notes
	        NAME:      Get-DiskPartition 
	        AUTHOR:    YetiCentral\bshell
	        Website:   www.bsonposh.com
	        #Requires -Version 2.0
	    #>
	    
	    [Cmdletbinding()]
	    Param(
	        [alias('dnsHostName')]
	        [Parameter(ValueFromPipelineByPropertyName=$true,ValueFromPipeline=$true)]
	        [string]$ComputerName = $Env:COMPUTERNAME
	    )
	    
	    Process 
	    {
	        Write-Verbose " [Get-DiskPartition] :: Process Start"
	        if($ComputerName -match "(.*)(\$)$")
	        {
	            $ComputerName = $ComputerName -replace "(.*)(\$)$",'$1'
	        }
	        if(Test-Host $ComputerName -TCPPort 135)
	        {
	            try
	            {
	                Write-Verbose " [Get-DiskPartition] :: Getting Partition info use WMI"
	                $Partitions = Get-WmiObject Win32_DiskPartition -ComputerName $ComputerName
	                Write-Verbose " [Get-DiskPartition] :: Found $($Partitions.Count) partitions" 
	                foreach($Partition in $Partitions)
	                {
	                    Write-Verbose " [Get-DiskPartition] :: Creating Hash Table"
	                    $myobj = @{}
	                    
	                    Write-Verbose " [Get-DiskPartition] :: Adding BlockSize        - $($Partition.BlockSize)"
	                    $myobj.BlockSize = $Partition.BlockSize
	                    
	                    Write-Verbose " [Get-DiskPartition] :: Adding BootPartition    - $($Partition.BootPartition)"
	                    $myobj.BootPartition = $Partition.BootPartition
	                    
	                    Write-Verbose " [Get-DiskPartition] :: Adding ComputerName     - $ComputerName"
	                    $myobj.ComputerName = $ComputerName
	                    
	                    Write-Verbose " [Get-DiskPartition] :: Adding Description      - $($Partition.name)"
	                    $myobj.Description = $Partition.Name
	                    
	                    Write-Verbose " [Get-DiskPartition] :: Adding PrimaryPartition - $($Partition.PrimaryPartition)"
	                    $myobj.PrimaryPartition = $Partition.PrimaryPartition
	                    
	                    Write-Verbose " [Get-DiskPartition] :: Adding Index            - $($Partition.Index)"
	                    $myobj.Index = $Partition.Index
	                    
	                    Write-Verbose " [Get-DiskPartition] :: Adding SizeMB           - $($Partition.Size)"
	                    $myobj.SizeMB = ($Partition.Size/1mb).ToString("n2",$Culture)
	                    
	                    Write-Verbose " [Get-DiskPartition] :: Adding Type             - $($Partition.Type)"
	                    $myobj.Type = $Partition.Type
	                    
	                    Write-Verbose " [Get-DiskPartition] :: Setting IsAligned "
	                    $myobj.IsAligned = $Partition.StartingOffset%64kb -eq 0
	                    
	                    Write-Verbose " [Get-DiskPartition] :: Creating Object"
	                    $obj = New-Object PSObject -Property $myobj
	                    $obj.PSTypeNames.Clear()
	                    $obj.PSTypeNames.Add('BSonPosh.DiskPartition')
	                    $obj
	                }
	            }
	            catch
	            {
	                Write-Verbose " [Get-DiskPartition] :: [$ComputerName] Failed with Error: $($Error[0])" 
	            }
	        }
	        Write-Verbose " [Get-DiskPartition] :: Process End"
	    }
	}
	    
	#endregion 
	
	#region Get-DiskSpace
	
	function Get-DiskSpace 
	{
	        
	    <#
	        .Synopsis  
	            Gets the disk space for specified host
	            
	        .Description
	            Gets the disk space for specified host
	            
	        .Parameter ComputerName
	            Name of the Computer to get the diskspace from (Default is localhost.)
	            
	        .Example
	            Get-Diskspace
	            # Gets diskspace from local machine
	    
	        .Example
	            Get-Diskspace -ComputerName MyServer
	            Description
	            -----------
	            Gets diskspace from MyServer
	            
	        .Example
	            $Servers | Get-Diskspace
	            Description
	            -----------
	            Gets diskspace for each machine in the pipeline
	            
	        .OUTPUTS
	            PSCustomObject
	            
	        .Notes
	            NAME:      Get-DiskSpace 
	            AUTHOR:    YetiCentral\bshell
	            Website:   www.bsonposh.com
	            #Requires -Version 2.0
	    #>
	    
	    [Cmdletbinding()]
	    Param(
	        [alias('dnsHostName')]
	        [Parameter(ValueFromPipelineByPropertyName=$true,ValueFromPipeline=$true)]
	        [string]$ComputerName = $Env:COMPUTERNAME
	    )
	    
	    Begin 
	    {
	        Write-Verbose " [Get-DiskSpace] :: Start Begin"
	        $Culture = New-Object System.Globalization.CultureInfo("en-US") 
	        Write-Verbose " [Get-DiskSpace] :: End Begin"
	    }
	    
	    Process 
	    {
	        Write-Verbose " [Get-DiskSpace] :: Start Process"
	        if($ComputerName -match "(.*)(\$)$")
	        {
	            $ComputerName = $ComputerName -replace "(.*)(\$)$",'$1'
	            
	        }
	        Write-Verbose " [Get-DiskSpace] :: `$ComputerName - $ComputerName"
	        Write-Verbose " [Get-DiskSpace] :: Testing Connectivity"
	        if(Test-Host $ComputerName -TCPPort 135)
	        {
	            Write-Verbose " [Get-DiskSpace] :: Connectivity Passed"
	            try
	            {
	                Write-Verbose " [Get-DiskSpace] :: Getting Operating System Version using - Get-WmiObject Win32_OperatingSystem -ComputerName $ComputerName -Property Version"
	                $OSVersionInfo = Get-WmiObject Win32_OperatingSystem -ComputerName $ComputerName -Property Version -ea STOP
	                Write-Verbose " [Get-DiskSpace] :: Getting Operating System returned $($OSVersionInfo.Version)"
	                if($OSVersionInfo.Version -gt 5.2)
	                {
	                    Write-Verbose " [Get-DiskSpace] :: Version high enough to use Win32_Volume"
	                    Write-Verbose " [Get-DiskSpace] :: Calling Get-WmiObject -class Win32_Volume -ComputerName $ComputerName -Property `"Name`",`"FreeSpace`",`"Capacity`" -filter `"DriveType=3`""
	                    $DiskInfos = Get-WmiObject -class Win32_Volume                          `
	                                            -ComputerName $ComputerName                  `
	                                            -Property "Name","FreeSpace","Capacity"      `
	                                            -filter "DriveType=3" -ea STOP
	                    Write-Verbose " [Get-DiskSpace] :: Win32_Volume returned $($DiskInfos.count) disks"
	                    foreach($DiskInfo in $DiskInfos)
	                    {
	                        $myobj = @{}
	                        $myobj.ComputerName = $ComputerName
	                        $myobj.OSVersion    = $OSVersionInfo.Version
	                        $Myobj.Drive        = $DiskInfo.Name
	                        $Myobj.CapacityGB   = [float]($DiskInfo.Capacity/1GB).ToString("n2",$Culture)
	                        $Myobj.FreeSpaceGB  = [float]($DiskInfo.FreeSpace/1GB).ToString("n2",$Culture)
	                        $Myobj.PercentFree  = "{0:P2}" -f ($DiskInfo.FreeSpace / $DiskInfo.Capacity)
	                        $obj = New-Object PSObject -Property $myobj
	                        $obj.PSTypeNames.Clear()
	                        $obj.PSTypeNames.Add('BSonPosh.DiskSpace')
	                        $obj
	                    }
	                }
	                else
	                {
	                    Write-Verbose " [Get-DiskSpace] :: Version not high enough to use Win32_Volume using Win32_LogicalDisk"
	                    $DiskInfos = Get-WmiObject -class Win32_LogicalDisk                       `
	                                            -ComputerName $ComputerName                       `
	                                            -Property SystemName, DeviceID, FreeSpace, Size   `
	                                            -filter "DriveType=3" -ea STOP
	                    foreach($DiskInfo in $DiskInfos)
	                    {
	                        $myobj = @{}
	                        $myobj.ComputerName = $ComputerName
	                        $myobj.OSVersion    = $OSVersionInfo.Version
	                        $Myobj.Drive       = "{0}\" -f $DiskInfo.DeviceID
	                        $Myobj.CapacityGB   = [float]($DiskInfo.Capacity/1GB).ToString("n2",$Culture)
	                        $Myobj.FreeSpaceGB  = [float]($DiskInfo.FreeSpace/1GB).ToString("n2",$Culture)
	                        $Myobj.PercentFree  = "{0:P2}" -f ($DiskInfo.FreeSpace / $DiskInfo.Capacity)
	                        $obj = New-Object PSObject -Property $myobj
	                        $obj.PSTypeNames.Clear()
	                        $obj.PSTypeNames.Add('BSonPosh.DiskSpace')
	                        $obj
	                    }
	                }
	            }
	            catch
	            {
	                Write-Host " Host [$ComputerName] Failed with Error: $($Error[0])" -ForegroundColor Red
	            }
	        }
	        else
	        {
	            Write-Host " Host [$ComputerName] Failed Connectivity Test " -ForegroundColor Red
	        }
	        Write-Verbose " [Get-DiskSpace] :: End Process"
	    
	    }
	}
	    
	#endregion 
	
	#region Get-Processor
	
	function Get-Processor
	{
	        
	    <#
	        .Synopsis 
	            Gets the Computer Processor info for specified host.
	            
	        .Description
	            Gets the Computer Processor info for specified host.
	            
	        .Parameter ComputerName
	            Name of the Computer to get the Computer Processor info from (Default is localhost.)
	            
	        .Example
	            Get-Processor
	            Description
	            -----------
	            Gets Computer Processor info from local machine
	    
	        .Example
	            Get-Processor -ComputerName MyServer
	            Description
	            -----------
	            Gets Computer Processor info from MyServer
	            
	        .Example
	            $Servers | Get-Processor
	            Description
	            -----------
	            Gets Computer Processor info for each machine in the pipeline
	            
	        .OUTPUTS
	            PSCustomObject
	            
	        .INPUTS
	            System.String
	            
	        .Link
	            N/A
	            
	        .Notes
	            NAME:      Get-Processor
	            AUTHOR:    bsonposh
	            Website:   http://www.bsonposh.com
	            Version:   1
	            #Requires -Version 2.0
	    #>
	    
	    [Cmdletbinding()]
	    Param(
	        [alias('dnsHostName')]
	        [Parameter(ValueFromPipelineByPropertyName=$true,ValueFromPipeline=$true)]
	        [string]$ComputerName = $Env:COMPUTERNAME
	    )
	    
	    Process 
	    {
	    
	        if($ComputerName -match "(.*)(\$)$")
	        {
	            $ComputerName = $ComputerName -replace "(.*)(\$)$",'$1'
	        }
	        if(Test-Host -ComputerName $ComputerName -TCPPort 135)
	        {
	            try
	            {
	                $CPUS = Get-WmiObject Win32_Processor -ComputerName $ComputerName -ea STOP
	                foreach($CPU in $CPUs)
	                {
	                    $myobj = @{
	                        ComputerName = $ComputerName
	                        Name         = $CPU.Name
	                        Manufacturer = $CPU.Manufacturer
	                        Speed        = $CPU.MaxClockSpeed
	                        Cores        = $CPU.NumberOfCores
	                        L2Cache      = $CPU.L2CacheSize
	                        Stepping     = $CPU.Stepping
	                    }
	                }
	                $obj = New-Object PSObject -Property $myobj
	                $obj.PSTypeNames.Clear()
	                $obj.PSTypeNames.Add('BSonPosh.Computer.Processor')
	                $obj
	            }
	            catch
	            {
	                Write-Host " Host [$ComputerName] Failed with Error: $($Error[0])" -ForegroundColor Red
	            }
	        }
	        else
	        {
	            Write-Host " Host [$ComputerName] Failed Connectivity Test " -ForegroundColor Red
	        }
	    
	    }
	}
	    
	#endregion
	
	#region Get-IP 
	
	function Get-IP
	{
	        
	    <#
	        .Synopsis 
	            Get the IP of the specified host.
	            
	        .Description
	            Get the IP of the specified host.
	            
	        .Parameter ComputerName
	            Name of the Computer to get IP (Default localhost.)
	                
	        .Example
	            Get-IP
	            Description
	            -----------
	            Get IP information the localhost
	            
	            
	        .OUTPUTS
	            PSCustomObject
	            
	        .INPUTS
	            System.String
	        
	        .Notes
	            NAME:      Get-IP
	            AUTHOR:    YetiCentral\bshell
	            Website:   www.bsonposh.com
	            #Requires -Version 2.0
	    #>
	    
	    [Cmdletbinding()]
	    Param(
	        [alias('dnsHostName')]
	        [Parameter(ValueFromPipelineByPropertyName=$true,ValueFromPipeline=$true)]
	        [string]$ComputerName = $Env:COMPUTERNAME
	    )
	    Process
	    {
	        $NICs = Get-WmiObject Win32_NetworkAdapterConfiguration -Filter "IPEnabled='$True'" -ComputerName $ComputerName
	        foreach($Nic in $NICs)
	        {
	            $myobj = @{
	                Name          = $Nic.Description
	                MacAddress    = $Nic.MACAddress
	                IP4           = $Nic.IPAddress | Where-Object{$_ -match "\d+\.\d+\.\d+\.\d+"}
	                IP6           = $Nic.IPAddress | Where-Object{$_ -match "\:\:"}
	                IP4Subnet     = $Nic.IPSubnet  | Where-Object{$_ -match "\d+\.\d+\.\d+\.\d+"}
	                DefaultGWY    = $Nic.DefaultIPGateway | Select-Object -First 1
	                DNSServer     = $Nic.DNSServerSearchOrder
	                WINSPrimary   = $Nic.WINSPrimaryServer
	                WINSSecondary = $Nic.WINSSecondaryServer
	            }
	            $obj = New-Object PSObject -Property $myobj
	            $obj.PSTypeNames.Clear()
	            $obj.PSTypeNames.Add('BSonPosh.IPInfo')
	            $obj
	        }
	    }
	}
	    
	#endregion 
	
	#region Get-InstalledSoftware
	
	function Get-InstalledSoftware
	{
	
	    <#
	        .Synopsis
	            Gets the installed software using Uninstall regkey for specified host.
	
	        .Description
	            Gets the installed software using Uninstall regkey for specified host.
	
	        .Parameter ComputerName
	            Name of the Computer to get the installed software from (Default is localhost.)
	
	        .Example
	            Get-InstalledSoftware
	            Description
	            -----------
	            Gets installed software from local machine
	
	        .Example
	            Get-InstalledSoftware -ComputerName MyServer
	            Description
	            -----------
	            Gets installed software from MyServer
	
	        .Example
	            $Servers | Get-InstalledSoftware
	            Description
	            -----------
	            Gets installed software for each machine in the pipeline
	
	        .OUTPUTS
	            PSCustomObject
	
	        .Notes
	            NAME:      Get-InstalledSoftware
	            AUTHOR:    YetiCentral\bshell
	            Website:   www.bsonposh.com
	            #Requires -Version 2.0
	    #>
	
	    [Cmdletbinding()]
	    Param(
	        [alias('dnsHostName')]
	        [Parameter(ValueFromPipelineByPropertyName=$true,ValueFromPipeline=$true)]
	        [string]$ComputerName = $Env:COMPUTERNAME
	    )
	    begin 
	    {
	
	            Write-Verbose " [Get-InstalledPrograms] :: Start Begin"
	            $Culture = New-Object System.Globalization.CultureInfo("en-US")
	            Write-Verbose " [Get-InstalledPrograms] :: End Begin"
	
	    }
	    process 
	    {
	
	        Write-Verbose " [Get-InstalledPrograms] :: Start Process"
	        if($ComputerName -match "(.*)(\$)$")
	        {
	            $ComputerName = $ComputerName -replace "(.*)(\$)$",'$1'
	
	        }
	        Write-Verbose " [Get-InstalledPrograms] :: `$ComputerName - $ComputerName"
	        Write-Verbose " [Get-InstalledPrograms] :: Testing Connectivity"
	        if(Test-Host $ComputerName -TCPPort 135)
	        {
	            try
	            {
	                $RegKey1 = Get-RegistryKey -Path "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall" -ComputerName $ComputerName
	                foreach($key1 in $RegKey1.GetSubKeyNames())   
	                {   
	                    $SubKey1 = $RegKey1.OpenSubKey($key1)
	                    if($SubKey1.GetValue("DisplayName"))
	                    {
							$Date = $null
							$RawDate = $SubKey1.GetValue("InstallDate")
							if ($RawDate) {$Date = ($RawDate -Replace '\D').Insert(4,'-').Insert(7,'-')}
	                        $myobj1 = @{
	                            Name    = $SubKey1.GetValue("DisplayName")   
								Version = $SubKey1.GetValue("DisplayVersion")   
								InstallDate = $Date
	                            Vendor  = $SubKey1.GetValue("Publisher")
								UninstallString = $SubKey1.GetValue("UninstallString")
								QuietUninstallString = $SubKey1.GetValue("QuietUninstallString")
	                        }
	                        $obj1 = New-Object PSObject -Property $myobj1
							$obj1.PSTypeNames.Clear()
	                        $obj1.PSTypeNames.Add('BSonPosh.SoftwareInfo')
	                        $obj1
	                    }
	                }  
	                $RegKey2 = Get-RegistryKey -Path "HKLM\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall" -ComputerName $ComputerName
	                foreach($key2 in $RegKey2.GetSubKeyNames())   
	                {   
	                    $SubKey2 = $RegKey2.OpenSubKey($key2)
	                    if($SubKey2.GetValue("DisplayName"))
	                    {
							$Date = $null
							$RawDate = $SubKey1.GetValue("InstallDate")
							if ($RawDate) {$Date = ($RawDate -Replace '\D').Insert(4,'-').Insert(7,'-')}
	                        $myobj2 = @{
	                            Name    = $SubKey2.GetValue("DisplayName")
								Version = $SubKey2.GetValue("DisplayVersion")
								InstallDate = $Date
	                            Vendor  = $SubKey2.GetValue("Publisher")
								UninstallString = $SubKey2.GetValue("UninstallString")
								QuietUninstallString = $SubKey2.GetValue("QuietUninstallString")
	                        }
	                        $obj2 = New-Object PSObject -Property $myobj2
	                        $obj2.PSTypeNames.Clear()
	                        $obj2.PSTypeNames.Add('BSonPosh.SoftwareInfo')
	                        $obj2
	                    }
	                }  
	            }
	            catch
	            {
	                Write-Host " Host [$ComputerName] Failed with Error: $($Error[0])" -ForegroundColor Red
	            }
	        }
	        else
	        {
	            Write-Host " Host [$ComputerName] Failed Connectivity Test " -ForegroundColor Red
	        }
	        Write-Verbose " [Get-InstalledPrograms] :: End Process"
	
	    }
	}
	
	#endregion 
	
	#region Get-RegistryHive 
	
	function Get-RegistryHive 
	{
	    param($HiveName)
	    Switch -regex ($HiveName)
	    {
	        "^(HKCR|ClassesRoot|HKEY_CLASSES_ROOT)$"               {[Microsoft.Win32.RegistryHive]"ClassesRoot";continue}
	        "^(HKCU|CurrentUser|HKEY_CURRENTt_USER)$"              {[Microsoft.Win32.RegistryHive]"CurrentUser";continue}
	        "^(HKLM|LocalMachine|HKEY_LOCAL_MACHINE)$"          {[Microsoft.Win32.RegistryHive]"LocalMachine";continue} 
	        "^(HKU|Users|HKEY_USERS)$"                          {[Microsoft.Win32.RegistryHive]"Users";continue}
	        "^(HKCC|CurrentConfig|HKEY_CURRENT_CONFIG)$"          {[Microsoft.Win32.RegistryHive]"CurrentConfig";continue}
	        "^(HKPD|PerformanceData|HKEY_PERFORMANCE_DATA)$"    {[Microsoft.Win32.RegistryHive]"PerformanceData";continue}
	        Default                                                {1;continue}
	    }
	}
	    
	#endregion 
	
	#region Get-RegistryKey 
	
	function Get-RegistryKey 
	{
	        
	    <#
	        .Synopsis 
	            Gets the registry key provide by Path.
	            
	        .Description
	            Gets the registry key provide by Path.
	                        
	        .Parameter Path 
	            Path to the key.
	            
	        .Parameter ComputerName 
	            Computer to get the registry key from.
	            
	        .Parameter Recurse 
	            Recursively returns registry keys starting from the Path.
	        
	        .Parameter ReadWrite
	            Returns the Registry key in Read Write mode.
	            
	        .Example
	            Get-registrykey HKLM\Software\Adobe
	            Description
	            -----------
	            Returns the Registry key for HKLM\Software\Adobe
	            
	        .Example
	            Get-registrykey HKLM\Software\Adobe -ComputerName MyServer1
	            Description
	            -----------
	            Returns the Registry key for HKLM\Software\Adobe on MyServer1
	        
	        .Example
	            Get-registrykey HKLM\Software\Adobe -Recurse
	            Description
	            -----------
	            Returns the Registry key for HKLM\Software\Adobe and all child keys
	                    
	        .OUTPUTS
	            Microsoft.Win32.RegistryKey
	            
	        .INPUTS
	            System.String
	            
	        .Link
	            New-RegistryKey
	            Remove-RegistryKey
	            Test-RegistryKey
	        .Notes
	            NAME:      Get-RegistryKey
	            AUTHOR:    bsonposh
	            Website:   http://www.bsonposh.com
	            Version:   1
	            #Requires -Version 2.0
	    #>
	        
	    [Cmdletbinding()]
	    Param(
	    
	        [Parameter(mandatory=$true)]
	        [string]$Path,
	        
	        [Alias("Server")]
	        [Parameter(ValueFromPipeline=$true)]
	        [string]$ComputerName = $Env:ComputerName,
	        
	        [Parameter()]
	        [switch]$Recurse,
	        
	        [Alias("RW")]
	        [Parameter()]
	        [switch]$ReadWrite
	        
	    )
	    
	    Begin 
	    {
	        
	        Write-Verbose " [Get-RegistryKey] :: Start Begin"
	        Write-Verbose " [Get-RegistryKey] :: `$Path = $Path"
	        Write-Verbose " [Get-RegistryKey] :: Getting `$Hive and `$KeyPath from $Path "
	        $PathParts = $Path -split "\\|/",0,"RegexMatch"
	        $Hive = $PathParts[0]
	        $KeyPath = $PathParts[1..$PathParts.count] -join "\"
	        Write-Verbose " [Get-RegistryKey] :: `$Hive = $Hive"
	        Write-Verbose " [Get-RegistryKey] :: `$KeyPath = $KeyPath"
	        
	        Write-Verbose " [Get-RegistryKey] :: End Begin"
	        
	    }
	    
	    Process 
	    {
	    
	        Write-Verbose " [Get-RegistryKey] :: Start Process"
	        Write-Verbose " [Get-RegistryKey] :: `$ComputerName = $ComputerName"
	        
	        $RegHive = Get-RegistryHive $hive
	        
	        if($RegHive -eq 1)
	        {
	            Write-Host "Invalid Path: $Path, Registry Hive [$hive] is invalid!" -ForegroundColor Red
	        }
	        else
	        {
	            Write-Verbose " [Get-RegistryKey] :: `$RegHive = $RegHive"
	            
	            $BaseKey = [Microsoft.Win32.RegistryKey]::OpenRemoteBaseKey($RegHive,$ComputerName)
	            Write-Verbose " [Get-RegistryKey] :: `$BaseKey = $BaseKey"
	    
	            if($ReadWrite)
	            {
	                try
	                {
	                    $Key = $BaseKey.OpenSubKey($KeyPath,$true)
	                    $Key = $Key | Add-Member -Name "ComputerName" -MemberType NoteProperty -Value $ComputerName -PassThru
	                    $Key = $Key | Add-Member -Name "Hive" -MemberType NoteProperty -Value $RegHive -PassThru 
	                    $Key = $Key | Add-Member -Name "Path" -MemberType NoteProperty -Value $KeyPath -PassThru
	                    $Key.PSTypeNames.Clear()
	                    $Key.PSTypeNames.Add('BSonPosh.Registry.Key')
	                    $Key
	                }
	                catch
	                {
	                    Write-Verbose " [Get-RegistryKey] ::  ERROR :: Unable to Open Key:$KeyPath in $KeyPath with RW Access"
	                }
	                
	            }
	            else
	            {
	                try
	                {
	                    $Key = $BaseKey.OpenSubKey("$KeyPath")
	                    if($Key)
	                    {
	                        $Key = $Key | Add-Member -Name "ComputerName" -MemberType NoteProperty -Value $ComputerName -PassThru
	                        $Key = $Key | Add-Member -Name "Hive" -MemberType NoteProperty -Value $RegHive -PassThru 
	                        $Key = $Key | Add-Member -Name "Path" -MemberType NoteProperty -Value $KeyPath -PassThru
	                        $Key.PSTypeNames.Clear()
	                        $Key.PSTypeNames.Add('BSonPosh.Registry.Key')
	                        $Key
	                    }
	                }
	                catch
	                {
	                    Write-Verbose " [Get-RegistryKey] ::  ERROR :: Unable to Open SubKey:$Name in $KeyPath"
	                }
	            }
	            
	            if($Recurse)
	            {
	                Write-Verbose " [Get-RegistryKey] :: Recurse Passed: Processing Subkeys of [$($Key.Name)]"
	                $Key
	                $SubKeyNames = $Key.GetSubKeyNames()
	                foreach($Name in $SubKeyNames)
	                {
	                    try
	                    {
	                        $SubKey = $Key.OpenSubKey($Name)
	                        if($SubKey.GetSubKeyNames())
	                        {
	                            Write-Verbose " [Get-RegistryKey] :: Calling [Get-RegistryKey] for [$($SubKey.Name)]"
	                            Get-RegistryKey -ComputerName $ComputerName -Path $SubKey.Name -Recurse
	                        }
	                        else
	                        {
	                            Get-RegistryKey -ComputerName $ComputerName -Path $SubKey.Name 
	                        }
	                    }
	                    catch
	                    {
	                        Write-Verbose " [Get-RegistryKey] ::  ERROR :: Write-Host Unable to Open SubKey:$Name in $($Key.Name)"
	                    }
	                }
	            }
	        }
	        Write-Verbose " [Get-RegistryKey] :: End Process"
	    
	    }
	}
	    
	#endregion 
	
	#region Get-RegistryValue 
	
	function Get-RegistryValue
	{
	    
	    <#
	        .Synopsis 
	            Get the value for given the registry value.
	            
	        .Description
	            Get the value for given the registry value.
	                        
	        .Parameter Path 
	            Path to the key that contains the value.
	            
	        .Parameter Name 
	            Name of the Value to check.
	            
	        .Parameter ComputerName 
	            Computer to get value.
	            
	        .Parameter Recurse 
	            Recursively gets the Values on the given key.
	            
	        .Parameter Default 
	            Returns the default value for the Value.
	        
	        .Example
	            Get-RegistryValue HKLM\SOFTWARE\Adobe\SwInstall -Name State 
	            Description
	            -----------
	            Returns value of State under HKLM\SOFTWARE\Adobe\SwInstall.
	            
	        .Example
	            Get-RegistryValue HKLM\Software\Adobe -Name State -ComputerName MyServer1
	            Description
	            -----------
	            Returns value of State under HKLM\SOFTWARE\Adobe\SwInstall on MyServer1
	            
	        .Example
	            Get-RegistryValue HKLM\Software\Adobe -Recurse
	            Description
	            -----------
	            Returns all the values under HKLM\SOFTWARE\Adobe.
	    
	        .Example
	            Get-RegistryValue HKLM\Software\Adobe -ComputerName MyServer1 -Recurse
	            Description
	            -----------
	            Returns all the values under HKLM\SOFTWARE\Adobe on MyServer1
	            
	        .Example
	            Get-RegistryValue HKLM\Software\Adobe -Default
	            Description
	            -----------
	            Returns the default value for HKLM\SOFTWARE\Adobe.
	                    
	        .OUTPUTS
	            PSCustomObject
	            
	        .INPUTS
	            System.String
	            
	        .Link
	            New-RegistryValue
	            Remove-RegistryValue
	            Test-RegistryValue
	            
	        .Notes    
	            NAME:      Get-RegistryValue
	            AUTHOR:    bsonposh
	            Website:   http://www.bsonposh.com
	            Version:   1
	            #Requires -Version 2.0
	    #>
	    
	    [Cmdletbinding()]
	    Param(
	        [Parameter(mandatory=$true)]
	        [string]$Path,
	    
	        [Parameter()]
	        [string]$Name,
	        
	        [Alias("dnsHostName")]
	        [Parameter(ValueFromPipeline=$true)]
	        [string]$ComputerName = $Env:ComputerName,
	        
	        [Parameter()]
	        [switch]$Recurse,
	        
	        [Parameter()]
	        [switch]$Default
	    )
	    
	    Process
	    {
	    
	        Write-Verbose " [Get-RegistryValue] :: Begin Process"
	        Write-Verbose " [Get-RegistryValue] :: Calling Get-RegistryKey -Path $path -ComputerName $ComputerName"
	        
	        if($Recurse)
	        {
	            $Keys = Get-RegistryKey -Path $path -ComputerName $ComputerName -Recurse
	            foreach($Key in $Keys)
	            {
	                if($Name)
	                {
	                    try
	                    {
	                        Write-Verbose " [Get-RegistryValue] :: Getting Value for [$Name]"
	                        $myobj = @{} #| Select ComputerName,Name,Value,Type,Path
	                        $myobj.ComputerName = $ComputerName
	                        $myobj.Name = $Name
	                        $myobj.value = $Key.GetValue($Name)
	                        $myobj.Type = $Key.GetValueKind($Name)
	                        $myobj.path = $Key
	                        
	                        $obj = New-Object PSCustomObject -Property $myobj
	                        $obj.PSTypeNames.Clear()
	                        $obj.PSTypeNames.Add('BSonPosh.Registry.Value')
	                        $obj
	                    }
	                    catch
	                    {
	                        Write-Verbose " [Get-RegistryValue] ::  ERROR :: Unable to Get Value for:$Name in $($Key.Name)"
	                    }
	                
	                }
	                elseif($Default)
	                {
	                    try
	                    {
	                        Write-Verbose " [Get-RegistryValue] :: Getting Value for [(Default)]"
	                        $myobj = @{} #"" | Select ComputerName,Name,Value,Type,Path
	                        $myobj.ComputerName = $ComputerName
	                        $myobj.Name = "(Default)"
	                        $myobj.value = if($Key.GetValue("")){$Key.GetValue("")}else{"EMPTY"}
	                        $myobj.Type = if($Key.GetValue("")){$Key.GetValueKind("")}else{"N/A"}
	                        $myobj.path = $Key
	                        
	                        $obj = New-Object PSCustomObject -Property $myobj
	                        $obj.PSTypeNames.Clear()
	                        $obj.PSTypeNames.Add('BSonPosh.Registry.Value')
	                        $obj
	                    }
	                    catch
	                    {
	                        Write-Verbose " [Get-RegistryValue] ::  ERROR :: Unable to Get Value for:(Default) in $($Key.Name)"
	                    }
	                }
	                else
	                {
	                    try
	                    {
	                        Write-Verbose " [Get-RegistryValue] :: Getting all Values for [$Key]"
	                        foreach($ValueName in $Key.GetValueNames())
	                        {
	                            Write-Verbose " [Get-RegistryValue] :: Getting all Value for [$ValueName]"
	                            $myobj = @{} #"" | Select ComputerName,Name,Value,Type,Path
	                            $myobj.ComputerName = $ComputerName
	                            $myobj.Name = if($ValueName -match "^$"){"(Default)"}else{$ValueName}
	                            $myobj.value = $Key.GetValue($ValueName)
	                            $myobj.Type = $Key.GetValueKind($ValueName)
	                            $myobj.path = $Key
	                            
	                            $obj = New-Object PSCustomObject -Property $myobj
	                            $obj.PSTypeNames.Clear()
	                            $obj.PSTypeNames.Add('BSonPosh.Registry.Value')
	                            $obj
	                        }
	                    }
	                    catch
	                    {
	                        Write-Verbose " [Get-RegistryValue] ::  ERROR :: Unable to Get Value for:$ValueName in $($Key.Name)"
	                    }
	                }
	            }
	        }
	        else
	        {
	            $Key = Get-RegistryKey -Path $path -ComputerName $ComputerName 
	            Write-Verbose " [Get-RegistryValue] :: Get-RegistryKey returned $Key"
	            if($Name)
	            {
	                try
	                {
	                    Write-Verbose " [Get-RegistryValue] :: Getting Value for [$Name]"
	                    $myobj = @{} # | Select ComputerName,Name,Value,Type,Path
	                    $myobj.ComputerName = $ComputerName
	                    $myobj.Name = $Name
	                    $myobj.value = $Key.GetValue($Name)
	                    $myobj.Type = $Key.GetValueKind($Name)
	                    $myobj.path = $Key
	                    
	                    $obj = New-Object PSCustomObject -Property $myobj
	                    $obj.PSTypeNames.Clear()
	                    $obj.PSTypeNames.Add('BSonPosh.Registry.Value')
	                    $obj
	                }
	                catch
	                {
	                    Write-Verbose " [Get-RegistryValue] ::  ERROR :: Unable to Get Value for:$Name in $($Key.Name)"
	                }
	            }
	            elseif($Default)
	            {
	                try
	                {
	                    Write-Verbose " [Get-RegistryValue] :: Getting Value for [(Default)]"
	                    $myobj = @{} #"" | Select ComputerName,Name,Value,Type,Path
	                    $myobj.ComputerName = $ComputerName
	                    $myobj.Name = "(Default)"
	                    $myobj.value = if($Key.GetValue("")){$Key.GetValue("")}else{"EMPTY"}
	                    $myobj.Type = if($Key.GetValue("")){$Key.GetValueKind("")}else{"N/A"}
	                    $myobj.path = $Key
	                    
	                    $obj = New-Object PSCustomObject -Property $myobj
	                    $obj.PSTypeNames.Clear()
	                    $obj.PSTypeNames.Add('BSonPosh.Registry.Value')
	                    $obj
	                }
	                catch
	                {
	                    Write-Verbose " [Get-RegistryValue] ::  ERROR :: Unable to Get Value for:$Name in $($Key.Name)"
	                }
	            }
	            else
	            {
	                Write-Verbose " [Get-RegistryValue] :: Getting all Values for [$Key]"
	                foreach($ValueName in $Key.GetValueNames())
	                {
	                    Write-Verbose " [Get-RegistryValue] :: Getting all Value for [$ValueName]"
	                    $myobj = @{} #"" | Select ComputerName,Name,Value,Type,Path
	                    $myobj.ComputerName = $ComputerName
	                    $myobj.Name = if($ValueName -match "^$"){"(Default)"}else{$ValueName}
	                    $myobj.value = $Key.GetValue($ValueName)
	                    $myobj.Type = $Key.GetValueKind($ValueName)
	                    $myobj.path = $Key
	                    
	                    $obj = New-Object PSCustomObject -Property $myobj
	                    $obj.PSTypeNames.Clear()
	                    $obj.PSTypeNames.Add('BSonPosh.Registry.Value')
	                    $obj
	                }
	            }
	        }
	        
	        Write-Verbose " [Get-RegistryValue] :: End Process"
	    
	    }
	}
	    
	#endregion  
	
	#region Get-MountPoint
	function Get-MountPoint {
	param (
	    [string]$computername = "localhost"
	)
	    Get-WmiObject -Class Win32_MountPoint -ComputerName $computername | 
	    Where-Object {$_.Directory -like 'Win32_Directory.Name="*"'} | 
	    ForEach-Object {
	        $vol = $_.Volume
	        Get-WmiObject -Class Win32_Volume -ComputerName $computername | Where-Object {$_.__RELPATH -eq $vol} | 
	        Select-Object @{Name="Folder"; Expression={$_.Caption}}, 
	        @{Name="Size (GB)"; Expression={"{0:F3}" -f $($_.Capacity / 1GB)}},
	        @{Name="Free (GB)"; Expression={"{0:F3}" -f $($_.FreeSpace / 1GB)}},
	        @{Name="%Free"; Expression={"{0:F2}" -f $(($_.FreeSpace/$_.Capacity)*100)}}|Format-Table -AutoSize
	    }
	}
	#endregion
	
	#region Get-MappedDrive
	function Get-MappedDrive {
	param (
	    [string]$computername = "localhost"
	)
	    Get-WmiObject -Class Win32_MappedLogicalDisk -ComputerName $computername | 
	    Format-List DeviceId, VolumeName, SessionID, Size, FreeSpace, ProviderName
	}
	#endregion
	
	#region Test-Host 
	
	function Test-Host
	{
	        
	    <#
	        .Synopsis 
	            Test a host for connectivity using either WMI ping or TCP port
	            
	        .Description
	            Allows you to test a host for connectivity before further processing
	            
	        .Parameter Server
	            Name of the Server to Process.
	            
	        .Parameter TCPPort
	            TCP Port to connect to. (default 135)
	            
	        .Parameter Timeout
	            Timeout for the TCP connection (default 1 sec)
	            
	        .Parameter Property
	            Name of the Property that contains the value to test.
	            
	        .Example
	            cat ServerFile.txt | Test-Host | Invoke-DoSomething
	            Description
	            -----------
	            To test a list of hosts.
	            
	        .Example
	            cat ServerFile.txt | Test-Host -tcp 80 | Invoke-DoSomething
	            Description
	            -----------
	            To test a list of hosts against port 80.
	            
	        .Example
	            Get-ADComputer | Test-Host -property dnsHostname | Invoke-DoSomething
	            Description
	            -----------
	            To test the output of Get-ADComputer using the dnshostname property
	            
	            
	        .OUTPUTS
	            System.Object
	            
	        .INPUTS
	            System.String
	            
	        .Link
	            Test-Port
	            
	        NAME:      Test-Host
	        AUTHOR:    YetiCentral\bshell
	        Website:   www.bsonposh.com
	        LASTEDIT:  02/04/2009 18:25:15
	        #Requires -Version 2.0
	    #>
	    
	    [CmdletBinding()]
	    
	    Param(
	    
	        [Parameter(ValueFromPipelineByPropertyName=$true,ValueFromPipeline=$true,Mandatory=$True)]
	        [string]$ComputerName,
	        
	        [Parameter()]
	        [int]$TCPPort=80,
	        
	        [Parameter()]
	        [int]$timeout=3000,
	        
	        [Parameter()]
	        [string]$property
	        
	    )
	    Begin 
	    {
	    
	        function PingServer 
	        {
	            Param($MyHost)
	            $ErrorActionPreference = "SilentlyContinue"
	            Write-Verbose " [PingServer] :: Pinging [$MyHost]"
	            try
	            {
	                $pingresult = Get-WmiObject win32_pingstatus -f "address='$MyHost'"
	                $ResultCode = $pingresult.statuscode
	                Write-Verbose " [PingServer] :: Ping returned $ResultCode"
	                if($ResultCode -eq 0) {$true} else {$false}
	            }
	            catch
	            {
	                Write-Verbose " [PingServer] :: Ping Failed with Error: ${error[0]}"
	                $false
	            }
	        }
	    
	    }
	    
	    Process 
	    {
	    
	        Write-Verbose " [Test-Host] :: Begin Process"
	        if($ComputerName -match "(.*)(\$)$")
	        {
	            $ComputerName = $ComputerName -replace "(.*)(\$)$",'$1'
	        }
	        Write-Verbose " [Test-Host] :: ComputerName   : $ComputerName"
	        if($TCPPort)
	        {
	            Write-Verbose " [Test-Host] :: Timeout  : $timeout"
	            Write-Verbose " [Test-Host] :: Port     : $TCPPort"
	            if($property)
	            {
	                Write-Verbose " [Test-Host] :: Property : $Property"
	                $Result = Test-Port $_.$property -tcp $TCPPort -timeout $timeout
	                if($Result)
	                {
	                    if($_){ $_ }else{ $ComputerName }
	                }
	            }
	            else
	            {
	                Write-Verbose " [Test-Host] :: Running - 'Test-Port $ComputerName -tcp $TCPPort -timeout $timeout'"
	                $Result = Test-Port $ComputerName -tcp $TCPPort -timeout $timeout
	                if($Result)
	                {
	                    if($_){ $_ }else{ $ComputerName }
	                } 
	            }
	        }
	        else
	        {
	            if($property)
	            {
	                Write-Verbose " [Test-Host] :: Property : $Property"
	                try
	                {
	                    if(PingServer $_.$property)
	                    {
	                        if($_){ $_ }else{ $ComputerName }
	                    } 
	                }
	                catch
	                {
	                    Write-Verbose " [Test-Host] :: $($_.$property) Failed Ping"
	                }
	            }
	            else
	            {
	                Write-Verbose " [Test-Host] :: Simple Ping"
	                try
	                {
	                    if(PingServer $ComputerName){$ComputerName}
	                }
	                catch
	                {
	                    Write-Verbose " [Test-Host] :: $ComputerName Failed Ping"
	                }
	            }
	        }
	        Write-Verbose " [Test-Host] :: End Process"
	    
	    }
	    
	}
	    
	#endregion 
	
	#region Test-Port 
	
	function Test-Port
	{
	        
	    <#
	        .Synopsis 
	            Test a host to see if the specified port is open.
	            
	        .Description
	            Test a host to see if the specified port is open.
	                        
	        .Parameter TCPPort 
	            Port to test (Default 135.)
	            
	        .Parameter Timeout 
	            How long to wait (in milliseconds) for the TCP connection (Default 3000.)
	            
	        .Parameter ComputerName 
	            Computer to test the port against (Default in localhost.)
	            
	        .Example
	            Test-Port -tcp 3389
	            Description
	            -----------
	            Returns $True if the localhost is listening on 3389
	            
	        .Example
	            Test-Port -tcp 3389 -ComputerName MyServer1
	            Description
	            -----------
	            Returns $True if MyServer1 is listening on 3389
	                    
	        .OUTPUTS
	            System.Boolean
	            
	        .INPUTS
	            System.String
	            
	        .Link
	            Test-Host
	            Wait-Port
	            
	        .Notes
	            NAME:      Test-Port
	            AUTHOR:    bsonposh
	            Website:   http://www.bsonposh.com
	            Version:   1
	            #Requires -Version 2.0
	    #>
	    
	    [Cmdletbinding()]
	    Param(
	        [Parameter()]
	        [int]$TCPport = 135,
	        [Parameter()]
	        [int]$TimeOut = 3000,
	        [Alias("dnsHostName")]
	        [Parameter(ValueFromPipelineByPropertyName=$true,ValueFromPipeline=$true)]
	        [String]$ComputerName = $env:COMPUTERNAME
	    )
	    Begin 
	    {
	        Write-Verbose " [Test-Port] :: Start Script"
	        Write-Verbose " [Test-Port] :: Setting Error state = 0"
	    }
	    
	    Process 
	    {
	    
	        Write-Verbose " [Test-Port] :: Creating [system.Net.Sockets.TcpClient] instance"
	        $tcpclient = New-Object system.Net.Sockets.TcpClient
	        
	        Write-Verbose " [Test-Port] :: Calling BeginConnect($ComputerName,$TCPport,$null,$null)"
	        try
	        {
	            $iar = $tcpclient.BeginConnect($ComputerName,$TCPport,$null,$null)
	            Write-Verbose " [Test-Port] :: Waiting for timeout [$timeout]"
	            $wait = $iar.AsyncWaitHandle.WaitOne($TimeOut,$false)
	        }
	        catch [System.Net.Sockets.SocketException]
	        {
	            Write-Verbose " [Test-Port] :: Exception: $($_.exception.message)"
	            Write-Verbose " [Test-Port] :: End"
	            return $false
	        }
	        catch
	        {
	            Write-Verbose " [Test-Port] :: General Exception"
	            Write-Verbose " [Test-Port] :: End"
	            return $false
	        }
	    
	        if(!$wait)
	        {
	            $tcpclient.Close()
	            Write-Verbose " [Test-Port] :: Connection Timeout"
	            Write-Verbose " [Test-Port] :: End"
	            return $false
	        }
	        else
	        {
	            Write-Verbose " [Test-Port] :: Closing TCP Socket"
	            try
	            {
	                $tcpclient.EndConnect($iar) | out-Null
	                $tcpclient.Close()
	            }
	            catch
	            {
	                Write-Verbose " [Test-Port] :: Unable to Close TCP Socket"
	            }
	            $true
	        }
	    }
	    End 
	    {
	        Write-Verbose " [Test-Port] :: End Script"
	    }
	}  
	#endregion 
	
	#region Get-MemoryConfiguration 
	
	function Get-MemoryConfiguration
	{
	        
	    <#
	        .Synopsis 
	            Gets the Memory Config for specified host.
	            
	        .Description
	            Gets the Memory Config for specified host.
	            
	        .Parameter ComputerName
	            Name of the Computer to get the Memory Config from (Default is localhost.)
	            
	        .Example
	            Get-MemoryConfiguration
	            Description
	            -----------
	            Gets Memory Config from local machine
	    
	        .Example
	            Get-MemoryConfiguration -ComputerName MyServer
	            Description
	            -----------
	            Gets Memory Config from MyServer
	            
	        .Example
	            $Servers | Get-MemoryConfiguration
	            Description
	            -----------
	            Gets Memory Config for each machine in the pipeline
	            
	        .OUTPUTS
	            PSCustomObject
	            
	        .Notes
	            NAME:      Get-MemoryConfiguration 
	            AUTHOR:    YetiCentral\bshell
	            Website:   www.bsonposh.com
	            #Requires -Version 2.0
	    #>
	    
	    [Cmdletbinding()]
	    Param(
	        [alias('dnsHostName')]
	        [Parameter(ValueFromPipelineByPropertyName=$true,ValueFromPipeline=$true)]
	        [string]$ComputerName = $Env:COMPUTERNAME
	    )
	    
	    Process 
	    {
	    
	        Write-Verbose " [Get-MemoryConfiguration] :: Begin Process"
	        if($ComputerName -match "(.*)(\$)$")
	        {
	            $ComputerName = $ComputerName -replace "(.*)(\$)$",'$1'
	        }
	        if(Test-Host $ComputerName -TCPPort 135)
	        {
	            Write-Verbose " [Get-MemoryConfiguration] :: Processing $ComputerName"
	            try
	            {
	                $MemorySlots = Get-WmiObject Win32_PhysicalMemory -ComputerName $ComputerName -ea STOP
	                foreach($Dimm in $MemorySlots)
	                {
	                    $myobj = @{}
	                    $myobj.ComputerName = $ComputerName
	                    $myobj.Description  = $Dimm.Tag
	                    $myobj.Slot         = $Dimm.DeviceLocator
	                    $myobj.Speed        = $Dimm.Speed
	                    $myobj.SizeGB       = $Dimm.Capacity/1gb
	                    
	                    $obj = New-Object PSObject -Property $myobj
	                    $obj.PSTypeNames.Clear()
	                    $obj.PSTypeNames.Add('BSonPosh.MemoryConfiguration')
	                    $obj
	                }
	            }
	            catch
	            {
	                Write-Host " Host [$ComputerName] Failed with Error: $($Error[0])" -ForegroundColor Red
	            }    
	        }
	        else
	        {
	            Write-Host " Host [$ComputerName] Failed Connectivity Test " -ForegroundColor Red
	        }
	        Write-Verbose " [Get-MemoryConfiguration] :: End Process"
	    
	    }
	}
	    
	#endregion 
	
	#region Get-NetStat 
	            #Need to Implement
	            #[Parameter()]
	            #[int]$ID,
	            #[Parameter()]
	            #[int]$RemotePort,
	            #[Parameter()]
	            #[string]$RemoteAddress,
	function Get-NetStat
	{
	
	    <#
	        .Synopsis 
	            Get the Network stats of the local host.
	            
	        .Description
	            Get the Network stats of the local host.
	            
	        .Parameter ProcessName
	            Name of the Process to get Network stats for.
	        
	        .Parameter State
	            State to return: Valid Values are: "LISTENING", "ESTABLISHED", "CLOSE_WAIT", or "TIME_WAIT"
	
	        .Parameter Interval
	            Number of times you want to run netstat. Cannot be used with Loop.
	            
	        .Parameter Sleep
	            Time between calls to netstat. Used with Interval or Loop.
	            
	        .Parameter Loop
	            Loops netstat calls until you press ctrl-c. Cannot be used with Internval.
	            
	        .Example
	            Get-NetStat
	            Description
	            -----------
	            Returns all Network stat information on the localhost
	        
	        .Example
	            Get-NetStat -ProcessName chrome
	            Description
	            -----------
	            Returns all Network stat information on the localhost for process chrome.
	            
	        .Example
	            Get-NetStat -State ESTABLISHED
	            Description
	            -----------
	            Returns all the established connections on the localhost
	            
	        .Example
	            Get-NetStat -State ESTABLISHED -Loop
	            Description
	            -----------
	            Loops established connections.
	            
	        .OUTPUTS
	            PSCustomObject
	            
	        .INPUTS
	            System.String
	        
	        .Notes
	            NAME:      Get-NetStat
	            AUTHOR:    YetiCentral\bshell
	            Website:   www.bsonposh.com
	            #Requires -Version 2.0
	
	    #>
	    
	    [Cmdletbinding(DefaultParameterSetName="All")]
	    Param(
	        [Parameter()]
	        [string]$ProcessName,
	
	        [Parameter()]
	        [ValidateSet("LISTENING", "ESTABLISHED", "CLOSE_WAIT","TIME_WAIT")]
	        [string]$State,
	        
	        [Parameter(ParameterSetName="Interval")]
	        [int]$Interval,
	        
	        [Parameter()]
	        [int]$Sleep = 1,
	        
	        [Parameter(ParameterSetName="Loop")]
	        [switch]$Loop
	    )
	
	    function Read-Netstat ($NetStat)
	    {
	        Write-Verbose " [Read-Netstat] :: Parsing Netstat results"
	        switch -regex ($NetStat)
	        {
	            $RegEx  
	            {
	                Write-Verbose " [Read-Netstat] :: creating Custom object"
	                $myobj = @{
	                    Protocol      = $matches.Protocol
	                    LocalAddress  = $matches.LocalAddress.split(":")[0]
	                    LocalPort     = $matches.LocalAddress.split(":")[1]
	                    RemoteAddress = $matches.RemoteAddress.split(":")[0]
	                    RemotePort    = $matches.RemoteAddress.split(":")[1]
	                    State         = $matches.State
	                    ProcessID     = $matches.PID
	                    ProcessName   = Get-Process -id $matches.PID -ea 0 | ForEach-Object{$_.name}
	                }
	                
	                $obj = New-Object PSCustomObject -Property $myobj
	                $obj.PSTypeNames.Clear()
	                $obj.PSTypeNames.Add('BSonPosh.NetStatInfo')
	                Write-Verbose " [Read-Netstat] :: Created object for [$($obj.LocalAddress):$($obj.LocalPort)]"
	                
	                if($ProcessName)
	                {
	                    $obj | Where-Object{$_.ProcessName -eq $ProcessName}
	                }
	                elseif($State)
	                {
	                    $obj | Where-Object{$_.State -eq $State}
	                }
	                else
	                {
	                    $obj
	                }
	                
	            }
	        }
	    }
	    
	    [RegEX]$RegEx = '\s+(?<Protocol>\S+)\s+(?<LocalAddress>\S+)\s+(?<RemoteAddress>\S+)\s+(?<State>\S+)\s+(?<PID>\S+)'
	    $Connections = @{}
	    
	    switch -exact ($pscmdlet.ParameterSetName)
	    {    
	        "All"           {
	                            Write-Verbose " [Get-NetStat] :: ParameterSet - ALL"
	                            $NetStatResults = netstat -ano | Where-Object{$_ -match "(TCP|UDP)\s+\d"}
	                            Read-Netstat $NetStatResults
	                        }
	        "Interval"      {
	                            Write-Verbose " [Get-NetStat] :: ParameterSet - Interval"
	                            for($i = 1 ; $i -le $Interval ; $i++)
	                            {
	                                Start-Sleep $Sleep
	                                $NetStatResults = netstat -ano | Where-Object{$_ -match "(TCP|UDP)\s+\d"}
	                                Read-Netstat $NetStatResults | Out-String
	                            }
	                        }
	        "Loop"          {
	                            Write-Verbose " [Get-NetStat] :: ParameterSet - Loop"
	                            Write-Host
	                            Write-Host "Protocol LocalAddress  LocalPort RemoteAddress  RemotePort State       ProcessName   PID"
	                            Write-Host "-------- ------------  --------- -------------  ---------- -----       -----------   ---" -ForegroundColor White
	                            $oldPos = $Host.UI.RawUI.CursorPosition
	                            [console]::TreatControlCAsInput = $true
	                            while($true)
	                            {
	                                Write-Verbose " [Get-NetStat] :: Getting Netstat data"
	                                $NetStatResults = netstat -ano | Where-Object{$_ -match "(TCP|UDP)\s+\d"}
	                                Write-Verbose " [Get-NetStat] :: Getting Netstat data from Netstat"
	                                $Results = Read-Netstat $NetStatResults 
	                                Write-Verbose " [Get-NetStat] :: Read-Netstat returned $($results.count) results"
	                                foreach($Result in $Results)
	                                {
	                                    $Key = $Result.LocalPort
	                                    $Value = $Result.ProcessID
	                                    $msg = "{0,-9}{1,-14}{2,-10}{3,-15}{4,-11}{5,-12}{6,-14}{7,-10}" -f  $Result.Protocol,$Result.LocalAddress,$Result.LocalPort,
	                                                                                                         $Result.RemoteAddress,$Result.RemotePort,$Result.State,
	                                                                                                         $Result.ProcessName,$Result.ProcessID
	                                    if($Connections.$Key -eq $Value)
	                                    {
	                                        Write-Host $msg
	                                    }
	                                    else
	                                    {
	                                        $Connections.$Key = $Value
	                                        Write-Host $msg -ForegroundColor Yellow
	                                    }
	                                }
	                                if ($Host.UI.RawUI.KeyAvailable -and (3 -eq [int]$Host.UI.RawUI.ReadKey("AllowCtrlC,IncludeKeyUp,NoEcho").Character))
	                                {
	                                    Write-Host "Exiting now..." -foregroundcolor Yellow
	                                    Write-Host
	                                    [console]::TreatControlCAsInput = $false
	                                    break
	                                }
	                                $Host.UI.RawUI.CursorPosition = $oldPos
	                                start-sleep $Sleep
	                            }
	                        }
	    }
	}
	
	#endregion 
	
	#region Get-NicInfo 
	
	function Get-NICInfo
	{
	
	    <#
	        .Synopsis  
	            Gets the NIC info for specified host
	            
	        .Description
	            Gets the NIC info for specified host
	            
	        .Parameter ComputerName
	            Name of the Computer to get the NIC info from (Default is localhost.)
	            
	        .Example
	            Get-NicInfo
	            # Gets NIC info from local machine
	    
	        .Example
	            Get-NicInfo -ComputerName MyServer
	            Description
	            -----------
	            Gets NIC info from MyServer
	            
	        .Example
	            $Servers | Get-NicInfo
	            Description
	            -----------
	            Gets NIC info for each machine in the pipeline
	            
	        .OUTPUTS
	            PSCustomObject
	            
	        .Notes
	            NAME:      Get-NicInfo 
	            AUTHOR:    YetiCentral\bshell
	            Website:   www.bsonposh.com
	            #Requires -Version 2.0
	    #>
	    
		[Cmdletbinding()]
		Param(
		    [alias('dnsHostName')]
			[Parameter(ValueFromPipelineByPropertyName=$true,ValueFromPipeline=$true)]
			[string]$ComputerName = $Env:COMPUTERNAME
		)
	
		Process
		{
			if($ComputerName -match "(.*)(\$)$")
			{
				$ComputerName = $ComputerName -replace "(.*)(\$)$",'$1'
			}
			
			if(Test-Host -ComputerName $ComputerName -TCPPort 135)
			{
				try
				{
					$NICS = Get-WmiObject -class Win32_NetworkAdapterConfiguration -ComputerName $ComputerName
					
					foreach($NIC in $NICS)
					{
						$Query = "Select Name,NetConnectionID FROM Win32_NetworkAdapter WHERE Index='$($NIC.Index)'"
						$NetConnnectionID = Get-WmiObject -Query $Query -ComputerName $ComputerName
						
						$myobj = @{
	                        ComputerName = $ComputerName
							Name         = $NetConnnectionID.Name
							NetID        = $NetConnnectionID.NetConnectionID
							MacAddress   = $NIC.MacAddress
							IP           = $NIC.IPAddress | Where-Object{$_ -match "\d*\.\d*\.\d*\."}
							Subnet       = $NIC.IPSubnet  | Where-Object{$_ -match "\d*\.\d*\.\d*\."}
							Enabled      = $NIC.IPEnabled
							Index        = $NIC.Index
						}
						
						$obj = New-Object PSObject -Property $myobj
						$obj.PSTypeNames.Clear()
						$obj.PSTypeNames.Add('BSonPosh.NICInfo')
						$obj
					}
				}
				catch
				{
					Write-Host " Host [$ComputerName] Failed with Error: $($Error[0])" -ForegroundColor Red
				}
			}
			else
			{
				Write-Host " Host [$ComputerName] Failed Connectivity Test " -ForegroundColor Red
			}
		}
	} 
	
	#endregion 
	
	#region Get-MotherBoard
	
	function Get-MotherBoard
	{
	        
	    <#
	        .Synopsis 
	            Gets the Mother Board info for specified host.
	            
	        .Description
	            Gets the Mother Board info for specified host.
	            
	        .Parameter ComputerName
	            Name of the Computer to get the Mother Board info from (Default is localhost.) 
	            
	        .Example
	            Get-MotherBoard
	            Description
	            -----------
	            Gets Mother Board info from local machine
	    
	        .Example
	            Get-MotherBoard -ComputerName MyOtherDesktop
	            Description
	            -----------
	            Gets Mother Board info from MyOtherDesktop
	            
	        .Example
	            $Windows7Machines | Get-MotherBoard
	            Description
	            -----------
	            Gets Mother Board info for each machine in the pipeline
	            
	        .OUTPUTS
	            PSCustomObject
	            
	        .INPUTS
	            System.String
	            
	        .Link
	            N/A
	            
	        .Notes
	            NAME:      Get-MotherBoard
	            AUTHOR:    bsonposh
	            Website:   http://www.bsonposh.com
	            Version:   1
	            #Requires -Version 2.0
	    #>
	    
	    [Cmdletbinding()]
	    Param(
	        [alias('dnsHostName')]
	        [Parameter(ValueFromPipelineByPropertyName=$true,ValueFromPipeline=$true)]
	        [string]$ComputerName = $Env:COMPUTERNAME
	    )
	    
	    Process 
	    {
	    
	        if($ComputerName -match "(.*)(\$)$")
	        {
	            $ComputerName = $ComputerName -replace "(.*)(\$)$",'$1'
	        }
	        if(Test-Host -ComputerName $ComputerName -TCPPort 135)
	        {
	            try
	            {
	                $MBInfo = Get-WmiObject Win32_BaseBoard -ComputerName $ComputerName -ea STOP
	                $myobj = @{
	                    ComputerName     = $ComputerName
	                    Name             = $MBInfo.Product
	                    Manufacturer     = $MBInfo.Manufacturer
	                    Version          = $MBInfo.Version
	                    SerialNumber     = $MBInfo.SerialNumber
	                 }
	                
	                $obj = New-Object PSObject -Property $myobj
	                $obj.PSTypeNames.Clear()
	                $obj.PSTypeNames.Add('BSonPosh.Computer.MotherBoard')
	                $obj
	            }
	            catch
	            {
	                Write-Host " Host [$ComputerName] Failed with Error: $($Error[0])" -ForegroundColor Red
	            }
	        }
	        else
	        {
	            Write-Host " Host [$ComputerName] Failed Connectivity Test " -ForegroundColor Red
	        }
	    
	    }
	}
	    
	#endregion # Get-MotherBoard
	
	#region Get-Routetable 
	
	function Get-Routetable
	{
	    
	    <#
	        .Synopsis 
	            Gets the route table for specified host.
	            
	        .Description
	            Gets the route table for specified host.
	            
	        .Parameter ComputerName
	            Name of the Computer to get the route table from (Default is localhost.)
	            
	        .Example
	            Get-RouteTable
	            Description
	            -----------
	            Gets route table from local machine
	    
	        .Example
	            Get-RouteTable -ComputerName MyServer
	            Description
	            -----------
	            Gets route table from MyServer
	            
	        .Example
	            $Servers | Get-RouteTable
	            Description
	            -----------
	            Gets route table for each machine in the pipeline
	            
	        .OUTPUTS
	            PSCustomObject
	            
	        .INPUTS
	            System.String
	            
	        .Link
	            N/A
	            
	        .Notes
	            NAME:      Get-RouteTable
	            AUTHOR:    bsonposh
	            Website:   http://www.bsonposh.com
	            Version:   1
	            #Requires -Version 2.0
	    #>
	    
	    [Cmdletbinding()]
	    Param(
	        [alias('dnsHostName')]
	        [Parameter(ValueFromPipelineByPropertyName=$true,ValueFromPipeline=$true)]
	        [string]$ComputerName = $Env:COMPUTERNAME
	    )
	    process 
	    {
	    
	        if($ComputerName -match "(.*)(\$)$")
	        {
	            $ComputerName = $ComputerName -replace "(.*)(\$)$",'$1'
	        }
	        if(Test-Host $ComputerName -TCPPort 135)
	        {
	            $Routes = Get-WMIObject Win32_IP4RouteTable -ComputerName $ComputerName -Property Name,Mask,NextHop,Metric1,Type
	            foreach($Route in $Routes)
	            {
	                $myobj = @{}
	                $myobj.ComputerName = $ComputerName
	                $myobj.Name = $Route.Name
	                $myobj.NetworkMask = $Route.mask
	                $myobj.Gateway = if($Route.NextHop -eq "0.0.0.0"){"On-Link"}else{$Route.NextHop}
	                $myobj.Metric = $Route.Metric1
	                
	                $obj = New-Object PSObject -Property $myobj
	                $obj.PSTypeNames.Clear()
	                $obj.PSTypeNames.Add('BSonPosh.RouteTable')
	                $obj
	            }
	        }
	        else
	        {
	            Write-Host " Host [$ComputerName] Failed Connectivity Test " -ForegroundColor Red
	        }
	    
	    }
	}
	    
	#endregion 
	
	#region Get-SystemType 
	
	function Get-SystemType
	{
	        
	    <#
	        .Synopsis 
	            Gets the system type for specified host
	            
	        .Description
	            Gets the system type info for specified host
	            
	        .Parameter ComputerName
	            Name of the Computer to get the System Type from (Default is localhost.)
	            
	        .Example
	            Get-SystemType
	            Description
	            -----------
	            Gets System Type from local machine
	    
	        .Example
	            Get-SystemType -ComputerName MyServer
	            Description
	            -----------
	            Gets System Type from MyServer
	            
	        .Example
	            $Servers | Get-SystemType
	            Description
	            -----------
	            Gets System Type for each machine in the pipeline
	            
	        .OUTPUTS
	            PSObject
	            
	        .Notes
	            NAME:      Get-SystemType 
	            AUTHOR:    YetiCentral\bshell
	            Website:   www.bsonposh.com
	            #Requires -Version 2.0
	    #>
	    
	    [Cmdletbinding()]
	    Param(
	        [alias('dnsHostName')]
	        [Parameter(ValueFromPipelineByPropertyName=$true,ValueFromPipeline=$true)]
	        [string]$ComputerName = $Env:COMPUTERNAME
	    )
	    
	    Begin 
	    {
	    
	        function ConvertTo-ChassisType($Type)
	        {
	            switch ($Type)
	            {
	                1    {"Other"}
	                2    {"Unknown"}
	                3    {"Desktop"}
	                4    {"Low Profile Desktop"}
	                5    {"Pizza Box"}
	                6    {"Mini Tower"}
	                7    {"Tower"}
	                8    {"Portable"}
	                9    {"Laptop"}
	                10    {"Notebook"}
	                11    {"Hand Held"}
	                12    {"Docking Station"}
	                13    {"All in One"}
	                14    {"Sub Notebook"}
	                15    {"Space-Saving"}
	                16    {"Lunch Box"}
	                17    {"Main System Chassis"}
	                18    {"Expansion Chassis"}
	                19    {"SubChassis"}
	                20    {"Bus Expansion Chassis"}
	                21    {"Peripheral Chassis"}
	                22    {"Storage Chassis"}
	                23    {"Rack Mount Chassis"}
	                24    {"Sealed-Case PC"}
	            }
	        }
	        function ConvertTo-SecurityStatus($Status)
	        {
	            switch ($Status)
	            {
	                1    {"Other"}
	                2    {"Unknown"}
	                3    {"None"}
	                4    {"External Interface Locked Out"}
	                5    {"External Interface Enabled"}
	            }
	        }
	    
	    }
	    Process 
	    {
	    
	        Write-Verbose " [Get-SystemType] :: Process Start"
	        if($ComputerName -match "(.*)(\$)$")
	        {
	            $ComputerName = $ComputerName -replace "(.*)(\$)$",'$1'
	        }
	        if(Test-Host $ComputerName -TCPPort 135)
	        {
	            try
	            {
	                Write-Verbose " [Get-SystemType] :: Getting System (Enclosure) Type info use WMI"
	                $SystemInfo = Get-WmiObject Win32_SystemEnclosure -ComputerName $ComputerName
	                $CSInfo = Get-WmiObject -Query "Select Model FROM Win32_ComputerSystem" -ComputerName $ComputerName
	                
	                Write-Verbose " [Get-SystemType] :: Creating Hash Table"
	                $myobj = @{}
	                Write-Verbose " [Get-SystemType] :: Setting ComputerName   - $ComputerName"
	                $myobj.ComputerName = $ComputerName
	                
	                Write-Verbose " [Get-SystemType] :: Setting Manufacturer   - $($SystemInfo.Manufacturer)"
	                $myobj.Manufacturer = $SystemInfo.Manufacturer
	                
	                Write-Verbose " [Get-SystemType] :: Setting Module   - $($CSInfo.Model)"
	                $myobj.Model = $CSInfo.Model
	                
	                Write-Verbose " [Get-SystemType] :: Setting SerialNumber   - $($SystemInfo.SerialNumber)"
	                $myobj.SerialNumber = $SystemInfo.SerialNumber
	                
	                Write-Verbose " [Get-SystemType] :: Setting SecurityStatus - $($SystemInfo.SecurityStatus)"
	                $myobj.SecurityStatus = ConvertTo-SecurityStatus $SystemInfo.SecurityStatus
	                
	                Write-Verbose " [Get-SystemType] :: Setting Type           - $($SystemInfo.ChassisTypes)"
	                $myobj.Type = ConvertTo-ChassisType $SystemInfo.ChassisTypes
	                
	                Write-Verbose " [Get-SystemType] :: Creating Custom Object"
	                $obj = New-Object PSCustomObject -Property $myobj
	                $obj.PSTypeNames.Clear()
	                $obj.PSTypeNames.Add('BSonPosh.SystemType')
	                $obj
	            }
	            catch
	            {
	                Write-Verbose " [Get-SystemType] :: [$ComputerName] Failed with Error: $($Error[0])" 
	            }
	        }
	    
	    }
	    
	}
	    
	#endregion 
	
	#region Get-RebootTime 
	
	function Get-RebootTime
	{
	    <#
	        .Synopsis 
	            Gets the reboot time for specified host.
	            
	        .Description
	            Gets the reboot time for specified host.
	            
	        .Parameter ComputerName
	            Name of the Computer to get the reboot time from (Default is localhost.)
	            
	        .Example
	            Get-RebootTime
	            Description
	            -----------
	            Gets OS Version from local     
	        
	        .Example
	            Get-RebootTime -Last
	            Description
	            -----------
	            Gets last reboot time from local machine
	
	        .Example
	            Get-RebootTime -ComputerName MyServer
	            Description
	            -----------
	            Gets reboot time from MyServer
	            
	        .Example
	            $Servers | Get-RebootTime
	            Description
	            -----------
	            Gets reboot time for each machine in the pipeline
	            
	        .OUTPUTS
	            PSCustomObject
	            
	        .INPUTS
	            System.String
	            
	        .Link
	            N/A
	            
	        .Notes
	            NAME:      Get-RebootTime
	            AUTHOR:    bsonposh
	            Website:   http://www.bsonposh.com
	            Version:   1
	            #Requires -Version 2.0
	    #>
	    
	    [cmdletbinding()]
	    Param(
	        [alias('dnsHostName')]
	        [Parameter(ValueFromPipelineByPropertyName=$true,ValueFromPipeline=$true)]
	        [string]$ComputerName = $Env:COMPUTERNAME,
	        
	        [Parameter()]
	        [Switch]$Last
	    )
	    process 
	    {
	    
	        if($ComputerName -match "(.*)(\$)$")
	        {
	            $ComputerName = $ComputerName -replace "(.*)(\$)$",'$1'
	        }
	        if(Test-Host $ComputerName -TCPPort 135)
	        {
	            try
	            {
	                if($Last)
	                {
	                    $date = Get-WmiObject Win32_OperatingSystem -ComputerName $ComputerName -ea STOP | ForEach-Object{$_.LastBootUpTime}
	                    $RebootTime = [System.DateTime]::ParseExact($date.split('.')[0],'yyyyMMddHHmmss',$null)
	                    $myobj = @{}
	                    $myobj.ComputerName = $ComputerName
	                    $myobj.RebootTime = $RebootTime
	                    
	                    $obj = New-Object PSObject -Property $myobj
	                    $obj.PSTypeNames.Clear()
	                    $obj.PSTypeNames.Add('BSonPosh.RebootTime')
	                    $obj
	                }
	                else
	                {
	                    $Query = "Select * FROM Win32_NTLogEvent WHERE SourceName='eventlog' AND EventCode='6009'"
	                    Get-WmiObject -Query $Query -ea 0 -ComputerName $ComputerName | ForEach-Object {
	                        $myobj = @{}
	                        $RebootTime = [DateTime]::ParseExact($_.TimeGenerated.Split(".")[0],'yyyyMMddHHmmss',$null)
	                        $myobj.ComputerName = $ComputerName
	                        $myobj.RebootTime = $RebootTime
	                        
	                        $obj = New-Object PSObject -Property $myobj
	                        $obj.PSTypeNames.Clear()
	                        $obj.PSTypeNames.Add('BSonPosh.RebootTime')
	                        $obj
	                    }
	                }
	    
	            }
	            catch
	            {
	                Write-Host " Host [$ComputerName] Failed with Error: $($Error[0])" -ForegroundColor Red
	            }
	        }
	        else
	        {
	            Write-Host " Host [$ComputerName] Failed Connectivity Test " -ForegroundColor Red
	        }
	    
	    }
	}
	    
	#endregion 
	
	### BSONPOSH - KMS
	
	#region ConvertTo-KMSStatus 
	
	function ConvertTo-KMSStatus
	{
		[cmdletbinding()]
		Param(
			[Parameter(mandatory=$true)]
			[int]$StatusCode
		)
		switch -exact ($StatusCode)
		{
			0		{"Unlicensed"}
			1		{"Licensed"}
			2		{"OOBGrace"}
			3		{"OOTGrace"}
			4		{"NonGenuineGrace"}
			5		{"Notification"}
			6		{"ExtendedGrace"}
			default {"Unknown"}
		}
	}
	#endregion 
	
	#region Get-KMSActivationDetail 
	
	function Get-KMSActivationDetail
	{
	
	    <#
	        .Synopsis 
	            Gets the Activation Detail from the KMS Server.
	            
	        .Description
	            Gets the Activation Detail from the KMS Server.
	            
	        .Parameter KMS
	            KMS Server to connect to.
	            
	        .Parameter Filter
	            Filter for the Computers to get activation for.
	        
	        .Parameter After
	            The DateTime to start the query from. For example if I only want activations for the last thirty days:
	            the date time would be ((Get-Date).AddMonths(-1))
	            
	        .Parameter Unique
	            Only return Unique entries.
	            
	        .Example
	            Get-KMSActivationDetail -kms MyKMSServer
	            Description
	            -----------
	            Get all the activations for the target KMS server.
	            
	        .Example
	            Get-KMSActivationDetail -kms MyKMSServer -filter mypc
	            Description
	            -----------
	            Get all the activations for all the machines that are like "mypc" on the target KMS server.
	            
	        .Example
	            Get-KMSActivationDetail -kms MyKMSServer -After ((Get-Date).AddDays(-1))
	            Description
	            -----------
	            Get all the activations for the last day on the target KMS server.
	    
	        .Example
	            Get-KMSActivationDetail -kms MyKMSServer -unique
	            Description
	            -----------
	            Returns all the unique activate for the targeted KMS server.
	            
	        .OUTPUTS
	            PSCustomObject
	            
	        .INPUTS
	            System.String
	            
	        .Link
	            Get-KMSServer
	            Get-KMSStatus
	            
	        .Notes
	            NAME:      Get-KMSActivationDetail
	            AUTHOR:    bsonposh
	            Website:   http://www.bsonposh.com
	            Version:   1
	            #Requires -Version 2.0
	    #>
	    
	    [Cmdletbinding()]
	    Param(
	    
	        [Parameter(mandatory=$true)]
	        [string]$KMS,
	        
	        [Parameter()]
	        [string]$Filter="*",
	        
	        [Parameter()]
	        [datetime]$After,
	        
	        [Parameter()]
	        [switch]$Unique
	        
	    )
	    Write-Verbose " [Get-KMSActivationDetail] :: Cmdlet Start"
	    Write-Verbose " [Get-KMSActivationDetail] :: KMS Server   = $KMS"
	    Write-Verbose " [Get-KMSActivationDetail] :: Filter       = $Filter"
	    Write-Verbose " [Get-KMSActivationDetail] :: After Date   = $After"
	    Write-Verbose " [Get-KMSActivationDetail] :: Unique       = $Unique"
	    
	    if($After)
	    {
	        Write-Verbose " [Get-KMSActivationDetail] :: Processing Records after $After"
	        $Events = Get-Eventlog -LogName "Key Management Service" -ComputerName $KMS -After $After -Message "*$Filter*"
	    }
	    else
	    {
	        Write-Verbose " [Get-KMSActivationDetail] :: Processing Records"
	        $Events = Get-Eventlog -LogName "Key Management Service" -ComputerName $KMS -Message "*$Filter*"
	    }
	    
	    Write-Verbose " [Get-KMSActivationDetail] :: Creating Objects Collection"
	    $MyObjects = @()
	    
	    Write-Verbose " [Get-KMSActivationDetail] :: Processing {$($Events.count)} Events"
	    foreach($Event in $Events)
	    {
	        Write-Verbose " [Get-KMSActivationDetail] :: Creating Hash Table [$($Event.Index)]"
	        $Message = $Event.Message.Split(",")
	        
	        $myobj = @{}
	        Write-Verbose " [Get-KMSActivationDetail] :: Setting ComputerName to $($Message[3])"
	        $myobj.Computername = $Message[3]
	        Write-Verbose " [Get-KMSActivationDetail] :: Setting Date to $($Event.TimeGenerated)"
	        $myobj.Date = $Event.TimeGenerated
	        Write-Verbose " [Get-KMSActivationDetail] :: Creating Custom Object [$($Event.Index)]"
	        $MyObjects += New-Object PSObject -Property $myobj
	    }
	    
	    if($Unique)
	    {
	        Write-Verbose " [Get-KMSActivationDetail] :: Parsing out Unique Objects"
	        $UniqueObjects = $MyObjects | Group-Object -Property Computername
	        foreach($UniqueObject in $UniqueObjects)
	        {
	            $myobj = @{}
	            $myobj.ComputerName = $UniqueObject.Name
	            $myobj.Count = $UniqueObject.count
	    
	            $obj = New-Object PSObject -Property $myobj
	            $obj.PSTypeNames.Clear()
	            $obj.PSTypeNames.Add('BSonPosh.KMS.ActivationDetail')
	            $obj
	        }
	        
	    }
	    else
	    {
	        $MyObjects
	    }
	
	}
	#endregion 
	
	### IP Calculator - source: http://www.indented.co.uk/index.php/2010/01/23/powershell-subnet-math/
	
	#region ConvertTo-BinaryIP
	function ConvertTo-BinaryIP {
	  <#
	    .Synopsis
	      Converts a Decimal IP address into a binary format.
	    .Description
	      ConvertTo-BinaryIP uses System.Convert to switch between decimal and binary format. The output from this function is dotted binary.
	    .Parameter IPAddress
	      An IP Address to convert.
	  #>
	 
	  [CmdLetBinding()]
	  Param(
	    [Parameter(Mandatory = $True, Position = 0, ValueFromPipeline = $True)]
	    [Net.IPAddress]$IPAddress
	  )
	 
	  Process {
	    Return [String]::Join('.', $( $IPAddress.GetAddressBytes() |
	      ForEach-Object { [Convert]::ToString($_, 2).PadLeft(8, '0') } ))
	  }
	}
	#endregion ConvertTo-BinaryIP
	
	#region ConvertTo-DecimalIP
	function ConvertTo-DecimalIP {
	  <#
	    .Synopsis
	      Converts a Decimal IP address into a 32-bit unsigned integer.
	    .Description
	      ConvertTo-DecimalIP takes a decimal IP, uses a shift-like operation on each octet and returns a single UInt32 value.
	    .Parameter IPAddress
	      An IP Address to convert.
	  #>
	 
	  [CmdLetBinding()]
	  Param(
	    [Parameter(Mandatory = $True, Position = 0, ValueFromPipeline = $True)]
	    [Net.IPAddress]$IPAddress
	  )
	 
	  Process {
	    $i = 3; $DecimalIP = 0;
	    $IPAddress.GetAddressBytes() | ForEach-Object { $DecimalIP += $_ * [Math]::Pow(256, $i); $i-- }
	 
	    Return [UInt32]$DecimalIP
	  }
	}
	#endregion ConvertTo-DecimalIP
	
	#region ConvertTo-DottedDecimalIP
	function ConvertTo-DottedDecimalIP {
	  <#
	    .Synopsis
	      Returns a dotted decimal IP address from either an unsigned 32-bit integer or a dotted binary string.
	    .Description
	      ConvertTo-DottedDecimalIP uses a regular expression match on the input string to convert to an IP address.
	    .Parameter IPAddress
	      A string representation of an IP address from either UInt32 or dotted binary.
	  #>
	 
	  [CmdLetBinding()]
	  Param(
	    [Parameter(Mandatory = $True, Position = 0, ValueFromPipeline = $True)]
	    [String]$IPAddress
	  )
	 
	  Process {
	    Switch -RegEx ($IPAddress) {
	      "([01]{8}\.){3}[01]{8}" {
	        Return [String]::Join('.', $( $IPAddress.Split('.') | ForEach-Object { [Convert]::ToUInt32($_, 2) } ))
	      }
	      "\d" {
	        $IPAddress = [UInt32]$IPAddress
	        $DottedIP = $( For ($i = 3; $i -gt -1; $i--) {
	          $Remainder = $IPAddress % [Math]::Pow(256, $i)
	          ($IPAddress - $Remainder) / [Math]::Pow(256, $i)
	          $IPAddress = $Remainder
	         } )
	 
	        Return [String]::Join('.', $DottedIP)
	      }
	      default {
	        Write-Error "Cannot convert this format"
	      }
	    }
	  }
	}
	#endregion ConvertTo-DottedDecimalIP
	
	#region ConvertTo-MaskLength
	function ConvertTo-MaskLength {
	  <#
	    .Synopsis
	      Returns the length of a subnet mask.
	    .Description
	      ConvertTo-MaskLength accepts any IPv4 address as input, however the output value
	      only makes sense when using a subnet mask.
	    .Parameter SubnetMask
	      A subnet mask to convert into length
	  #>
	 
	  [CmdLetBinding()]
	  Param(
	    [Parameter(Mandatory = $True, Position = 0, ValueFromPipeline = $True)]
	    [Alias("Mask")]
	    [Net.IPAddress]$SubnetMask
	  )
	 
	  Process {
	    $Bits = "$( $SubnetMask.GetAddressBytes() | ForEach-Object { [Convert]::ToString($_, 2) } )" -Replace '[\s0]'
	 
	    Return $Bits.Length
	  }
	}
	#endregion ConvertTo-MaskLength
	
	#region ConvertTo-Mask
	function ConvertTo-Mask {
	  <#
	    .Synopsis
	      Returns a dotted decimal subnet mask from a mask length.
	    .Description
	      ConvertTo-Mask returns a subnet mask in dotted decimal format from an integer value ranging
	      between 0 and 32. ConvertTo-Mask first creates a binary string from the length, converts
	      that to an unsigned 32-bit integer then calls ConvertTo-DottedDecimalIP to complete the operation.
	    .Parameter MaskLength
	      The number of bits which must be masked.
	  #>
	 
	  [CmdLetBinding()]
	  Param(
	    [Parameter(Mandatory = $True, Position = 0, ValueFromPipeline = $True)]
	    [Alias("Length")]
	    [ValidateRange(0, 32)]
	    $MaskLength
	  )
	 
	  Process {
	    Return ConvertTo-DottedDecimalIP ([Convert]::ToUInt32($(("1" * $MaskLength).PadRight(32, "0")), 2))
	  }
	}
	#endregion ConvertTo-Mask
	
	### Technet Functions http://technet.com
	
	#region Test-Server
	#http://gallery.technet.microsoft.com/scriptcenter/Powershell-Test-Server-e0cdea9a
	function Test-Server{
	[cmdletBinding()]
	param(
		[parameter(Mandatory=$true,ValueFromPipeline=$true)]
		[string[]]$ComputerName,
		[parameter(Mandatory=$false)]
		[switch]$CredSSP,
		[Management.Automation.PSCredential] $Credential)
		
	begin{
		$total = Get-Date
		$results = @()
		if($credssp){if(!($credential)){Write-Host "must supply Credentials with CredSSP test";break}}
	}
	process{
	    foreach($name in $computername)
	    {
		$dt = $cdt= Get-Date
		Write-verbose "Testing: $Name"
		$failed = 0
		try{
		$DNSEntity = [Net.Dns]::GetHostEntry($name)
		$domain = ($DNSEntity.hostname).replace("$name.","")
		$ips = $DNSEntity.AddressList | ForEach-Object{$_.IPAddressToString}
		}
		catch
		{
			$rst = "" |  Select-Object Name,IP,Domain,Ping,WSMAN,CredSSP,RemoteReg,RPC,RDP
			$rst.name = $name
			$results += $rst
			$failed = 1
		}
		Write-verbose "DNS:  $((New-TimeSpan $dt ($dt = get-date)).totalseconds)"
		if($failed -eq 0){
		foreach($ip in $ips)
		{
		    
			$rst = "" |  Select-Object Name,IP,Domain,Ping,WSMAN,CredSSP,RemoteReg,RPC,RDP
		    $rst.name = $name
			$rst.ip = $ip
			$rst.domain = $domain
			####RDP Check (firewall may block rest so do before ping
			try{
	            $socket = New-Object Net.Sockets.TcpClient($name, 3389)
			  if($null -eq $socket)
			  {
				 $rst.RDP = $false
			  }
			  else
			  {
				 $rst.RDP = $true
				 $socket.close()
			  }
	       }
	       catch
	       {
	            $rst.RDP = $false
	       }
			Write-verbose "RDP:  $((New-TimeSpan $dt ($dt = get-date)).totalseconds)"
	        #########ping
		    if(test-connection $ip -count 1 -Quiet)
		    {
		        Write-verbose "PING:  $((New-TimeSpan $dt ($dt = get-date)).totalseconds)"
				$rst.ping = $true
				try{############wsman
					Test-WSMan $ip | Out-Null
					$rst.WSMAN = $true
					}
				catch
					{$rst.WSMAN = $false}
					Write-verbose "WSMAN:  $((New-TimeSpan $dt ($dt = get-date)).totalseconds)"
				if($rst.WSMAN -and $credssp) ########### credssp
				{
					try{
						Test-WSMan $ip -Authentication Credssp -Credential $cred
						$rst.CredSSP = $true
						}
					catch
						{$rst.CredSSP = $false}
					Write-verbose "CredSSP:  $((New-TimeSpan $dt ($dt = get-date)).totalseconds)"
				}
				try ########remote reg
				{
					[Microsoft.Win32.RegistryKey]::OpenRemoteBaseKey([Microsoft.Win32.RegistryHive]::LocalMachine, $ip) | Out-Null
					$rst.remotereg = $true
				}
				catch
					{$rst.remotereg = $false}
				Write-verbose "remote reg:  $((New-TimeSpan $dt ($dt = get-date)).totalseconds)"
				try ######### wmi
				{	
					$w = [wmi] ''
					$w.psbase.options.timeout = 15000000
					$w.path = "\\$Name\root\cimv2:Win32_ComputerSystem.Name='$Name'"
					$w | Select-Object none | Out-Null
					$rst.RPC = $true
				}
				catch
					{$rst.rpc = $false}
				Write-verbose "WMI:  $((New-TimeSpan $dt ($dt = get-date)).totalseconds)" 
		    }
			else
			{
				$rst.ping = $false
				$rst.wsman = $false
				$rst.credssp = $false
				$rst.remotereg = $false
				$rst.rpc = $false
			}
			$results += $rst	
		}}
		Write-Verbose "Time for $($Name): $((New-TimeSpan $cdt ($dt)).totalseconds)"
		Write-Verbose "----------------------------"
	}
	}
	end{
		Write-Verbose "Time for all: $((New-TimeSpan $total ($dt)).totalseconds)"
		Write-Verbose "----------------------------"
	return $results
	}
	}
	#endregion
	
	#region Get-USB
	
	function Get-USB {
	    <#
	    .Synopsis
	        Gets USB devices attached to the system
	    .Description
	        Uses WMI to get the USB Devices attached to the system
	    .Example
	        Get-USB
	    .Example
	        Get-USB | Group-Object Manufacturer  
	    .Parameter ComputerName
	        The name of the computer to get the USB devices from
	    #>
	    param($computerName = "localhost")
	    Get-WmiObject Win32_USBControllerDevice -ComputerName $ComputerName `
	        -Impersonation Impersonate -Authentication PacketPrivacy | 
	        Foreach-Object { [Wmi]$_.Dependent }
	}
	#endregion
	
	#region Get-ComputerComment
	#http://gallery.technet.microsoft.com/5c5bb1f7-519b-43b3-9d3a-dce8b9390244
	function Get-ComputerComment( $ComputerName ) 
	{ 
		$Registry = [Microsoft.Win32.RegistryKey]::OpenRemoteBaseKey( "LocalMachine", $ComputerName ) 
	    if ( $Null -eq $Registry ) { 
	        return "Can't connect to the registry"
	    } 
	    $RegKey= $Registry.OpenSubKey( "SYSTEM\CurrentControlSet\Services\lanmanserver\parameters" ) 
	    if ( $Null -eq $RegKey ) { 
	        return "No Computer Description"
	    } 
	 
	    [string]$Description = $RegKey.GetValue("srvcomment") 
	    if ( $Null -eq $Description ) { 
	        $Description = "No Computer Description" 
	    } 
	    return "Computer Description: $Description "
	} 
	#endregion
	
	#region Set-ComputerComment
	#http://gallery.technet.microsoft.com/5c5bb1f7-519b-43b3-9d3a-dce8b9390244
	function Set-ComputerComment 
	{
		param(
		[string]$ComputerName,
		[string]$Description
		)
	    # $OsInfo = Get-WmiObject Win32_OperatingSystem -Computer $Computer 
	    # $Description  = $OsInfo 
	    $Registry = [Microsoft.Win32.RegistryKey]::OpenRemoteBaseKey( "LocalMachine", $ComputerName ) 
	    if ( $Null -eq $Registry ) { 
	        return $Null 
	    } 
	    $RegPermCheck = [Microsoft.Win32.RegistryKeyPermissionCheck]::ReadWriteSubTree  
	    $RegKeyRights = [System.Security.AccessControl.RegistryRights]::SetValue 
	    $RegKey = $Registry.OpenSubKey( "SYSTEM\CurrentControlSet\Services\lanmanserver\parameters", $RegPermCheck, $RegKeyRights )
	    if ( $Null -eq $RegKey ) { 
	        return $Null 
	    }
	    $RegKey.SetValue("srvcomment", $Description ) 
	} 
	#endregion
	
	#region Get-DnsDomain
	function Get-DnsDomain()
	#http://gallery.technet.microsoft.com/5c5bb1f7-519b-43b3-9d3a-dce8b9390244
	{ 
		# --------------------------------------------------------- 
		# Get the name of the domain this computer belongs to. 
		# --------------------------------------------------------- 
	    if ( $null -eq $Global:DnsDomain ) { 
	        $WmiInfo = get-wmiobject "Win32_NTDomain" | Where-Object {$null -ne $_.DnsForestName } 
	        $Global:DnsDomain = $WmiInfo.DnsForestName 
	    } 
	    return $Global:DnsDomain 
	} 
	#endregion
	
	#region Get-AdDomainPath
	function Get-AdDomainPath()
	#http://gallery.technet.microsoft.com/5c5bb1f7-519b-43b3-9d3a-dce8b9390244
	{ 
	    $DnsDomain = Get-DnsDomain 
	    $Tokens = $DnsDomain.Split(".") 
	    $Seperator= "" 
	    $Path = "" 
	    foreach ( $Token in $Tokens ) 
	    {     
	        $Path+= $Seperator 
	        $Path+= "DC=" 
	        $Path+= $Token 
	        $Seperator = "," 
	    } 
	    return $Path 
	} 
	#endregion
	
	#region Show-MsgBox

	 
	function Show-MsgBox 
	{ 
			<# 
	            .SYNOPSIS  
	            Shows a graphical message box, with various prompt types available. 
	 
	            .DESCRIPTION 
	            Emulates the Visual Basic MsgBox function.  It takes four parameters, of which only the prompt is mandatory 
	 
	            .INPUTS 
	            The parameters are:- 
	             
	            Prompt (mandatory):  
	                Text string that you wish to display 
	                 
	            Title (optional): 
	                The title that appears on the message box 
	                 
	            Icon (optional).  Available options are: 
	                Information, Question, Critical, Exclamation (not case sensitive) 
	                
	            BoxType (optional). Available options are: 
	                OKOnly, OkCancel, AbortRetryIgnore, YesNoCancel, YesNo, RetryCancel (not case sensitive) 
	                 
	            DefaultButton (optional). Available options are: 
	                1, 2, 3 
	 
	            .OUTPUTS 
	            Microsoft.VisualBasic.MsgBoxResult 
	 
	            .EXAMPLE 
	            C:\PS> Show-MsgBox Hello 
	            Shows a popup message with the text "Hello", and the default box, icon and defaultbutton settings. 
	 
	            .EXAMPLE 
	            C:\PS> Show-MsgBox -Prompt "This is the prompt" -Title "This Is The Title" -Icon Critical -BoxType YesNo -DefaultButton 2 
	            Shows a popup with the parameter as supplied. 
	 
	            .LINK 
	            http://msdn.microsoft.com/en-us/library/microsoft.visualbasic.msgboxresult.aspx 
	 
	            .LINK 
	            http://msdn.microsoft.com/en-us/library/microsoft.visualbasic.msgboxstyle.aspx 
	            #> 
	# By BigTeddy August 24, 2011 
	# http://social.technet.microsoft.com/profile/bigteddy/. 
	 
	 [CmdletBinding()] 
	    param( 
	    [Parameter(Position=0, Mandatory=$true)] [string]$Prompt, 
	    [Parameter(Position=1, Mandatory=$false)] [string]$Title ="", 
	    [Parameter(Position=2, Mandatory=$false)] [ValidateSet("Information", "Question", "Critical", "Exclamation")] [string]$Icon ="Information", 
	    [Parameter(Position=3, Mandatory=$false)] [ValidateSet("OKOnly", "OKCancel", "AbortRetryIgnore", "YesNoCancel", "YesNo", "RetryCancel")] [string]$BoxType ="OkOnly", 
	    [Parameter(Position=4, Mandatory=$false)] [ValidateSet(1,2,3)] [int]$DefaultButton = 1 
	    ) 
	[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.VisualBasic") | Out-Null 
	switch ($Icon) { 
	            "Question" {$vb_icon = [microsoft.visualbasic.msgboxstyle]::Question } 
	            "Critical" {$vb_icon = [microsoft.visualbasic.msgboxstyle]::Critical} 
	            "Exclamation" {$vb_icon = [microsoft.visualbasic.msgboxstyle]::Exclamation} 
	            "Information" {$vb_icon = [microsoft.visualbasic.msgboxstyle]::Information}} 
	switch ($BoxType) { 
	            "OKOnly" {$vb_box = [microsoft.visualbasic.msgboxstyle]::OKOnly} 
	            "OKCancel" {$vb_box = [microsoft.visualbasic.msgboxstyle]::OkCancel} 
	            "AbortRetryIgnore" {$vb_box = [microsoft.visualbasic.msgboxstyle]::AbortRetryIgnore} 
	            "YesNoCancel" {$vb_box = [microsoft.visualbasic.msgboxstyle]::YesNoCancel} 
	            "YesNo" {$vb_box = [microsoft.visualbasic.msgboxstyle]::YesNo} 
	            "RetryCancel" {$vb_box = [microsoft.visualbasic.msgboxstyle]::RetryCancel}} 
	switch ($Defaultbutton) { 
	            1 {$vb_defaultbutton = [microsoft.visualbasic.msgboxstyle]::DefaultButton1} 
	            2 {$vb_defaultbutton = [microsoft.visualbasic.msgboxstyle]::DefaultButton2} 
	            3 {$vb_defaultbutton = [microsoft.visualbasic.msgboxstyle]::DefaultButton3}} 
	$popuptype = $vb_icon -bor $vb_box -bor $vb_defaultbutton 
	$ans = [Microsoft.VisualBasic.Interaction]::MsgBox($prompt,$popuptype,$title) 
	return $ans 
	} #end
	#endregion
	
	#region Invoke-RemoteCMD
	#http://gallery.technet.microsoft.com/scriptcenter/56962f03-0243-4c83-8cdd-88c37898ccc4
	function Invoke-RemoteCMD { 
	    param( 
	    [Parameter(Mandatory=$true,valuefrompipeline=$true)] 
	    [string]$ComputerName,
		[string]$Command,
		[int]$timeout = 30
		)
	    begin { 
	        [string]$outputFile = "windows\temp\LWAGUI.tmp"
			[string]$cmd = "POWERSHELL.EXE /C " + $command + " > C:\windows\temp\LWAGUITEMP.tmp; Move-Item C:\windows\temp\LWAGUITEMP.tmp C:\$outputFile"
			If (Test-Path "\\$ComputerName\C$\$outputFile") {
				Remove-Item "\\$ComputerName\C$\$outputFile"
			}
	    } 
	    process { 
	        $newproc = Invoke-WmiMethod -class Win32_process -name Create -ArgumentList ($cmd) -ComputerName $ComputerName 
			if ($newproc.ReturnValue -ne 0 ) {
				Return "Failed to process `"$Command`""
			}
			for ($i = 0; $i -lt $timeout; $i++) {
				If (Test-Path "\\$ComputerName\C$\$outputFile") {
					$Return = Get-Content "\\$ComputerName\C$\$outputFile"
					Remove-Item "\\$ComputerName\C$\$outputFile"
					Return $Return
				}
				Start-Sleep 1
			}
		}
	}
	#endregion
	
	# Lee Holmes - http://www.leeholmes.com
	
	#region Test-PSRemoting
	
	function Test-PSRemoting 
	{ 
	    Param(
	        [alias('dnsHostName')]
	        [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true,ValueFromPipeline=$true)]
	        [string]$ComputerName
	    )
	    Process
	    {
	        Write-Verbose " [Test-PSRemoting] :: Start Process"
	        if($ComputerName -match "(.*)(\$)$")
	        {
	            $ComputerName = $ComputerName -replace "(.*)(\$)$",'$1'
	        }
	        
	        try 
	        { 
				# Checks if the port is open
				New-Object System.Net.Sockets.TCPClient -ArgumentList "$Computername",5985
	            $result = Invoke-Command -ComputerName $computername { 1 } -ErrorAction SilentlyContinue
	            
	            if($result -eq 1 )
	            {
	                return $True
	            }
	            else
	            {
	                return $False
	            }
	        } 
	        catch 
	        { 
	            return $False 
	        } 
	    }
	} 
		
	#endregion
	
	# Sapien Forum
	
	#region Show-InputBox
	#http://www.sapien.com/forums/scriptinganswers/forum_posts.asp?TID=2890

	Function Show-InputBox {
	 Param([string]$message=$(Throw "You must enter a prompt message"),
	       [string]$title="Input",
	       [string]$default
	       )
	       
	 [reflection.assembly]::loadwithpartialname("microsoft.visualbasic") | Out-Null
	 [microsoft.visualbasic.interaction]::InputBox($message,$title,$default)
	 
	}
	#endregion
	
	Function Remove-App {
		$UninstallApp = $Args[0] | Out-GridView -OutputMode Single -Title "Select a program to uninstall it"
		if ($UninstallApp) {
			$Confirmation = Show-MsgBox -Prompt "Uninstall $($UninstallApp.Name), are you sure ?" -Title "$ComputerName - Uninstall $($UninstallApp.Name)" -Icon "Exclamation" -BoxType YesNo
			if ($Confirmation -eq "YES") { 	
				if ($UninstallApp.QuietUninstallString) {
					Add-Logs -text "Uninstalling $($UninstallApp.Name)..."
					$UninstallString = $UninstallApp.QuietUninstallString
					
				}
				elseif ($UninstallApp.UninstallString) {
					Add-Logs -text "No quiet uninstall string for $($UninstallApp.Name), a remote uninstall would likely fail"
					$UninstallString = $UninstallApp.UninstallString
				}
				else {
					Add-Logs -text "No uninstall string for $($UninstallApp.Name), unable to uninstall"
					Return
				}
				# msiexec uninstall strings seem to always be missing the silent switch, and are missing quotation marks needed for PowerShell to handle them correctly
				$RegExMatches = Select-String '(?<=^msiexec.exe \/x).*\}|(?<=}).*$' -input $UninstallString -allmatches
				if ($RegExMatches) {
					$ID = $RegExMatches.Matches[0].Value
					$Append = $RegExMatches.Matches[1].Value
					$UninstallSTring = "msiexec.exe /quiet /X`'$ID`'$Append"
				}
				Add-RichTextBox "Running $UninstallString"
				Invoke-RemoteCMD -ComputerName $ComputerName -Command $UninstallString
			}
		}
	}
#endregion
	$script:TabIndex = 0
	Function Get-TabIndex {
		$script:TabIndex
		[void]$script:TabIndex++
	}
	Function Get-ComputerInfo {
		#Disable the button
		$button_Check.Enabled = $false
		# Clear previous values
		$label_PermissionStatus.Text = $null
		$label_PSRemotingStatus.Text = $null
		$label_UptimeStatus.Text = $null
		$label_CurrentUser.Text = $null
		$label_PingStatus.Text = $null
		$label_RDPStatus.Text = $null
		$label_OSStatus.Text = $null
		# Trims whitespace in the computername
		$textbox_computername.Text = $textbox_computername.Text.trim()
		#Get the current computer in txt box
		Get-ComputerTxtBox
		Add-logs -text "$ComputerName - Check Connectivity and Basic Properties"
		# Test Connection
		if (Test-Connection $ComputerName -Count 1 -Quiet) {
			$label_PingStatus.Text = "OK";$label_PingStatus.ForeColor = "#88FF88"
	
			# Test Permissions
			if (Test-Path "\\$ComputerName\c$"){
				$label_PermissionStatus.Text = "OK";$label_PermissionStatus.ForeColor = "#88FF88"
				
				# Test PSRemoting
				if (Test-PSRemoting -computername $ComputerName){$label_PSRemotingStatus.Text = "OPEN";$label_PSRemotingStatus.ForeColor = "#88FF88"}	
				else{$label_PSRemotingStatus.Text = "CLOSED";$label_PSRemotingStatus.ForeColor = "red"}
				
				# Test RDP
				if (Test-Port -tcp 3389 -computername $ComputerName ){$label_RDPStatus.Text = "OPEN";$label_RDPStatus.ForeColor = "#88FF88"}
				else{$label_RDPStatus.Text = "CLOSED";$label_RDPStatus.ForeColor = "red"}
				
				# Get the OS
				 $OSWin32_OS = Get-WmiObject -Query "SELECT * FROM Win32_OperatingSystem" -ComputerName $ComputerName
				 $OSCaption = ($OSWin32_OS|Select-Object caption).Caption
				 $OSBuild = $OSWin32_OS.BuildNumber
				 # List of Windows 10 build numbers and their corresponding release numbers
				 $hashBuilds = @{
					18362 = 1903;
					17763 = 1809;
					17134 = 1803;
					16299 = 1709;
					15063 = 1703;
					14393 = 1607;
					10586 = 1511;
				 }
				 If ($hashBuilds.[int]$OSBuild) {
					$OSRelease = " | " + $hashBuilds.[int]$OSBuild
				 }
				 Else {
					$OSRelease = $null
				 }
				#2003/xp+
				 $OSOther = $OSWin32_OS.OtherTypeDescription
				 $OSSP = $OSWin32_OS.CSDVersion
				#2008/win7+
				 $OSArchi = $OSWin32_OS.OSArchitecture
				
				$OSFullCaption = "$OSCaption $OSOther $OSArchi $OSSP ($OSBuild$OSRelease)"
				if ($OSFullCaption -contains "64"){$OSFullCaption = "$OSCaption $OSOther x86 $OSSP"}
				
				$label_OSStatus.Text = $OSFullCaption.Replace('  ',' ')

				
				# Get the uptime
				$LBTime = $OSWin32_OS.ConvertToDateTime($OSWin32_OS.Lastbootuptime)
				[TimeSpan]$uptime = New-TimeSpan $LBTime $(get-date)
				$label_UptimeStatus.Text = "$($uptime.days) Days $($uptime.hours) Hours $($uptime.minutes) Minutes $($uptime.seconds) Seconds"
				
				# Get the current user
				$userdata = Invoke-RemoteCMD -ComputerName $ComputerName -Command "quser"
                $user = ($userdata[1..$userdata.length] | Foreach-Object { if ($_ -match ">") {$_.replace(">","").split()[0]} else {$_.split()[1]}})
                $currentuser = Get-DomainUser -UserName $user.Split('\')[-1] | Foreach-object {$_.DisplayName, $_.Email} | Write-output
				if ($user) {
					Add-RichTextBox "$user $currentuser" 
				}
                $label_CurrentUser.Text = "$user"
				
			}#end if (Test-Path "\\$ComputerName\c$")
			else {$label_PermissionStatus.Text = "FAIL";$label_PermissionStatus.ForeColor = "red"}
		}#end if (Test-Connection $ComputerName -Count 1 -Quiet)
		else {$label_PingStatus.Text = "FAIL";$label_PingStatus.ForeColor = "red"}
	$button_Check.Enabled = $true
	}

	Function Start-Ping {
		Get-ComputerTxtBox
		Add-Logs -text "$ComputerName - Test-Connection"
		$button_ping.Enabled = $False
		start-process ping -ArgumentList $ComputerName,-t;
		$button_ping.Enabled = $true
	}
#region Start-About_pff
function Start-About_pff
{
	#----------------------------------------------
	#region Import the Assemblies
	#----------------------------------------------
	[void][reflection.assembly]::Load("System, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")
	[void][reflection.assembly]::Load("System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")
	[void][reflection.assembly]::Load("System.Data, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")
	[void][reflection.assembly]::Load("System.Drawing, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")
	[void][reflection.assembly]::Load("mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")
	[Reflection.Assembly]::LoadWithPartialName('System.Drawing')
	#endregion Import Assemblies

	#----------------------------------------------
	#region Generated Form Objects
	#----------------------------------------------
	[System.Windows.Forms.Application]::EnableVisualStyles()
	$form_Author = New-Object 'System.Windows.Forms.Form'
	$labelLastUpdateApplicatio = New-Object 'System.Windows.Forms.Label'
	$labelAbout = New-Object 'System.Windows.Forms.Label'
	$linklabel_Twitter = New-Object 'System.Windows.Forms.LinkLabel'
	$labelTwitter = New-Object 'System.Windows.Forms.Label'
	$labelLazyWinAdminIsAPower = New-Object 'System.Windows.Forms.Label'
	$labelAuthorName = New-Object 'System.Windows.Forms.Label'
	$labelEmail = New-Object 'System.Windows.Forms.Label'
	$linklabel_Blog = New-Object 'System.Windows.Forms.LinkLabel'
	$labelContributors = New-Object 'System.Windows.Forms.Label'
	$label_Blog = New-Object 'System.Windows.Forms.Label'
	$linklabel_Email = New-Object 'System.Windows.Forms.LinkLabel'
	$label_Author = New-Object 'System.Windows.Forms.Label'
	$button_AuthorOK = New-Object 'System.Windows.Forms.Button'
	$InitialFormWindowState = New-Object 'System.Windows.Forms.FormWindowState'
	#endregion Generated Form Objects

	#----------------------------------------------
	# User Generated Script
	#----------------------------------------------
	
	$FormEvent_Load={
		#TODO: Initialize Form Controls here
		$linklabel_Blog.text = $AuthorBlogName
		$linklabel_Email.Text = $AuthorEmail
	}
	
	$linklabel_AuthorBlog_LinkClicked=[System.Windows.Forms.LinkLabelLinkClickedEventHandler]{
		[System.Diagnostics.Process]::Start("$AuthorBlogURL")
	}
	
	$linklabel_AuthorEmail_LinkClicked=[System.Windows.Forms.LinkLabelLinkClickedEventHandler]{
		[System.Diagnostics.Process]::Start("mailto:$authoremail?subject=$AuthorBlogName")
	}
	
	$linklabel_Twitter_LinkClicked=[System.Windows.Forms.LinkLabelLinkClickedEventHandler]{
		[System.Diagnostics.Process]::Start("$global:AuthorTwitterURL")
	}	# --End User Generated Script--
	#----------------------------------------------
	#region Generated Events
	#----------------------------------------------
	
	$Form_StateCorrection_Load=
	{
		#Correct the initial state of the form to prevent the .Net maximized form issue
		$form_Author.WindowState = $InitialFormWindowState
	}
	
	$Form_StoreValues_Closing=
	{
		#Store the control values
	}

	
	$Form_Cleanup_FormClosed=
	{
		#Remove all event handlers from the controls
		try
		{
			$linklabel_Twitter.remove_LinkClicked($linklabel_Twitter_LinkClicked)
			$linklabel_Blog.remove_LinkClicked($linklabel_AuthorBlog_LinkClicked)
			$linklabel_Email.remove_LinkClicked($linklabel_AuthorEmail_LinkClicked)
			$form_Author.remove_Load($FormEvent_Load)
			$form_Author.remove_Load($Form_StateCorrection_Load)
			$form_Author.remove_Closing($Form_StoreValues_Closing)
			$form_Author.remove_FormClosed($Form_Cleanup_FormClosed)
		}
		catch [Exception]
		{ }
	}
	#endregion Generated Events

	#----------------------------------------------
	#region Generated Form Code
	#----------------------------------------------
	#
	# form_Author
	#
	$form_Author.Controls.Add($labelLastUpdateApplicatio)
	$form_Author.Controls.Add($labelAbout)
	$form_Author.Controls.Add($linklabel_Twitter)
	$form_Author.Controls.Add($labelTwitter)
	$form_Author.Controls.Add($labelLazyWinAdminIsAPower)
	$form_Author.Controls.Add($labelAuthorName)
	$form_Author.Controls.Add($labelEmail)
	$form_Author.Controls.Add($linklabel_Blog)
	$form_Author.Controls.Add($label_Blog)
	$form_Author.Controls.Add($linklabel_Email)
	$form_Author.Controls.Add($label_Author)
	$form_Author.Controls.Add($labelContributors)
	$form_Author.Controls.Add($button_AuthorOK)
	$form_Author.AcceptButton = $button_AuthorOK
	$form_Author.ClientSize = '290, 264'
	$form_Author.FormBorderStyle = 'FixedDialog'
	$form_Author.MaximizeBox = $False
	$form_Author.MinimizeBox = $False
	$form_Author.Name = "form_Author"
	$form_Author.Text = "About LazyWinAdmin"
	$form_Author.add_Load($FormEvent_Load)
	#
	# labelLastUpdateApplicatio
	#
	$labelLastUpdateApplicatio.Font = "Microsoft Sans Serif, 9.25pt, style=Bold"
	$labelLastUpdateApplicatio.Location = '21, 32'
	$labelLastUpdateApplicatio.Name = "labelLastUpdateApplicatio"
	$labelLastUpdateApplicatio.Size = '242, 23'
	$labelLastUpdateApplicatio.TabIndex = 11
	$labelLastUpdateApplicatio.Text = "Last Update: $ApplicationLastUpdate"
	#
	# labelAbout
	#
	$labelAbout.Font = "Microsoft Sans Serif, 9.25pt, style=Bold"
	$labelAbout.Location = '21, 9'
	$labelAbout.Name = "labelAbout"
	$labelAbout.Size = '242, 23'
	$labelAbout.TabIndex = 10
	$labelAbout.Text = "$ApplicationName $ApplicationVersion"
	#
	# labelLazyWinAdminIsAPower
	#
	$labelLazyWinAdminIsAPower.Location = '21, 61'
	$labelLazyWinAdminIsAPower.Name = "labelLazyWinAdminIsAPower"
	$labelLazyWinAdminIsAPower.Size = '244, 67'
	$labelLazyWinAdminIsAPower.TabIndex = 7
	$labelLazyWinAdminIsAPower.Text = "LazyWinAdmin is a PowerShell script for managing Windows computers.

The original GUI/WinForm was created using Sapien Powershell Studio 2012."
	#
	# label_Author
	#
	$label_Author.Location = '20, 135'
	$label_Author.Name = "label_Author"
	$label_Author.Size = '140, 15'
	$label_Author.TabIndex = 2
	$label_Author.Text = "Original Author"
	#
	# labelAuthorName
	#
	$labelAuthorName.Font = "Microsoft Sans Serif, 9.25pt, style=Bold"
	$labelAuthorName.Location = '20, 150'
	$labelAuthorName.Name = "labelAuthorName"
	$labelAuthorName.Size = '140, 15'
	$labelAuthorName.TabIndex = 6
	$labelAuthorName.Text = "$AuthorName"
	#
	# linklabel_Email
	#
	$linklabel_Email.Location = '20, 165'
	$linklabel_Email.Name = "linklabel_Email"
	$linklabel_Email.Size = '140, 15'
	$linklabel_Email.TabIndex = 1
	$linklabel_Email.TabStop = $True
	$linklabel_Email.Text = "$AuthorEmail"
	$linklabel_Email.add_LinkClicked($linklabel_AuthorEmail_LinkClicked)
	#
	# linklabel_Blog
	#
	$linklabel_Blog.Location = '20, 180'
	$linklabel_Blog.Name = "linklabel_Blog"
	$linklabel_Blog.Size = '140, 15'
	$linklabel_Blog.TabIndex = 4
	$linklabel_Blog.TabStop = $True
	$linklabel_Blog.Text = "$AuthorBlogURL"
	$linklabel_Blog.add_LinkClicked($linklabel_AuthorBlog_LinkClicked)
	#
	# labelContributors
	#
	$labelContributors.Location = '160, 135'
	$labelContributors.Name = 'labelContributors_Label'
	$labelContributors.Size = '100, 60'
	$labelContributors.TabIndex = 12
	$labelContributors.Text = "Contributors `nLukas Velikov `nTim `nJonathan Daigle"
	#
	# button_AuthorOK
	#
	$button_AuthorOK.DialogResult = 'OK'
	$button_AuthorOK.Location = '106, 234'
	$button_AuthorOK.Name = "button_AuthorOK"
	$button_AuthorOK.Size = '75, 23'
	$button_AuthorOK.TabIndex = 0
	$button_AuthorOK.Text = "OK"
	#endregion Generated Form Code

	#----------------------------------------------

	#Save the initial state of the form
	$InitialFormWindowState = $form_Author.WindowState
	#Init the OnLoad event to correct the initial state of the form
	$form_Author.add_Load($Form_StateCorrection_Load)
	#Clean up the control events
	$form_Author.add_FormClosed($Form_Cleanup_FormClosed)
	#Store the control values when form is closing
	$form_Author.add_Closing($Form_StoreValues_Closing)
	#Show the Form
	return $form_Author.ShowDialog()

}
#endregion

#region Get-PendingReboot
Function Get-PendingReboot
{
<#
.SYNOPSIS
    Gets the pending reboot status on a local or remote computer.

.DESCRIPTION
    This function will query the registry on a local or remote computer and determine if the
    system is pending a reboot, from Microsoft updates, Configuration Manager Client SDK, Pending Computer 
    Rename, Domain Join or Pending File Rename Operations. For Windows 2008+ the function will query the 
    CBS registry key as another factor in determining pending reboot state.  "PendingFileRenameOperations" 
    and "Auto Update\RebootRequired" are observed as being consistant across Windows Server 2003 & 2008.
	
    CBServicing = Component Based Servicing (Windows 2008+)
    WindowsUpdate = Windows Update / Auto Update (Windows 2003+)
    CCMClientSDK = SCCM 2012 Clients only (DetermineIfRebootPending method) otherwise $null value
    PendComputerRename = Detects either a computer rename or domain join operation (Windows 2003+)
    PendFileRename = PendingFileRenameOperations (Windows 2003+)
    PendFileRenVal = PendingFilerenameOperations registry value; used to filter if need be, some Anti-
                     Virus leverage this key for def/dat removal, giving a false positive PendingReboot

.PARAMETER ComputerName
    A single Computer or an array of computer names.  The default is localhost ($env:COMPUTERNAME).

.PARAMETER ErrorLog
    A single path to send error data to a log file.

.EXAMPLE
    PS C:\> Get-PendingReboot -ComputerName (Get-Content C:\ServerList.txt) | Format-Table -AutoSize
	
    Computer CBServicing WindowsUpdate CCMClientSDK PendFileRename PendFileRenVal RebootPending
    -------- ----------- ------------- ------------ -------------- -------------- -------------
    DC01           False         False                       False                        False
    DC02           False         False                       False                        False
    FS01           False         False                       False                        False

    This example will capture the contents of C:\ServerList.txt and query the pending reboot
    information from the systems contained in the file and display the output in a table. The
    null values are by design, since these systems do not have the SCCM 2012 client installed,
    nor was the PendingFileRenameOperations value populated.

.EXAMPLE
    PS C:\> Get-PendingReboot
	
    Computer           : WKS01
    CBServicing        : False
    WindowsUpdate      : True
    CCMClient          : False
    PendComputerRename : False
    PendFileRename     : False
    PendFileRenVal     : 
    RebootPending      : True
	
    This example will query the local machine for pending reboot information.
	
.EXAMPLE
    PS C:\> $Servers = Get-Content C:\Servers.txt
    PS C:\> Get-PendingReboot -Computer $Servers | Export-Csv C:\PendingRebootReport.csv -NoTypeInformation
	
    This example will create a report that contains pending reboot information.

.LINK
    Component-Based Servicing:
    http://technet.microsoft.com/en-us/library/cc756291(v=WS.10).aspx
	
    PendingFileRename/Auto Update:
    http://support.microsoft.com/kb/2723674
    http://technet.microsoft.com/en-us/library/cc960241.aspx
    http://blogs.msdn.com/b/hansr/archive/2006/02/17/patchreboot.aspx

    SCCM 2012/CCM_ClientSDK:
    http://msdn.microsoft.com/en-us/library/jj902723.aspx

.NOTES
    Author:  Brian Wilhite
    Email:   bcwilhite (at) live.com
    Date:    29AUG2012
    PSVer:   2.0/3.0/4.0/5.0
    Updated: 27JUL2015
    UpdNote: Added Domain Join detection to PendComputerRename, does not detect Workgroup Join/Change
             Fixed Bug where a computer rename was not detected in 2008 R2 and above if a domain join occurred at the same time.
             Fixed Bug where the CBServicing wasn't detected on Windows 10 and/or Windows Server Technical Preview (2016)
             Added CCMClient property - Used with SCCM 2012 Clients only
             Added ValueFromPipelineByPropertyName=$true to the ComputerName Parameter
             Removed $Data variable from the PSObject - it is not needed
             Bug with the way CCMClientSDK returned null value if it was false
             Removed unneeded variables
             Added PendFileRenVal - Contents of the PendingFileRenameOperations Reg Entry
             Removed .Net Registry connection, replaced with WMI StdRegProv
             Added ComputerPendingRename
#>

[CmdletBinding()]
param(
	[Parameter(Position=0,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
	[Alias("CN","Computer")]
	[String[]]$ComputerName="$env:COMPUTERNAME",
	[String]$ErrorLog
	)

Begin {  }## End Begin Script Block
Process {
  Foreach ($Computer in $ComputerName) {
	Try {
	    ## Setting pending values to false to cut down on the number of else statements
	    $CompPendRen,$PendFileRename,$Pending,$SCCM = $false,$false,$false,$false
                        
	    ## Setting CBSRebootPend to null since not all versions of Windows has this value
	    $CBSRebootPend = $null
						
	    ## Querying WMI for build version
	    $WMI_OS = Get-WmiObject -Class Win32_OperatingSystem -Property BuildNumber, CSName -ComputerName $Computer -ErrorAction Stop

	    ## Making registry connection to the local/remote computer
	    $HKLM = [UInt32] "0x80000002"
	    $WMI_Reg = [WMIClass] "\\$Computer\root\default:StdRegProv"
						
	    ## If Vista/2008 & Above query the CBS Reg Key
	    If ([Int32]$WMI_OS.BuildNumber -ge 6001) {
		    $RegSubKeysCBS = $WMI_Reg.EnumKey($HKLM,"SOFTWARE\Microsoft\Windows\CurrentVersion\Component Based Servicing\")
		    $CBSRebootPend = $RegSubKeysCBS.sNames -contains "RebootPending"		
	    }
							
	    ## Query WUAU from the registry
	    $RegWUAURebootReq = $WMI_Reg.EnumKey($HKLM,"SOFTWARE\Microsoft\Windows\CurrentVersion\WindowsUpdate\Auto Update\")
	    $WUAURebootReq = $RegWUAURebootReq.sNames -contains "RebootRequired"
						
	    ## Query PendingFileRenameOperations from the registry
	    $RegSubKeySM = $WMI_Reg.GetMultiStringValue($HKLM,"SYSTEM\CurrentControlSet\Control\Session Manager\","PendingFileRenameOperations")
	    $RegValuePFRO = $RegSubKeySM.sValue

	    ## Query JoinDomain key from the registry - These keys are present if pending a reboot from a domain join operation
	    $Netlogon = $WMI_Reg.EnumKey($HKLM,"SYSTEM\CurrentControlSet\Services\Netlogon").sNames
	    $PendDomJoin = ($Netlogon -contains 'JoinDomain') -or ($Netlogon -contains 'AvoidSpnSet')

	    ## Query ComputerName and ActiveComputerName from the registry
	    $ActCompNm = $WMI_Reg.GetStringValue($HKLM,"SYSTEM\CurrentControlSet\Control\ComputerName\ActiveComputerName\","ComputerName")            
	    $CompNm = $WMI_Reg.GetStringValue($HKLM,"SYSTEM\CurrentControlSet\Control\ComputerName\ComputerName\","ComputerName")

	    If (($ActCompNm -ne $CompNm) -or $PendDomJoin) {
	        $CompPendRen = $true
	    }
						
	    ## If PendingFileRenameOperations has a value set $RegValuePFRO variable to $true
	    If ($RegValuePFRO) {
		    $PendFileRename = $true
	    }

	    ## Determine SCCM 2012 Client Reboot Pending Status
	    ## To avoid nested 'if' statements and unneeded WMI calls to determine if the CCM_ClientUtilities class exist, setting EA = 0
	    $CCMClientSDK = $null
	    $CCMSplat = @{
	        NameSpace='ROOT\ccm\ClientSDK'
	        Class='CCM_ClientUtilities'
	        Name='DetermineIfRebootPending'
	        ComputerName=$Computer
	        ErrorAction='Stop'
	    }
	    ## Try CCMClientSDK
	    Try {
	        $CCMClientSDK = Invoke-WmiMethod @CCMSplat
	    } Catch [System.UnauthorizedAccessException] {
	        $CcmStatus = Get-Service -Name CcmExec -ComputerName $Computer -ErrorAction SilentlyContinue
	        If ($CcmStatus.Status -ne 'Running') {
	            Write-Warning "$Computer`: Error - CcmExec service is not running."
	            $CCMClientSDK = $null
	        }
	    } Catch {
	        $CCMClientSDK = $null
	    }

	    If ($CCMClientSDK) {
	        If ($CCMClientSDK.ReturnValue -ne 0) {
		        Write-Warning "Error: DetermineIfRebootPending returned error code $($CCMClientSDK.ReturnValue)"          
		    }
		    If ($CCMClientSDK.IsHardRebootPending -or $CCMClientSDK.RebootPending) {
		        $SCCM = $true
		    }
	    }
            
	    Else {
	        $SCCM = $null
	    }

	    ## Creating Custom PSObject and Select-Object Splat
	    $SelectSplat = @{
	        Property=(
	            'Computer',
	            'CBServicing',
	            'WindowsUpdate',
	            'CCMClientSDK',
	            'PendComputerRename',
	            'PendFileRename',
	            'PendFileRenVal',
	            'RebootPending'
	        )}
	    New-Object -TypeName PSObject -Property @{
	        Computer=$WMI_OS.CSName
	        CBServicing=$CBSRebootPend
	        WindowsUpdate=$WUAURebootReq
	        CCMClientSDK=$SCCM
	        PendComputerRename=$CompPendRen
	        PendFileRename=$PendFileRename
	        PendFileRenVal=$RegValuePFRO
	        RebootPending=($CompPendRen -or $CBSRebootPend -or $WUAURebootReq -or $SCCM -or $PendFileRename)
	    } | Select-Object @SelectSplat

	} Catch {
	    Write-Warning "$Computer`: $_"
	    ## If $ErrorLog, log the file to a user specified location/path
	    If ($ErrorLog) {
	        Out-File -InputObject "$Computer`,$_" -FilePath $ErrorLog -Append
	    }				
	}			
  }## End Foreach ($Computer in $ComputerName)			
}## End Process

End {  }## End End

}
#endregion Get-PendingReboot

#region Start-MainForm_pff
function Start-MainForm_pff
{
	#----------------------------------------------
	#region Import the Assemblies
	#----------------------------------------------
	[void][reflection.assembly]::Load("System, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")
	[void][reflection.assembly]::Load("System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")
	[void][reflection.assembly]::Load("System.Drawing, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")
	[void][reflection.assembly]::Load("mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")
	[void][reflection.assembly]::Load("System.Data, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")
	[void][reflection.assembly]::Load("System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")
	#endregion Import Assemblies

	#----------------------------------------------
	#region Hide PowerShell Window
	#----------------------------------------------
	If (!$Nohide) {
		# Running a powershell command with windowstyle hidden in the current terminal will hide the current terminal
		powershell.exe -windowstyle hidden -Command ""
	}
	#endregion Hide PowerShell Window

	#----------------------------------------------
	#region Styling
	#----------------------------------------------
	[System.Windows.Forms.Application]::EnableVisualStyles()
	# Array of all the buttons in the script
	$arrButtons = @()
	# Toolbar buttons, currently only in use on the 'general' page
	$arrToolbarButtons = @()
	# Array of all the toolstrip items
	$arrToolStripItems = @()
	# Array of all the buttons used to manipulate the output field
	$arrOutputButtons = @()
	# Array of the buttons on the network tabpage
	$arrTabPageNetworkButtons = @()
	# Array of the different Tab Pages
	$arrTabPages = @()
	# Array of the different panels
	$arrPanels = @()
	# Array of all group boxes
	$arrGroupboxes = @()
	#endregion Styling

	#----------------------------------------------
	#region Generated Form Objects
	#----------------------------------------------
	$form_MainForm = New-Object 'System.Windows.Forms.Form'
	$richtextbox_output = New-Object 'System.Windows.Forms.RichTextBox'
	$panel_RTBButtons = New-Object 'System.Windows.Forms.Panel'
	$button_outputClear = New-Object 'System.Windows.Forms.Button'
	$button_ExportRTF = New-Object 'System.Windows.Forms.Button'
	$button_outputCopy = New-Object 'System.Windows.Forms.Button'
	$tabcontrol_computer = New-Object 'System.Windows.Forms.TabControl'
	$tabpage_general = New-Object 'System.Windows.Forms.TabPage'
	$buttonSendCommand = New-Object 'System.Windows.Forms.Button'
	$groupbox_ManagementConsole = New-Object 'System.Windows.Forms.GroupBox'
	$button_mmcCompmgmt = New-Object 'System.Windows.Forms.Button'
	$buttonServices = New-Object 'System.Windows.Forms.Button'
	$buttonShares = New-Object 'System.Windows.Forms.Button'
	$buttonEventVwr = New-Object 'System.Windows.Forms.Button'
	$button_GPupdate = New-Object 'System.Windows.Forms.Button'
	$button_Applications = New-Object 'System.Windows.Forms.Button'
	$button_ping = New-Object 'System.Windows.Forms.Button'
	$button_remot = New-Object 'System.Windows.Forms.Button'
	$buttonRemoteAssistance = New-Object 'System.Windows.Forms.Button'
	$button_PsRemoting = New-Object 'System.Windows.Forms.Button'
	$buttonC = New-Object 'System.Windows.Forms.Button'
	$button_networkconfig = New-Object 'System.Windows.Forms.Button'
	$button_Restart = New-Object 'System.Windows.Forms.Button'
	$button_Shutdown = New-Object 'System.Windows.Forms.Button'
	$groupbox_Notify = New-Object 'System.Windows.Forms.GroupBox'
	$button_sendMsg = New-Object 'System.Windows.Forms.Button'
	$button_promptReboot = New-Object 'System.Windows.Forms.Button'
	$button_scheduleReboot = New-Object 'System.Windows.Forms.Button'
	$tabpage_ComputerOSSystem = New-Object 'System.Windows.Forms.TabPage'
	$groupbox_UsersAndGroups = New-Object 'System.Windows.Forms.GroupBox'
	$button_UsersGroupLocalUsers = New-Object 'System.Windows.Forms.Button'
	$button_UsersGroupDelLocalUsers = New-Object 'System.Windows.Forms.Button'
	$button_UsersGroupLocalGroups = New-Object 'System.Windows.Forms.Button'
	$button_UserADGroups = New-Object 'System.Windows.Forms.Button'
	$button_ComputerADGroups = New-Object 'System.Windows.Forms.Button'
	$groupbox_software = New-Object 'System.Windows.Forms.GroupBox'
	$groupbox_ComputerDescription = New-Object 'System.Windows.Forms.GroupBox'
	$button_ComputerDescriptionChange = New-Object 'System.Windows.Forms.Button'
	$button_ComputerDescriptionQuery = New-Object 'System.Windows.Forms.Button'
	$groupbox2 = New-Object 'System.Windows.Forms.GroupBox'
	$buttonReportingEventslog = New-Object 'System.Windows.Forms.Button'
	$button_HotFix = New-Object 'System.Windows.Forms.Button'
	$buttonWindowsUpdateLog = New-Object 'System.Windows.Forms.Button'
	$groupbox_RemoteDesktop = New-Object 'System.Windows.Forms.GroupBox'
	$button_RDPDisable = New-Object 'System.Windows.Forms.Button'
	$button_RDPEnable = New-Object 'System.Windows.Forms.Button'
	$buttonApplications = New-Object 'System.Windows.Forms.Button'
	$button_PageFile = New-Object 'System.Windows.Forms.Button'
	$button_HostsFile = New-Object 'System.Windows.Forms.Button'
	$button_Pend_Reboot = New-Object 'System.Windows.Forms.Button'
	$button_StartupCommand = New-Object 'System.Windows.Forms.Button'
	$groupbox_Hardware = New-Object 'System.Windows.Forms.GroupBox'
	$button_MotherBoard = New-Object 'System.Windows.Forms.Button'
	$button_Processor = New-Object 'System.Windows.Forms.Button'
	$button_Memory = New-Object 'System.Windows.Forms.Button'
	$button_SystemType = New-Object 'System.Windows.Forms.Button'
	$button_Printers = New-Object 'System.Windows.Forms.Button'
	$button_USBDevices = New-Object 'System.Windows.Forms.Button'
	$tabpage_network = New-Object 'System.Windows.Forms.TabPage'
	$button_ConnectivityTesting = New-Object 'System.Windows.Forms.Button'
	$button_NIC = New-Object 'System.Windows.Forms.Button'
	$button_networkIPConfig = New-Object 'System.Windows.Forms.Button'
	$button_networkTestPort = New-Object 'System.Windows.Forms.Button'
	$button_networkRouteTable = New-Object 'System.Windows.Forms.Button'
	$tabpage_processes = New-Object 'System.Windows.Forms.TabPage'
	$buttonCommandLineGridView = New-Object 'System.Windows.Forms.Button'
	$button_processAll = New-Object 'System.Windows.Forms.Button'
	$buttonCommandLine = New-Object 'System.Windows.Forms.Button'
	$groupbox1 = New-Object 'System.Windows.Forms.GroupBox'
	$textbox_processName = New-Object 'System.Windows.Forms.TextBox'
	$label_processEnterAProcessName = New-Object 'System.Windows.Forms.Label'
	$button_processTerminate = New-Object 'System.Windows.Forms.Button'
	$button_process100MB = New-Object 'System.Windows.Forms.Button'
	$button_ProcessGrid = New-Object 'System.Windows.Forms.Button'
	$button_processOwners = New-Object 'System.Windows.Forms.Button'
	$button_processLastHour = New-Object 'System.Windows.Forms.Button'
	$tabpage_services = New-Object 'System.Windows.Forms.TabPage'
	$button_servicesNonStandardUser = New-Object 'System.Windows.Forms.Button'
	$button_mmcServices = New-Object 'System.Windows.Forms.Button'
	$button_servicesAutoNotStarted = New-Object 'System.Windows.Forms.Button'
	$groupbox_Service_QueryStartStop = New-Object 'System.Windows.Forms.GroupBox'
	$textbox_servicesAction = New-Object 'System.Windows.Forms.TextBox'
	$button_servicesRestart = New-Object 'System.Windows.Forms.Button'
	$label_servicesEnterAServiceName = New-Object 'System.Windows.Forms.Label'
	$button_servicesQuery = New-Object 'System.Windows.Forms.Button'
	$button_servicesStart = New-Object 'System.Windows.Forms.Button'
	$button_servicesStop = New-Object 'System.Windows.Forms.Button'
	$button_servicesRunning = New-Object 'System.Windows.Forms.Button'
	$button_servicesAll = New-Object 'System.Windows.Forms.Button'
	$button_servicesGridView = New-Object 'System.Windows.Forms.Button'
	$button_servicesAutomatic = New-Object 'System.Windows.Forms.Button'
	$tabpage_diskdrives = New-Object 'System.Windows.Forms.TabPage'
	$button_DiskUsage = New-Object 'System.Windows.Forms.Button'
	$button_DiskPhysical = New-Object 'System.Windows.Forms.Button'
	$button_DiskPartition = New-Object 'System.Windows.Forms.Button'
	$button_DiskLogical = New-Object 'System.Windows.Forms.Button'
	$button_DiskMountPoint = New-Object 'System.Windows.Forms.Button'
	$button_DiskRelationship = New-Object 'System.Windows.Forms.Button'
	$button_DiskMappedDrive = New-Object 'System.Windows.Forms.Button'
	$tabpage_shares = New-Object 'System.Windows.Forms.TabPage'
	$button_mmcShares = New-Object 'System.Windows.Forms.Button'
	$button_SharesGrid = New-Object 'System.Windows.Forms.Button'
	$button_Shares = New-Object 'System.Windows.Forms.Button'
	$tabpage_eventlog = New-Object 'System.Windows.Forms.TabPage'
	$button_RebootHistory = New-Object 'System.Windows.Forms.Button'
	$button_mmcEvents = New-Object 'System.Windows.Forms.Button'
	$button_EventsSearch = New-Object 'System.Windows.Forms.Button'
	$button_EventsLogNames = New-Object 'System.Windows.Forms.Button'
	$button_EventsLast20 = New-Object 'System.Windows.Forms.Button'
	$tabpage_ExternalTools = New-Object 'System.Windows.Forms.TabPage'
	$groupbox3 = New-Object 'System.Windows.Forms.GroupBox'
	$label_SYDI = New-Object 'System.Windows.Forms.Label'
	$combobox_sydi_format = New-Object 'System.Windows.Forms.ComboBox'
	$textbox_sydi_arguments = New-Object 'System.Windows.Forms.TextBox'
	$button_SYDIGo = New-Object 'System.Windows.Forms.Button'
	$button_Rwinsta = New-Object 'System.Windows.Forms.Button'
	$button_Qwinsta = New-Object 'System.Windows.Forms.Button'
	$button_MsInfo32 = New-Object 'System.Windows.Forms.Button'
	$button_Telnet = New-Object 'System.Windows.Forms.Button'
	$button_DriverQuery = New-Object 'System.Windows.Forms.Button'
	$button_SystemInfoexe = New-Object 'System.Windows.Forms.Button'
	$button_PAExec = New-Object 'System.Windows.Forms.Button'
	$button_psexec = New-Object 'System.Windows.Forms.Button'
	$textbox_networktracertparam = New-Object 'System.Windows.Forms.TextBox'
	$button_networkTracert = New-Object 'System.Windows.Forms.Button'
	$button_networkNsLookup = New-Object 'System.Windows.Forms.Button'
	$button_networkPing = New-Object 'System.Windows.Forms.Button'
	$textbox_networkpathpingparam = New-Object 'System.Windows.Forms.TextBox'
	$textbox_pingparam = New-Object 'System.Windows.Forms.TextBox'
	$button_networkPathPing = New-Object 'System.Windows.Forms.Button'
	$groupbox_ComputerName = New-Object 'System.Windows.Forms.GroupBox'
	$label_UptimeStatus = New-Object 'System.Windows.Forms.Label'
    $label_CurrentUser = New-Object 'System.Windows.Forms.Label'
	$textbox_computername = New-Object 'System.Windows.Forms.TextBox'
	$label_OSStatus = New-Object 'System.Windows.Forms.Label'
	$button_Check = New-Object 'System.Windows.Forms.Button'
	$label_PingStatus = New-Object 'System.Windows.Forms.Label'
	$label_Ping = New-Object 'System.Windows.Forms.Label'
	$label_PSRemotingStatus = New-Object 'System.Windows.Forms.Label'
	$label_Uptime = New-Object 'System.Windows.Forms.Label'
	$label_User = New-Object 'System.Windows.Forms.Label'
	$label_RDPStatus = New-Object 'System.Windows.Forms.Label'
	$label_OS = New-Object 'System.Windows.Forms.Label'
	$label_PermissionStatus = New-Object 'System.Windows.Forms.Label'
	$label_Permission = New-Object 'System.Windows.Forms.Label'
	$label_PSRemoting = New-Object 'System.Windows.Forms.Label'
	$label_RDP = New-Object 'System.Windows.Forms.Label'
	$richtextbox_Logs = New-Object 'System.Windows.Forms.RichTextBox'
	$statusbar1 = New-Object 'System.Windows.Forms.StatusBar'
	$menustrip_principal = New-Object 'System.Windows.Forms.MenuStrip'
	$ToolStripMenuItem_AdminArsenal = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_CommandPrompt = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_Powershell = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_Notepad = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_RemoteDesktopConnection = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_localhost = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_compmgmt = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_taskManager = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_services = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_regedit = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_mmc = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_shutdownGui = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_registeredSnappins = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_about = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_AboutInfo = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$contextmenustripServer = New-Object 'System.Windows.Forms.ContextMenuStrip'
	$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_Tools = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_ConsolesMMC = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_Ping = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_RDP = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_compmgmt = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_services = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_eventvwr = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_InternetExplorer = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_TerminalAdmin = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_ADSearchDialog = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_ADPrinters = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_netstatsListening = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_systemInformationMSinfo32exe = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_otherLocalTools = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_addRemovePrograms = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_administrativeTools = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_authprizationManager = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_certificateManager = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_devicemanager = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_addRemoveProgramsWindowsFeatures = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$toolstripseparator1 = New-Object 'System.Windows.Forms.ToolStripSeparator'
	$toolstripseparator3 = New-Object 'System.Windows.Forms.ToolStripSeparator'
	$ToolStripMenuItem_systemproperties = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$toolstripseparator4 = New-Object 'System.Windows.Forms.ToolStripSeparator'
	$toolstripseparator5 = New-Object 'System.Windows.Forms.ToolStripSeparator'
	$ToolStripMenuItem_Wordpad = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_sharedFolders = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_performanceMonitor = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_networkConnections = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_groupPolicyEditor = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_localUsersAndGroups = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_diskManagement = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_localSecuritySettings = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_componentServices = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_scheduledTasks = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_PowershellISE = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_hostsFile = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_netstat = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$errorprovider1 = New-Object 'System.Windows.Forms.ErrorProvider'
	$tooltipinfo = New-Object 'System.Windows.Forms.ToolTip'
	$ToolStripMenuItem_sysInternals = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_adExplorer = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_hostsFileGetContent = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_Qwinsta = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_rwinsta = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_GeneratePassword = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_scripts = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$ToolStripMenuItem_WMIExplorer = New-Object 'System.Windows.Forms.ToolStripMenuItem'
    $ToolStripMenuItem_ADUSERUnlocker = New-Object 'System.Windows.Forms.ToolStripMenuItem'
	$timerCheckJob = New-Object 'System.Windows.Forms.Timer'
	$InitialFormWindowState = New-Object 'System.Windows.Forms.FormWindowState'
	#endregion Generated Form Objects


	#----------------------------------------------
	# User Generated Script
	#----------------------------------------------
	#################################
	######### CONFIGURATION #########
	#################################
	
	# LazyAdminKit information
	$ApplicationName		= "LazyWinAdmin"
	$ApplicationVersion		= $Version
	$ApplicationLastUpdate	= $LastUpdate
	
	# Author Information
	$AuthorName			= "Francois-Xavier Cat"
	$AuthorEmail 		= "info@lazywinadmin.com"
	$AuthorBlogName 	= "LazyWinAdmin.com"
	$AuthorBlogURL 		= "http://www.lazywinadmin.com"
	$AuthorTwitter 		= "@LazyWinAdm"
    $AuthorTwitterURL	= "http://twitter.com/LazyWinAdm"
	
	# Text to show in the Status Bar when the form load
	$StatusBarStartUp	= "Originally created by $AuthorName - $AuthorEmail"
	
	# Title of the MainForm
	$domain				= $env:userdomain.ToUpper()
	$MainFormTitle 		= "$ApplicationName $ApplicationVersion - Last Update: $ApplicationLastUpdate - $domain\$env:username"
	
	# Default Error Action
	If (!$Debug) {
		$ErrorActionPreference 	= "SilentlyContinue"
	}
	
	# Script Paths
	$ToolsFolder 	= "$PSScriptRoot\tools"
	$ScriptsFolder 	= "$PSScriptRoot\scripts"
	$SavePath 		= "$env:userprofile\desktop"
	
	# Reset Error
	
		$Error = "" # This is needed to suppress unnecessary errors from showing in the console
	

	# Computers List Source
	Set-Location $PSScriptRoot
	$ComputersList_File = $PSScriptRoot + "\computers.txt"
	$ComputersList	= Get-Content $ComputersList_File
		
	# RichTextBox LOGS form
	#  Message to show when the form load
	$RichTexBoxLogsDefaultMessage="Welcome on $ApplicationName $LAKVersion - Visit my Blog: $AuthorBlogURL"
	
	# Current Operating System Information
	$current_OS 			= Get-WmiObject Win32_OperatingSystem
	$current_OS_caption 	= $current_OS.caption

	# Default command for sendcommand function
	$script:PreviousCommand = "ipconfig /all"
		
	###############
	$OnLoadFormEvent={
		# Set the status bar name
		$statusbar1.Text = $StatusBarStartUp
		
		
		$form_MainForm.Text = $MainFormTitle
		$textbox_computername.Text = $env:COMPUTERNAME
		Add-Logs -text $RichTexBoxLogsDefaultMessage
		
		# Load the Computers list from $ComputersList
		if (Test-Path $ComputersList_File){
			Add-logs -text "Computers List loaded - $($ComputersList.Count) Items - File: $ComputersList_File" -ErrorAction 'SilentlyContinue'
			$textbox_computername.AutoCompleteCustomSource.AddRange($ComputersList)
			}#end if (Test-Path $ComputersList
			else {
				Add-Logs -text "No Computers List found at the following location: $ComputersList_File"  -ErrorAction 'SilentlyContinue'
			}
		
		# Verify External Tools are presents
		
		# PSExec.exe
		if(Test-Path "$ToolsFolder\psexec.exe" -ErrorAction 'SilentlyContinue'){
			$button_psexec.ForeColor = '#88FF88'
			Add-Logs -text "External Tools check - PsExec.exe found" -ErrorAction 'SilentlyContinue'}
		else {$button_psexec.enabled = $false;Add-Logs -text "External Tools check - PsExec.exe not found - Button Disabled"  -ErrorAction 'SilentlyContinue'}
		
		# PAExec.exe
		if(Test-Path "$ToolsFolder\paexec.exe" -ErrorAction 'SilentlyContinue' ){
			$button_PAExec.ForeColor = '#88FF88'
			Add-Logs -text "External Tools check - PAExec.exe found" -ErrorAction 'SilentlyContinue'}
		else {$button_paexec.enabled = $false;Add-Logs -text "External Tools check - PAExec.exe not found - Button Disabled"  -ErrorAction 'SilentlyContinue'}
		
		# ADExplorer.exe
		if(Test-Path "$ToolsFolder\adexplorer.exe" -ErrorAction 'SilentlyContinue'){
			$ToolStripMenuItem_adExplorer.ForeColor = '#88FF88'
			Add-Logs -text "External Tools check - ADExplorer.exe found" -ErrorAction 'SilentlyContinue'}
		else {$ToolStripMenuItem_adExplorer.enabled = $false;Add-Logs -text "External Tools check - ADExplorer.exe not found - Button Disabled"  -ErrorAction 'SilentlyContinue'}
		
		# MSRA.exe (Remote Assistance)
		if(Test-Path "$env:systemroot/system32/msra.exe" -ErrorAction 'SilentlyContinue'){
			Add-Logs -text "External Tools check - MSRA.exe found" -ErrorAction 'SilentlyContinue'}
		else {$buttonRemoteAssistance.enabled = $false;Add-Logs -text "External Tools check - MSRA.exe not found (Remote Assistance) - Button Disabled" -ErrorAction 'SilentlyContinue'}
		
		# Telnet.exe
		if(Test-Path "$env:systemroot/system32/telnet.exe" -ErrorAction 'SilentlyContinue'){
			Add-Logs -text "External Tools check - Telnet.exe found" -ErrorAction 'SilentlyContinue'}
		else {$button_Telnet.enabled = $false;Add-Logs -text "External Tools check - Telnet.exe not found - Button Disabled" -ErrorAction 'SilentlyContinue'}
		
		# SystemInfo.exe
		if(Test-Path "$env:systemroot/system32/systeminfo.exe" -ErrorAction 'SilentlyContinue'){
			Add-Logs -text "External Tools check - Systeminfo.exe found"}
		else {$button_SystemInfoexe.enabled = $false;Add-Logs -text "External Tools check - Systeminfo.exe not found - Button Disabled"}
		
		# MSInfo32.exe
		if(Test-Path "$env:programfiles\Common Files\Microsoft Shared\MSInfo\msinfo32.exe" -ErrorAction 'SilentlyContinue'){
			Add-Logs -text "External Tools check - msinfo32.exe found"}
		else {$button_MsInfo32.enabled = $false;Add-Logs -text "External Tools check - msinfo32.exe not found - Button Disabled"}
		
		# DriverQuery.exe
			if(Test-Path "$env:systemroot/system32/driverquery.exe" -ErrorAction 'SilentlyContinue'){
			Add-Logs -text "External Tools check - Driverquery.exe found"}
		else {$button_DriverQuery.enabled = $false;Add-Logs -text "External Tools check - Driverquery.exe not found - Button Disabled"}
		
		# SCRIPTS
		
		# WMIExplore.ps1 - http://thepowershellguy.com
		if(Test-Path "$ScriptsFolder\WMIExplorer.ps1" -ErrorAction 'SilentlyContinue'){
			$ToolStripMenuItem_WMIExplorer.ForeColor = '#88FF88'
			Add-Logs -text "External Script check - WMIExplorer.ps1 found"}
		else {	$ToolStripMenuItem_WMIExplorer.ForeColor = 'Red';$ToolStripMenuItem_WMIExplorer.enabled = $false
				Add-Logs -text "External Script check - WMIExplorer.ps1 not found - Button Disabled"}
		
		# ADUserUnlocker.ps1 - https://gallery.technet.microsoft.com/WinForm-Active-Directory-a3771370
		if(Test-Path "$ScriptsFolder\ADUSERUnlocker.ps1" -ErrorAction 'SilentlyContinue'){
			$ToolStripMenuItem_ADUSERUnlocker.ForeColor = '#88FF88'
			Add-Logs -text "External Script check - ADUSERUnlocker.ps1 found"}
		else {	$ToolStripMenuItem_ADUSERUnlocker.ForeColor = 'Red';$ToolStripMenuItem_ADUSERUnlocker.enabled = $false
				Add-Logs -text "External Script check - ADUSERUnlocker.ps1 not found - Button Disabled"}

		# SYDI-Server.vbs - http://sydiproject.com/
		if(Test-Path "$ScriptsFolder\sydi-server.vbs" -ErrorAction 'SilentlyContinue'){
			$button_SYDIGo.ForeColor = '#88FF88'
			Add-Logs -text "External Script check - Sydi-Server.vbs found"}
		else {
			$button_SYDIGo.enabled = $false
			$combobox_sydi_format.Enabled = $false
			$textbox_sydi_arguments.Enabled = $false
			Add-Logs -text "External Script check - Sydi-Server.vbs not found - Button Disabled"}
	}		
	$button_remot_Click={
		Get-ComputerTxtBox
		add-logs -text "$ComputerName - Remote Desktop Connection"
		$port=":3389"
		$command = "mstsc"
		$argument = "/v:$computername$port /admin"
		Start-Process $command $argument
	}
	
	$ToolStripMenuItem_registeredSnappins_Click={
		add-logs -text "Localhost - Registered Snappin"
		$snappins = Get-PSSnapin -Registered |Out-String
		if ($snappins -eq ""){Add-RichTextBox "No Powershell Snappin registered"}
		$richtextbox_output.SelectionBackColor = [System.Drawing.Color]::black
		$richtextbox_output.SelectionColor = [System.Drawing.Color]::Red
		Add-RichTextBox $snappins
	}
	
	$button_outputClear_Click={Clear-RichTextBox}
	$ToolStripMenuItem_AboutInfo_Click={Start-About_pff}
	
	$button_mmcCompmgmt_Click={
		
		Get-ComputerTxtBox
		#disable the button to avoid multiple click
		$button_mmcCompmgmt.Enabled = $false
		if (($ComputerName -like "localhost") -or ($ComputerName -like ".") -or ($ComputerName -like "127.0.0.1") -or ($ComputerName -like "$env:computername")) {
			Add-logs -text "Localhost - Computer Management MMC (compmgmt.msc)"
			$command="compmgmt.msc"
			Start-Process $command 
			}
		else {
			Add-logs -text "$ComputerName - Computer Management MMC (compmgmt.msc /computer:$Computername)"
			$command="compmgmt.msc"
			$arguments = "/computer:$computername"
			Start-Process $command $arguments}
		#Enable the button
		$button_mmcCompmgmt.Enabled = $true
	}
	
	$button_Shares_Click={
		Get-ComputerTxtBox
		Add-Logs -text "$ComputerName - Shares list"
		$SharesList = Get-WmiObject win32_share -computer $ComputerName|Sort-Object name|Format-Table -AutoSize| Out-String -Width $richtextbox_output.Width
		Add-RichTextBox -text $SharesList
		}
	
	$button_mmcServices_Click={
		Get-ComputerTxtBox
		Add-logs -text "$ComputerName - Services MMC (services.msc /computer:$ComputerName)"
		$command = "services.msc"
		$arguments = "/computer:$computername"
		Start-Process $command $arguments 
	}
	
	$button_servicesRunning_Click={
		Get-ComputerTxtBox
		Add-Logs -text "$ComputerName - Services - Status: Running"
		if ($ComputerName -eq "localhost") {$ComputerName = "."}
		$Services_running = Get-Service -ComputerName $ComputerName| Where-Object { $_.Status -eq "Running" }|Format-Table -AutoSize |Out-String
		Add-RichTextBox -text $Services_running
	}
	
	$button_process100MB_Click={
		#Clear-RichTextBox
		Get-ComputerTxtBox
		Add-Logs -text "$ComputerName - Processes >100MB"
		if ($ComputerName -eq "localhost") {$ComputerName = "."}
		$owners = @{}
		Get-WmiObject win32_process -ComputerName $ComputerName |ForEach-Object {$owners[$_.handle] = $_.getowner().user}
		$Processes_Over100MB = Get-Process -ComputerName $ComputerName| Where-Object { $_.WorkingSet -gt 100mb }|Select-Object Handles,NPM,PM,WS,VM,CPU,ID,ProcessName,@{l="Owner";e={$owners[$_.id.tostring()]}}|Sort-Object ws|Format-Table -AutoSize|Out-String
		Add-RichTextBox $Processes_Over100MB
	}
	
	$button_mmcEvents_Click={
		Get-ComputerTxtBox
		Add-logs -text "$ComputerName - Event Viewer MMC (eventvwr $Computername)"
		$command="eventvwr"
		$arguments = "$ComputerName"
		Start-Process $command $arguments
	}
	
	$button_EventsLast20_Click={
		#Clear-RichTextBox
		Get-ComputerTxtBox
		Add-Logs "$ComputerName - EventLog - Last 20"
		if ($ComputerName -like "localhost"){
			$Events_Last20Sytem = Get-EventLog -Newest 20 | Select-Object Index,EventID,Source,Message,MachineName,UserName,TimeGenerated,TimeWritten |Format-List|Out-String
		Add-RichTextBox $Events_Last20Sytem
		}
		else {
		$Events_Last20Sytem = Get-EventLog -Newest 20 -ComputerName $ComputerName | Select-Object Index,EventID,Source,Message,MachineName,UserName,TimeGenerated,TimeWritten |Format-List|Out-String
		Add-RichTextBox $Events_Last20Sytem}
		
	}
	
	
	
	$button_EventsSearch_Click={
		#Clear-RichTextBox
		Get-ComputerTxtBox
		Add-Logs "$ComputerName - EventLog - Search"
		if ($ComputerName -like "localhost"){$SearchEventText =  Show-Inputbox -message "Enter the text to search" -title "$ComputerName - Search in events" -default "error"
			if ($SearchEventText -ne ""){
				$SearchEvent = Get-EventLog | Where-Object { $_.Message -match "$SearchEventText" }|Format-List * |Out-String
				Add-RichTextBox $SearchEvent
			}
		}
		else {$SearchEventText =  Show-Inputbox -message "Enter the text to search" -title "$ComputerName - Search in events" -default "error"
			if ($SearchEventText -ne ""){
				$SearchEvent = Get-EventLog -ComputerName $ComputerName| Where-Object { $_.Message -match "$SearchEventText" }|Format-List|Out-String
				Add-RichTextBox $SearchEvent
			}
		}
	}
	
	$button_servicesAutomatic_Click={
		#Clear-RichTextBox
		Get-ComputerTxtBox
		Add-Logs -text "$ComputerName - Services - StartMode:Automatic"
		if ($ComputerName -eq "localhost") {$ComputerName = "."}
		$Services_StartModeAuto = Get-WmiObject Win32_Service -ComputerName $ComputerName -Filter "startmode='auto'" |Select-Object DisplayName,Name,ProcessID,StartMode,State|Format-Table -AutoSize|out-string
		Add-RichTextBox $Services_StartModeAuto
	}
	
	$button_servicesQuery_Click={
		#Button Types 
		#
		#$a = new-object -comobject wscript.shell
		#$intAnswer = $a.popup("Do you want to continue ?",0,"Shutdown",4)
		#if ($intAnswer -eq 6){do something}
		#Value  Description  
		#0 Show OK button.
		#1 Show OK and Cancel buttons.
		#2 Show Abort, Retry, and Ignore buttons.
		#3 Show Yes, No, and Cancel buttons.
		#4 Show Yes and No buttons.
		#5 Show Retry and Cancel buttons.
		#Clear-RichTextBox
		Get-ComputerTxtBox
		$a = new-object -comobject wscript.shell
		Add-Logs "$COMPUTERNAME - Query Service"
		#$Service_query = Read-Host "Enter the Service Name to Query `n"
		$Service_query = $textbox_servicesAction.text
		$intAnswer = $a.popup("Do you want to continue ?",0,"$ComputerName - Query Service: $Service_query",4)
		if (($ComputerName -like "localhost") -and ($intAnswer -eq 6)) {
			Add-Logs "$COMPUTERNAME - Checking Service $Service_query ..."
			$Service_query_return=Get-WmiObject Win32_Service -Filter "Name='$Service_query'" |Out-String
			Add-Logs "$COMPUTERNAME - Command Sent! Service $Service_query"
			Add-RichTextBox $Service_query_return
			Add-Logs -Text "$ComputerName - Query Service $Service_query - Done."
		}
		else {
			if($intAnswer -eq 6){
				Add-Logs "$COMPUTERNAME - Checking the Service $Service_query ..."
				$Service_query_return=Get-WmiObject -computername $ComputerName Win32_Service -Filter "Name='$Service_query'" |Out-String
				Add-Logs "$COMPUTERNAME - Command Sent! Service $Service_query"
				Add-RichTextBox $Service_query_return
				Add-Logs -Text "$ComputerName - Query Service $Service_query - Done."
			}
		}
		#Add-Logs $($error[0].Exception.Message)
	}
	
	$button_servicesAll_Click={
		#Clear-RichTextBox
		Get-ComputerTxtBox
		Add-logs -text "$ComputerName - Services - All Services + Owners"
		if ($ComputerName -eq "localhost") {$ComputerName = "."}
		$Services_StartModeAuto = Get-WmiObject Win32_Service -ComputerName $ComputerName |Select-Object Name,ProcessID,StartMode,State,@{Name="Owner";Expression={$_.StartName}}|Format-Table -AutoSize|out-string
		Add-RichTextBox $Services_StartModeAuto
		
	}
	
	$button_servicesStop_Click={
		#Button Types 
		#
		#$a = new-object -comobject wscript.shell
		#$intAnswer = $a.popup("Do you want to continue ?",0,"Shutdown",4)
		#if ($intAnswer -eq 6){do something}
		#Value  Description  
		#0 Show OK button.
		#1 Show OK and Cancel buttons.
		#2 Show Abort, Retry, and Ignore buttons.
		#3 Show Yes, No, and Cancel buttons.
		#4 Show Yes and No buttons.
		#5 Show Retry and Cancel buttons.
		#Clear-RichTextBox
		Get-ComputerTxtBox
		#Add-RichTextBox "# SERVICES - STOP SERVICE - COMPUTERNAME: $ComputerName `n`n"
		Add-logs -text "$ComputerName - Stop Service"
		#$Service_query = Read-Host "Enter the Service Name to Stop `n"
		$Service_query = $textbox_servicesAction.text
		Add-logs -text "$ComputerName - Service to Stop: $Service_query"
		$a = new-object -comobject wscript.shell
		$intAnswer = $a.popup("Do you want to continue ?",0,"$ComputerName - Stop Service: $Service_query",4)
		if (($ComputerName -like "localhost") -and ($intAnswer -eq 6)) {
			Add-logs -text "$ComputerName - Stopping Service: $Service_query ..."
			$Service_query_return=Get-WmiObject Win32_Service -Filter "Name='$Service_query'"
			$Service_query_return.stopservice()
			Add-Logs -Text "$ComputerName - Command Sent! $Service_query should be stopped"
			Add-RichTextBox $Service_query_return
			Add-Logs -Text "$ComputerName - Checking the status of $Service_Query ..."
			Start-Sleep -Milliseconds 1000
			$Service_query_result=Get-WmiObject Win32_Service -Filter "Name='$Service_query'"|Out-String
			Add-RichTextBox $Service_query_result
			Add-Logs -Text "$ComputerName - Stop Service $Service_query - Done."
		}#end IF
		else {
			if ($intAnswer -eq 6){
				Add-logs -text "$ComputerName - Stopping Service: $Service_query ..."
				$Service_query_return=Get-WmiObject Win32_Service -computername $ComputerName -Filter "Name='$Service_query'"
				$Service_query_return.stopservice()
				Add-Logs -Text "$ComputerName - Command Sent! $Service_query should be stopped"
				Add-RichTextBox $Service_query_return
				Add-Logs -Text "$ComputerName - Checking the status of $Service_Query ..."
				Start-Sleep -Milliseconds 1000
				$Service_query_result=Get-WmiObject Win32_Service -computername $ComputerName -Filter "Name='$Service_query'"|Out-String
				Add-RichTextBox $Service_query_result
				Add-Logs -Text "$ComputerName - Stop Service $Service_query - Done."
			}#end IF
		}#end ELSE
	}
	
	$button_DiskPhysical_Click={
		#Clear-RichTextBox
		Get-ComputerTxtBox
		Add-logs -text "$ComputerName - Hard Drive - Physical Disk"
		if ($ComputerName -eq "localhost") {$ComputerName = "."}
		$Disks_Physical = Get-WmiObject Win32_DiskDrive -ComputerName $ComputerName|Select-Object DeviceID, `
	    Model,`
	    Manufacturer,`
	    @{Name="SizeGB";Expression={$_.Size/1GB}}, `
	    Caption, `
	    Partitions, `
	    SystemName,`
	    Status,`
	    InterfaceType,`
	    MediaType,`
	    SerialNumber,`
	    SCSIBus,SCSILogicalUnit,SCSIPort,SCSITargetId| Format-List |Out-String
		Add-RichTextBox $Disks_Physical
		
	}
	
	$button_DiskLogical_Click={
		#Clear-RichTextBox
		Get-ComputerTxtBox
		Add-logs -text "$ComputerName - Hard Drive - Logical Disk"
		if ($ComputerName -eq "localhost") {$ComputerName = "."}
		$Disks_Logical=Get-WMIObject Win32_LogicalDisk -ComputerName $ComputerName| Select-Object DeviceId,`
			DriveType,`
			@{Name="DriveTypeInfo";Expression={switch ($_.DriveType){0{"Unknown"}1{"No Root Directory"}2{"Removable Disk"}3{"Local Disk"}4{"Network Drive"}5{"Compact Disc"}6{"RAM Disk"}}}},`
			FileSystem,`
			@{Name="FreeSpaceGB";Expression={$_.FreeSpace/1GB}},`
			@{Name="SizeGB";Expression={$_.Size/1GB}},`
			@{Name="%Free";Expression={((100*($_.FreeSpace))/$_.Size)}}, 
			@{Name="%Usage";Expression={((($_.size) - ($_.Freespace))*100)/$_.size}}, 
			VolumeName,`
			SystemName,`
			Description,`
			InstallDate,`
			Compressed,`
			VolumeDirty,`
			VolumeSerialNumber|Format-List | Out-String
		Add-RichTextBox $Disks_Logical
		
	}
	
	$button_EventsLogNames_Click={
		Get-ComputerTxtBox
		Add-Logs -text "$ComputerName - EventLog - LogNames list"
		if ($ComputerName -eq "localhost") {
			$EventsLog = Get-EventLog -list |Format-List|Out-String
			Add-RichTextBox $EventsLog	
		}
		else {
			$EventsLog = Get-EventLog -list -ComputerName $ComputerName |Format-List|Out-String
			Add-RichTextBox $EventsLog
		}
	}
	
	$button_servicesStart_Click={
		#Button Types 
		#
		#$a = new-object -comobject wscript.shell
		#$intAnswer = $a.popup("Do you want to continue ?",0,"Shutdown",4)
		#if ($intAnswer -eq 6){do something}
		#Value  Description  
		#0 Show OK button.
		#1 Show OK and Cancel buttons.
		#2 Show Abort, Retry, and Ignore buttons.
		#3 Show Yes, No, and Cancel buttons.
		#4 Show Yes and No buttons.
		#5 Show Retry and Cancel buttons.
		Get-ComputerTxtBox
		Add-logs -text "$ComputerName - Start Service"
		$Service_query = $textbox_servicesAction.text
		Add-logs -text "$ComputerName - Service to start: $Service_query"
		$a = new-object -comobject wscript.shell
		$intAnswer = $a.popup("Do you want to continue ?",0,"$ComputerName - Start Service: $Service_query",4)
		if (($ComputerName -like "localhost") -and ($intAnswer -eq 6)) {
			Add-logs -text "$ComputerName - Starting Service: $Service_query ..."
			$Service_query_return=Get-WmiObject Win32_Service -Filter "Name='$Service_query'"
			$Service_query_return.startservice()
			Add-Logs -Text "$ComputerName - Command Sent! $Service_query should be started"
			Add-RichTextBox $Service_query_return
			Add-Logs -Text "$ComputerName - Checking the status of $Service_Query ..."
			Start-Sleep -Milliseconds 1000
			$Service_query_result=Get-WmiObject Win32_Service -Filter "Name='$Service_query'"|Out-String
			Add-RichTextBox $Service_query_result
			Add-Logs -Text "$ComputerName - Start Service $Service_query - Done."
		}
		else { 
			if ($intAnswer -eq 6){
				Add-logs -text "$ComputerName - Starting Service: $Service_query ..."
				$Service_query_return=Get-WmiObject Win32_Service -computername $ComputerName -Filter "Name='$Service_query'"
				$Service_query_return.startservice()
				Add-Logs -Text "$ComputerName - Command Sent! $Service_query should be started"
				Add-RichTextBox $Service_query_return
				Add-Logs -Text "$ComputerName - Checking the status of $Service_Query ..."
				Start-Sleep -Milliseconds 1000
				$Service_query_result=Get-WmiObject Win32_Service -computername $ComputerName -Filter "Name='$Service_query'"|Out-String
				Add-RichTextBox $Service_query_result
				Add-Logs -Text "$ComputerName - Start Service $Service_query - Done."
			}# IF
		}#ELSE
		#
	}
	
	$button_processOwners_Click={
		#Clear-RichTextBox
		Get-ComputerTxtBox
		Add-Logs -text "$ComputerName - Processes with owners"
		if ($ComputerName -eq "localhost") {$ComputerName = "."}
		$owners = @{}
		Get-WmiObject win32_process -ComputerName $ComputerName |ForEach-Object {$owners[$_.handle] = $_.getowner().user}
		$ProcessALL = get-process -ComputerName $ComputerName| Select-Object ProcessName,@{l="Owner";e={$owners[$_.id.tostring()]}},CPU,WorkingSet,Handles,Id|Format-Table -AutoSize|out-string
		Add-RichTextBox $ProcessALL
		
	}
	
	$button_processAll_Click={
		Get-ComputerTxtBox
		Add-Logs -text "$ComputerName - All Processes"
		if ($ComputerName -eq "localhost") {$ComputerName = "."}
		$ProcessALL = get-process -ComputerName $ComputerName|out-string
		Add-RichTextBox $ProcessALL
	}
	
	$button_ProcessGrid_Click={
		Get-ComputerTxtBox
		Add-Logs -text "$ComputerName - All Processes - GridView"
		if ($ComputerName -eq "localhost") {$ComputerName = "."}
		$owners = @{}
		Get-WmiObject win32_process -ComputerName $ComputerName |ForEach-Object {$owners[$_.handle] = $_.getowner().user}
		Get-Process -ComputerName $ComputerName| Select-Object @{l="Owner";e={$owners[$_.id.tostring()]}},*| Out-GridView
	}
	
	$button_servicesGridView_Click={
		Get-ComputerTxtBox
		Add-Logs -text "$ComputerName - All Services - GridView"
		if ($ComputerName -eq "localhost") {$ComputerName = "."}
		Get-WmiObject Win32_Service -ComputerName $ComputerName |Select-Object *,@{Name="Owner";Expression={$_.StartName}}|Out-GridView
	}
	
	$button_SharesGrid_Click={
		Get-ComputerTxtBox
		#Add-RichTextBox "$ComputerName - All Shares - GridView"
		if ($ComputerName -eq "localhost") {$ComputerName = "."}
		Get-WmiObject win32_share -computer $ComputerName |Select-Object -Property __SERVER,Name,Path,Status,Description,*|Sort-Object name| Out-GridView
	}
	
	$ToolStripMenuItem_TerminalAdmin_Click={
		if ($current_OS_caption -like "*2008*"){
			$cmd = "tsadmin.msc"
			Start-Process $cmd
		}
		else {
			Start-Process tsadmin.exe
		}
	}
	$ToolStripMenuItem_ADSearchDialog_Click={
		$cmd="$env:windir\system32\rundll32.exe"
		$param="dsquery.dll,OpenQueryWindow"
		Start-Process $cmd $param
	}
	
	$ToolStripMenuItem_ADPrinters_Click={
		$TemporaryFile = [System.IO.Path]::GetTempFileName().Replace(".tmp", ".qds")
		Add-Content $TemporaryFile "
		[CommonQuery]
		Handler=5EE6238AC231D011891C00A024AB2DBBC1
		Form=70F077B5E27ED011913F00AA00C16E65DB
		[DsQuery]
		ViewMode=0413000017
		EnableFilter=0000000000
		[Microsoft.Printers.MoreChoices]
		LocationLength=1200000012
		LocationValue=2400440079006E0061006D00690063004C006F0063006100740069006F006E002400000046
		color=0000000000
		duplex=0000000000
		stapling=0000000000
		resolution=0000000000
		speed=0100000001
		sizeLength=0100000001
		sizeValue=000000
		[Microsoft.PropertyWell]
		Items=0000000000
	"
		Start-Process $TemporaryFile
		Start-Sleep -Seconds 3
		Remove-Item -Force $TemporaryFile
	}
	
	$button_outputCopy_Click={
		
		Add-logs -text "Copying content of Logs Richtextbox to Clipboard"
		$texte = $richtextbox_output.Text
		Add-ClipBoard -text $texte}
	
	$button_ExportRTF_Click={
		$filename = [System.IO.Path]::GetTempFileName()
		# Text is unreadable in WordPad if it isn't dark
		$richtextbox_output.ForeColor = "Black"
		$richtextbox_output.SaveFile($filename)
		$richtextbox_output.ForeColor = "#CCCCCC"
		Add-logs -text "Sending RichTextBox to wordpad (RTF)..."
		Start-Process wordpad $filename
		Start-Sleep -Seconds 5
		#Remove-Item -Force $filename
	}
	
	
	$button_networkPing_Click={
		Get-ComputerTxtBox
		Add-logs -text "$ComputerName - Network - Ping"
		$cmd = "cmd"
		$param_user = $textbox_pingparam.text
		$param = "/k ping $param_user $computername"
		Start-Process $cmd $param
	}
	
	$button_networkPathPing_Click={
		Get-ComputerTxtBox
		Add-logs -text "$ComputerName - Network - PathPing"
		$cmd = "cmd"
		$param_user = $textbox_networkpathpingparam.Text
		$param = "/k pathping $param_user $computername"
		Start-Process $cmd $param
	}
	
	$button_servicesNonStandardUser_Click={
		Get-ComputerTxtBox
		Add-Logs -text "$ComputerName - Services - Non-Standard Windows Service Accounts"
		$wql = 'Select Name, DisplayName, StartName, __Server From Win32_Service WHERE ((StartName != "LocalSystem") and (StartName != "NT Authority\\LocalService") and (StartName != "NT Authority\\NetworkService"))'
		$query = Get-WmiObject -Query $wql -ComputerName $ComputerName -ErrorAction Stop | Select-Object __SERVER, StartName, Name, DisplayName|Format-Table -AutoSize |Out-String
		if ($null -eq $query){Add-RichTextBox "$Computername - All the services use Standard Windows Service Accounts"}
		else {Add-RichTextBox $query}
	}
	$button_networkTestPort_Click={
		#$error.clear()
		Get-ComputerTxtBox
		Add-logs -text "$ComputerName - Network - Test-Port"
		$port = Show-Inputbox -message "Enter a port to test" -title "$ComputerName - Test-Port" -default "80"
		if ($port -ne ""){
			#$port = Read-Host -Prompt "Enter a port to test on $ComputerName"
			$result = Test-TcpPort $ComputerName $port
			Add-RichTextBox $result	
		}
	}
	
	$button_networkNsLookup_Click={
		Get-ComputerTxtBox
		Add-logs -text "$ComputerName - Network - Nslookup"
		$cmd = "cmd"
		$param = "/k nslookup $ComputerName"
		Start-Process $cmd $param -WorkingDirectory c:\
	}
	
	$button_networkTracert_Click={
		Get-ComputerTxtBox
		Add-logs -text "$ComputerName - Network - Trace Route (Tracert)"
		$cmd = "cmd"
		$param = "/k tracert $($tb_tracert_paramuser.text) $ComputerName"
		Start-Process $cmd $param -WorkingDirectory c:\
	}
	
	$button_processLastHour_Click={
		#Clear-RichTextBox
		Get-ComputerTxtBox
		Add-Logs "$ComputerName - Processes - Processes started in last hour"
		if ($ComputerName -eq "localhost") {$ComputerName = "."}
		$owners = @{}
		Get-WmiObject win32_process -ComputerName $ComputerName|ForEach-Object {$owners[$_.handle] = $_.getowner().user}
		$ProcessALL = get-process -ComputerName $ComputerName| Where-Object { trap { continue }  (New-Timespan $_.StartTime).TotalMinutes -le 10 }|Select-Object ProcessName,@{l="StartTime";e={$_.StartTime}},@{l="Owner";e={$owners[$_.id.tostring()]}},CPU,WorkingSet,Handles,Id|Format-List|out-string
		Add-RichTextBox $ProcessALL
	}
	
	$button_PasswordGen_Click={
		#Clear-RichTextBox
		Add-Logs "Generating a Password"
		$Passwordlist = [Char[]]'abcdefgABCDEFG0123456&%$'
		$Newpass = -join (1..8 | Foreach-Object { Get-Random $Passwordlist -count 1 })|Out-String
		Add-RichTextBox $Newpass
	}
	$ToolStripMenuItem_systemInformationMSinfo32exe_Click={Start-Process msinfo32.exe}
	$ToolStripMenuItem_addRemovePrograms_Click={Start-Process appwiz.cpl;Add-logs -text "Localhost - Add/Remove Programs (appwiz.cpl)"}
	$ToolStripMenuItem_administrativeTools_Click={Start-Process (Control admintools);Add-logs -text "Localhost - Administrative Tools (Control admintools)"}
	$ToolStripMenuItem_certificateManager_Click={Start-Process certmgr.msc}
	$ToolStripMenuItem_addRemoveProgramsWindowsFeatures_Click={$cmd = "rundll32.exe";$param = "shell32.dll,Control_RunDLL appwiz.cpl,,2";Start-process $cmd -ArgumentList $param;Add-logs -text "Localhost - Add/Remove Programs - Windows Features ($cmd $param)"}
	$button_mmcShares_Click={$ComputerName=$textbox_computername.Text;Add-logs -text "$ComputerName - Shared Folders MMC (fsmgmt.msc /computer:$ComputerName";$cmd="fsmgmt.msc";$param="/computer:$ComputerName";Start-Process $cmd $param}
	$ToolStripMenuItem_systemproperties_Click={Start-Process "sysdm.cpl"}
	$ToolStripMenuItem_Wordpad_Click={Start-Process "wordpad"}
	$ToolStripMenuItem_sharedFolders_Click={Start-Process "fsmgmt.msc"}
	$ToolStripMenuItem_performanceMonitor_Click={Start-Process "Perfmon.msc"}
	$ToolStripMenuItem_networkConnections_Click={Start-Process "ncpa.cpl"}
	$ToolStripMenuItem_devicemanager_Click={Start-Process "devmgmt.msc"}
	$ToolStripMenuItem_groupPolicyEditor_Click={start-process "Gpedit.msc"}
	$ToolStripMenuItem_localUsersAndGroups_Click={start-process "lusrmgr.msc"}
	$ToolStripMenuItem_diskManagement_Click={start-process "diskmgmt.msc"}
	$ToolStripMenuItem_localSecuritySettings_Click={Start-Process "secpol.msc"}
	$ToolStripMenuItem_componentServices_Click={Start-Process "dcomcnfg"}
	$ToolStripMenuItem_scheduledTasks_Click={Start-Process (control schedtasks)}
	$ToolStripMenuItem_PowershellISE_Click={start-process powershell_ise.exe}
	
	$button_servicesAutoNotStarted_Click={
		Get-ComputerTxtBox
		Add-Logs -text "$ComputerName - Services - Services with StartMode: Automatic and Status: NOT Running"
		if ($ComputerName -eq "localhost") {$ComputerName = "."}
		$Services_StartModeAuto = Get-WmiObject Win32_Service -ComputerName $ComputerName -Filter "startmode='auto' AND state!='running'"|Select-Object DisplayName,Name,StartMode,State|Format-Table -AutoSize|out-string
		Add-RichTextBox $Services_StartModeAuto
	}
	
	$textbox_computername_TextChanged={
		$label_OSStatus.Text = ""
		$label_PermissionStatus.Text = ""
		$label_PingStatus.Text = ""
		$label_RDPStatus.Text = ""
		$label_PSRemotingStatus.Text = ""
		$label_UptimeStatus.Text = ""
        $label_CurrentUser.Text = ""
		if ($textbox_computername.Text -eq "") {
			$textbox_computername.BackColor =  [System.Drawing.Color]::FromArgb(255, 128, 128);
			add-logs -text "Please Enter a ComputerName"
			$errorprovider1.SetError($textbox_computername, "Please enter a ComputerName.")
		}
		if ($textbox_computername.Text -ne "") {
			$textbox_computername.BackColor =  [System.Drawing.Color]::FromArgb(255, 255, 192)
			$errorprovider1.SetError($textbox_computername, "")
		}
		$tabcontrol_computer.Enabled = $textbox_computername.Text -ne ""
		$button_Check.Enabled = $textbox_computername.Text -ne ""
	}
	
	$button_Processor_Click={
		Get-ComputerTxtBox
		Add-logs -text "$ComputerName - Processor Information"
		$result = Get-Processor -ComputerName $ComputerName|Out-String
		Add-RichTextBox $result
	}
	
	$button_UsersGroupLocalUsers_Click={
		Get-ComputerTxtBox
		$result = Get-WmiObject -class "Win32_UserAccount" -namespace "root\CIMV2" -filter "LocalAccount = True" -computername $ComputerName|Select-Object AccountType,Caption,Description,Disabled,Domain,FullName,InstallDate,LocalAccount,Lockout,Name,PasswordChangeable,PasswordExpires,PasswordRequired,SID,SIDType,Status|Format-List|Out-String
		Add-RichTextBox $result
	}
	$button_UsersGroupDelLocalUsers_Click={
		$button_UsersGroupDelLocalUsers.Enabled = $False
		Get-ComputerTxtBox
		$Accounts = Get-WmiObject -ComputerName $ComputerName -Class Win32_UserProfile | Where-Object Special -eq $False | Select-Object localpath,sid | Out-GridView -Title 'Choose accounts to delete' -PassThru
		If ($Accounts) {
			$Confirmation = Show-MsgBox -Prompt "You are about to DELETE the below user profiles on $Computername, are you sure?`n`n$($Accounts | Format-List | Out-String)" -Title "$ComputerName - Remove User Profiles" -Icon Exclamation -BoxType YesNo
			If ($Confirmation -eq "YES") {
				Foreach ($Account in $Accounts) {
					Get-WmiObject -ComputerName $ComputerName -Class Win32_UserProfile | Where-Object {$_.Special -eq $False -and $_.SID -eq $Account.SID} | Remove-WmiObject
					Add-Logs -text ("Deleted the below account:`n" + ($Account | Out-String).trim())
				}
			}
		}
		$button_UsersGroupDelLocalUsers.Enabled = $True
	}
	$button_UsersGroupLocalGroups_Click={
		$button_UsersGroupLocalGroups.Enabled = $false
		Get-ComputerTxtBox
		$result = Get-WmiObject -Class Win32_Group -ComputerName $ComputerName	| Where-Object {$_.LocalAccount}|Format-Table -auto|Out-String
		Add-RichTextBox $result
		$button_UsersGroupLocalGroups.Enabled = $true
	}
	$button_UserADGroups_Click={
		$button_UserADGroups.Enabled = $False
		$Username = $label_CurrentUser.Text
		If ($Username) {
			Add-Logs -text "Getting groups for $Username"
			Try {
				$Groups = Get-ADPrincipalGroupMembership $Username
			}
			Catch {
				Add-Logs -text "Failed to get groups for $Username. Is $Username a domain user?"
			}
			If ($Groups) {
				Add-RichTextBox ("$Username Groups:`n`n" + ( $Groups | Select-Object -ExpandProperty Name | Out-String))
			}
			Else {
				Add-Logs -text "No groups found for $Username."
			}
		}
		Else {
			Add-Logs -text "No user selected"
		}
		$button_UserADGroups.Enabled = $True
	}
	$button_ComputerADGroups_Click={
		$button_ComputerADGroups.Enabled = $False
		Get-ComputerTxtBox
		Add-RichTextBox ("$ComputerName GROUPS:`n`n" + (Get-ADPrincipalGroupMembership (Get-ADComputer $ComputerName).DistinguishedName | Select-Object -ExpandProperty Name | Out-String))
		$button_ComputerADGroups.Enabled = $True
	}
	$button_SYDIGo_Click={
		Get-ComputerTxtBox
		Add-logs -text "$ComputerName - SYDI Tool (Script Your Documentation Instantly) - Microsoft Word required for DOC format."
		$SYDI_VBS = "$ScriptsFolder\sydi-server.vbs"
		$SYDI_SelectedFormat = $combobox_sydi_format.SelectedItem
		$SYDI_date = get-date -Format "yyyyMMddHH_mmss" |Out-String
		if ($SYDI_SelectedFormat -eq "XML"){
			#Add-RichTextBox "SYDI - Selected Format: $SYDI_SelectedFormat`n"
			$argument = "-t$ComputerName -wabefghipPqrsu -racdklp -ex -o`"$SavePath\$ComputerName-$SYDI_date.xml`""
			Start-Proc -exe "cmd.exe" -arguments "/k cscript $SYDI_VBS $argument"
			Add-RichTextBox "SYDI - File will be placed on desktop: $env:userprofile\desktop\$ComputerName-<date>.xml`n"
			Invoke-Item $env:userprofile\desktop
		}
		if ($SYDI_SelectedFormat -eq "DOC"){
			#Add-RichTextBox "SYDI - Selected Format: $SYDI_SelectedFormat`n"
			$argument = "-t$ComputerName -wabefghipPqrsu -racdklp -ew -o`"$SavePath\$ComputerName-$SYDI_date.doc`""
			Start-Proc -exe "cmd.exe" -arguments "/k cscript $SYDI_VBS $argument"
			Add-RichTextBox "SYDI - File will be placed on desktop: $env:userprofile\desktop\$ComputerName-<date>.doc/docx`n"
			Invoke-Item $env:userprofile\desktop
		}
		if ($SYDI_SelectedFormat -eq ""){Add-RichTextBox "SYDI - No Format Selected, please choose between DOC and XML`n"}	
	}
	
	$button_PageFile_Click={
		Get-ComputerTxtBox
		Add-logs -text "$ComputerName - Page File Information and Settings"
		Add-RichTextBox (Get-WmiObject win32_pagefileusage -ComputerName $ComputerName | Select-Object Name,AllocatedBaseSize,CurrentUsage,PeakUsage | Out-String)
	}
	
	$button_DiskPartition_Click={
		Get-ComputerTxtBox
		Add-logs -text "$ComputerName - Hard Drive - Partition"
		$result = Get-DiskPartition -ComputerName $ComputerName | Out-String
		Add-RichTextBox $result
		
	}
	
	$button_DiskUsage_Click={
		Get-ComputerTxtBox
		Add-Logs -text "$ComputerName - Hard Drive - DiskSpace"
		$result = Get-DiskSpace -ComputerName $ComputerName | Out-String
		Add-RichTextBox -text $result
	}
	
	$button_networkIPConfig_Click={
		Get-ComputerTxtBox
		Add-logs -text "$ComputerName - Network - Configuration"
		$result = Get-IP -ComputerName $ComputerName | Format-Table Name,IP4,IP4Subnet,DefaultGWY,MacAddress,DNSServer,WinsPrimary,WinsSecondary -AutoSize | Out-String -Width $richtextbox_output.Width
		Add-RichTextBox "$result`n"
	}
	
	$button_DiskRelationship_Click={
		Get-ComputerTxtBox
		Add-logs -text "$ComputerName - Hard Disk - Disks Relationship"
		$result = Get-DiskRelationship -ComputerName $ComputerName | Out-String
		Add-RichTextBox $result
	}
	
	$button_DiskMountPoint_Click={
		Get-ComputerTxtBox
		Add-logs -text "$ComputerName - Hard Disk - MountPoint"
		$result = Get-MountPoint -ComputerName $ComputerName | Out-String
		if ($null -ne $result){Add-RichTextBox $result}
		else {Show-MsgBox -BoxType "OKOnly" -Title "$ComputerName - Hard Disk - MountPoint" -Prompt "$ComputerName - No MountPoint detected" -Icon "Information"}
	}
	
	$button_DiskMappedDrive_Click={
		Get-ComputerTxtBox
		Add-logs -text "$ComputerName - Hard Disk - Mapped Drive"
		$result = Get-MappedDrive -ComputerName $ComputerName | Out-String
		if ($null -ne $result){Add-RichTextBox $result}
		else {Show-MsgBox -BoxType "OKOnly" -Title "$ComputerName - Mapped Drive" -Prompt "$ComputerName - No Mapped Drive detected" -Icon "Information"}
	}
	
	$button_Memory_Click={
		Get-ComputerTxtBox
		Add-logs -text "$ComputerName - Memory Configuration"
		$result = Get-MemoryConfiguration -ComputerName $ComputerName | Out-String
		Add-RichTextBox $result
	}
	
	$button_NIC_Click={
		Get-ComputerTxtBox
		Add-logs -text "$ComputerName - Network Interface Card Configuration (slow)"
		$result = Get-NICInfo -ComputerName $ComputerName | Out-String
		Add-RichTextBox $result
	}
	
	$button_MotherBoard_Click={
		Get-ComputerTxtBox
		Add-logs -text "$ComputerName - MotherBoard"
		$result = Get-MotherBoard -ComputerName $ComputerName | Out-String
		Add-RichTextBox $result
	}
	
	$button_networkRouteTable_Click={
		Get-ComputerTxtBox
		Add-logs -text "$ComputerName - Route table"
		$result = Get-Routetable  -ComputerName $ComputerName |Format-Table -auto| Out-String
		Add-RichTextBox $result
	}
	
	$button_SystemType_Click={
		#Clear-RichTextBox
		Get-ComputerTxtBox
		Add-logs -text "$ComputerName - System Type"
		$result = get-systemtype -ComputerName $ComputerName| Out-String
		Add-RichTextBox $result
	}
	
	$richtextbox_output_TextChanged={
		#Scroll to Bottom when text is changed
		$richtextbox_output.SelectionStart=$richtextbox_output.Text.Length
		$richtextbox_output.ScrollToCaret()
	}
	
	$richtextbox_Logs_TextChanged={
		$richtextbox_Logs.SelectionStart=$richtextbox_Logs.Text.Length
		$richtextbox_Logs.ScrollToCaret()
		if ($error[0]){Add-logs -text $($error[0].Exception.Message)}
	}
	$ToolStripMenuItem_adExplorer_Click={
		Add-logs -text "Localhost - SysInternals AdExplorer"
		$command="AdExplorer.exe"
		Start-Process $command -WorkingDirectory $ToolsFolder		
	}
	
	$button_Telnet_Click={
		Get-ComputerTxtBox
		$Telnet_command="cmd.exe"
		$Telnet_Args = "/k telnet.exe $ComputerName"
		Start-Process $Telnet_command $Telnet_Args
		Add-Logs -text "$ComputerName - Telnet (default port 23)"
	}
	
	$textbox_servicesAction_Click={$textbox_servicesAction.text = ""}
	
	$button_servicesRestart_Click={
		#Button Types 
		#
		#$a = new-object -comobject wscript.shell
		#$intAnswer = $a.popup("Do you want to continue ?",0,"Shutdown",4)
		#if ($intAnswer -eq 6){do something}
		#Value  Description  
		#0 Show OK button.
		#1 Show OK and Cancel buttons.
		#2 Show Abort, Retry, and Ignore buttons.
		#3 Show Yes, No, and Cancel buttons.
		#4 Show Yes and No buttons.
		#5 Show Retry and Cancel buttons.
		
		#Clear-RichTextBox
		Get-ComputerTxtBox
		Add-logs -text "$ComputerName - Restart Service"
		#Add-RichTextBox "# SERVICES - RESTART SERVICE - COMPUTERNAME: $ComputerName `n`n"
		#$Service_query = Read-Host "Enter the Service Name to Start `n"
		$Service_query = $textbox_servicesAction.text
		Add-logs -text "$ComputerName - Service to Restart: $Service_query"
		#Add-RichTextBox "SERVICE: $Service_query"
		$a = new-object -comobject wscript.shell
		$intAnswer = $a.popup("Do you want to continue ?",0,"$ComputerName - Start Service: $Service_query",4)
		if (($ComputerName -like "localhost") -and ($intAnswer -eq 6)) {
			Add-logs -text "$ComputerName - Stopping Service: $Service_query ..."
			$Service_query_return=Get-WmiObject Win32_Service -Filter "Name='$Service_query'"
			$Service_query_return.stopservice()
			Add-Logs -Text "$ComputerName - Command Sent! $Service_query should be stopped"
			Add-RichTextBox $Service_query_return
			Add-Logs -Text "$ComputerName - Checking the status of $Service_Query ..."
			Start-Sleep -Milliseconds 1000
			$Service_query_result=Get-WmiObject Win32_Service -Filter "Name='$Service_query'"|Out-String
			Add-RichTextBox $Service_query_result
			Add-Logs -Text "$ComputerName - Stop Service $Service_query - Done."
			Add-Logs -Text "$ComputerName - Restarting the Service $Service_query ..."
			#Add-RichTextBox "Starting Service: $Service_query...`r"
			$Service_query_return=Get-WmiObject Win32_Service -Filter "Name='$Service_query'"
			$Service_query_return.startservice()
			Add-Logs -Text "$ComputerName - Command Sent! $Service_query should be started"
			Add-RichTextBox $Service_query_return
			Add-Logs -Text "$ComputerName - Checking the status of $Service_Query ..."
			Start-Sleep -Milliseconds 1000
			$Service_query_result=Get-WmiObject Win32_Service -Filter "Name='$Service_query'"|Out-String
			Add-RichTextBox $Service_query_result
			Add-Logs -Text "$ComputerName - Start Service $Service_query - Done."
		}
		else { 
			if ($intAnswer -eq 6){
				Add-logs -text "$ComputerName - Stopping Service: $Service_query ..."
				$Service_query_return=Get-WmiObject Win32_Service -Filter "Name='$Service_query'"
				$Service_query_return.stopservice()
				Add-Logs -Text "$ComputerName - Command Sent! $Service_query should be stopped"
				Add-RichTextBox $Service_query_return
				Add-Logs -Text "$ComputerName - Checking the status of $Service_Query ..."
				Start-Sleep -Milliseconds 1000
				$Service_query_result=Get-WmiObject Win32_Service -Filter "Name='$Service_query'"|Out-String
				Add-RichTextBox $Service_query_result
				Add-Logs -Text "$ComputerName - Stop Service $Service_query - Done."
				Add-Logs -Text "$ComputerName - Restarting the Service $Service_query ..."
				$Service_query_return=Get-WmiObject Win32_Service -computername $ComputerName -Filter "Name='$Service_query'"
				$Service_query_return.startservice()
				Add-Logs -Text "$ComputerName - Command Sent! $Service_query should be started"
				Add-RichTextBox $Service_query_return
				Add-Logs -Text "$ComputerName - Checking the status of $Service_Query ..."
				Start-Sleep -Milliseconds 1000
				$Service_query_result=Get-WmiObject Win32_Service -computername $ComputerName -Filter "Name='$Service_query'"|Out-String
				Add-RichTextBox $Service_query_result
				Add-Logs -Text "$ComputerName - Start Service $Service_query - Done."
			}# IF
		}#ELSE
		#
	}
	$ToolStripMenuItem_hostsFile_Click={
		Add-logs -text "LocalHost - Opening hosts file"
		. "C:\Windows\System32\drivers\etc\hosts"
	}

	$ToolStripMenuItem_hostsFileGetContent_Click={
		Add-logs -text "LocalHost - Getting contents of hosts file"
		$resultHostFile = Get-Content "\\$ComputerName\C$\Windows\System32\drivers\etc\hosts" | Out-String
		Add-RichTextBox $resultHostFile
	}
	
	$button_HostsFile_Click={
		$button_HostsFile.Enabled = $false
		Add-logs -text "$ComputerName - Opening remote hosts file"
		. "\\$ComputerName\C$\Windows\System32\drivers\etc\hosts"
		$button_HostsFile.Enabled = $true
	}
	$button_Pend_Reboot_Click={
		$button_Pend_Reboot.Enabled = $false
		Get-ComputerTxtBox
		Add-Logs -Text "LocalHost - Get-PendingReboot"
		Add-RichTextBox (Get-PendingReboot $ComputerName | Out-String)
		$button_Pend_Reboot.Enabled = $true

	}
	
	$button_processTerminate_Click={
		#Button Types 
		#
		#$a = new-object -comobject wscript.shell
		#$intAnswer = $a.popup("Do you want to continue ?",0,"Shutdown",4)
		#if ($intAnswer -eq 6){do something}
		#Value  Description  
		#0 Show OK button.
		#1 Show OK and Cancel buttons.
		#2 Show Abort, Retry, and Ignore buttons.
		#3 Show Yes, No, and Cancel buttons.
		#4 Show Yes and No buttons.
		#5 Show Retry and Cancel buttons.
		#Clear-RichTextBox
		Get-ComputerTxtBox
		Add-logs -text "$ComputerName - Terminate Process"
		#$Service_query = Read-Host "Enter the Service Name to Stop `n"
		$Process_query = $textbox_processName.text
		Add-logs -text "$ComputerName - Process to Terminate: $Process_query"
		$a = new-object -comobject wscript.shell
		$intAnswer = $a.popup("Do you want to continue ?",0,"$ComputerName - Terminate Process: $Process_query",4)
		
		#localhost
		if (($ComputerName -like "localhost") -and ($intAnswer -eq 6)) {
			Add-logs -text "$ComputerName - Terminate Process: $Process_query - Terminating..."
			$Process_query_return = (Get-WmiObject Win32_Process -Filter "Name='$Process_query'").Terminate() | Out-String
			#$Process_query_return.Terminate()
			#Add-Logs -Text "$ComputerName - Terminate Process: $Process_query ..."
			Add-RichTextBox $Process_query_return
			Add-logs -text "$ComputerName - Terminate Process: $Process_query - Checking Status... "
			Start-Sleep -Milliseconds 1000
			$Process_query_return = Get-WmiObject Win32_Process -Filter "Name='$Process_query'"|Out-String
			if (!($Process_query_return)){Add-Logs -Text "$ComputerName - $Process_query  has been terminated"}
			Add-logs -text "$ComputerName - Terminate Process: $Process_query - Terminated "
		}#end IF
		
		#RemoteHost
		else {
			if ($intAnswer -eq 6){
				Add-logs -text "$ComputerName - Terminate Process: $Process_query - Terminating..."
				$Process_query_return = (Get-WmiObject Win32_Process -Filter "Name='$Process_query'").Terminate() | Out-String
				#$Process_query_return.Terminate()
				Add-RichTextBox $Process_query_return
				Add-logs -text "$ComputerName - Terminate Process: $Process_query - Checking Status... "
				Start-Sleep -Milliseconds 1000
				$Process_query_return=Get-WmiObject Win32_Process -computername $ComputerName -Filter "Name='$Process_query'"|Out-String
				if (!($Process_query_return)){Add-Logs -Text "$ComputerName - Terminate Process: $Process_query - Terminated "}
				#Add-logs -text "$ComputerName - Terminate Process: $Process_query - Terminated "
			}#end IF
		}#end ELSE
	}
	
	
	$button_StartupCommand_Click={
		Get-ComputerTxtBox
		Add-Logs -text "$ComputerName - Startup Commands"
		$result = Get-WmiObject Win32_StartupCommand -ComputerName $ComputerName |Sort-Object Caption |Format-Table __Server,Caption,Command,User -auto | out-string -Width $richtextbox_output.Width
		Add-richtextbox $result
		Add-Logs -text "$ComputerName - Startup Commands - Done."
	}

	$button_ConnectivityTesting_Click={
		Get-ComputerTxtBox
		Add-Logs -text "$ComputerName - Connectivity Testing..."
		$result = Test-Server -computername $ComputerName | Out-String
		Add-richtextbox "$result`n"
	}
	$button_psexec_Click={
		Get-ComputerTxtBox
		Add-logs -text "$ComputerName - PSEXEC (Terminal)"
		if(Test-Path "$ToolsFolder\psexec.exe"){
			$argument = "/k `"$ToolsFolder\psexec.exe`" \\$ComputerName cmd.exe"
			Start-Process -WorkingDirectory "C:\windows\system32" cmd.exe $argument
		}
		else {$button_psexec.ForeColor = 'Red'}
	}
	
	$button_PAExec_Click={
		Get-ComputerTxtBox
		Add-logs -text "$ComputerName - PAEXEC (Terminal)"
		$argument = "/k `"$ToolsFolder\paexec.exe`" \\$ComputerName -s cmd.exe"
		Start-Process -WorkingDirectory "C:\windows\system32" cmd.exe $argument
	}
	$button_GPupdate_Click={
		Get-ComputerTxtBox
		Add-logs -text "$ComputerName - GPUpdate (Remotly via WMI)"
		$result = Invoke-GPUpdate -ComputerName $ComputerName
		if ($null -ne $result)
		{
			if ($result.ReturnValue -eq 0){
				Show-MsgBox -BoxType "OKOnly" -Title "$ComputerName - Group Policy Update" -Prompt "Gpupdate ran successfully!" -Icon "Information"
				Add-RichTextBox $($result|Out-String)
			}
		}
		else {Show-MsgBox -BoxType "OKOnly" -Title "$ComputerName - Group Policy Update" -Prompt "Gpupdate does not seem to work! Are you in a Domain ?" -Icon "Exclamation"}
	}
	
	$button_Applications_Click={
		Get-ComputerTxtBox
		$result = Get-InstalledSoftware -ComputerName $ComputerName | Select-Object -Property Name,Version,InstallDate,Vendor,UninstallString
		Add-Logs -text "$ComputerName - Installed Softwares List"
		Add-RichTextBox ($result | Format-Table Name,Version,Vendor,UninstallString -AutoSize | Out-String -Width $richtextbox_output.Width)
		Remove-App $result
	}
	
	$ToolStripMenuItem_netstatsListening_Click={
		$this.Enabled = $False
		Add-logs -text "$env:ComputerName - Netstat"
		$resultNetstat = Get-NetStat | Format-Table -AutoSize | Out-String
		Add-RichTextBox $resultNetstat
	}
	
	$ToolStripMenuItem_WMIExplorer_Click={
		Add-logs -text "Scripts - WMIExplorer.ps1 By www.ThePowerShellGuy.com (Marc van Orsouw)"
		& "$ScriptsFolder\WMIExplorer.ps1"
	}

	$ToolStripMenuItem_ADUSERUnlocker_Click={
		Add-logs -text "Scripts - ADUSERUnlocker.ps1 By LazyWinAdmin (Francois-Xavier Cat)"
		& "$ScriptsFolder\ADUSERUnlocker.ps1"
	}
	
	$button_DriverQuery_Click={
		$button_DriverQuery.Enabled = $False
		Get-ComputerTxtBox
		$DriverQuery_command="cmd.exe"
		$DriverQuery_arguments = "/k driverquery /s $ComputerName"
		Start-Process $DriverQuery_command $DriverQuery_arguments
		$button_DriverQuery.Enabled = $true
	}
	
	$button_PsRemoting_Click={
		Get-ComputerTXTBOX
		Add-logs -text "$ComputerName - Open a PowerShell Remoting Session"
		if (Test-PSRemoting -ComputerName $ComputerName){
			Add-logs -text "$ComputerName - Powershell Remote Session"
			Start-Process powershell.exe -ArgumentList "-noexit -command Enter-PSSession -ComputerName $ComputerName"
		}
		else {
			Add-logs -text "$ComputerName - PsRemoting does not seem to be enabled"
			Show-MsgBox -Title "PSRemoting" -BoxType "OKOnly" -Icon "Exclamation" -Prompt "PSRemoting does not seem to be enabled"
		}
	}
	$button_MsInfo32_Click={
		Get-ComputerTXTBOX
		Add-Logs "$ComputerName - System Information (MSinfo32.exe)"
		$cmd = "$env:programfiles\Common Files\Microsoft Shared\MSInfo\msinfo32.exe"
		$param = "/computer $ComputerName"
		Start-Process $cmd $param
	}
	
	$button_Qwinsta_Click={
		$button_Qwinsta.Enabled = $false
		Get-ComputerTXTBOX
		
		if ($current_OS_caption -notlike "*64*"){
			Add-Logs -text "$ComputerName - QWINSTA (Query Terminal Sessions) - 32 bits"
			$Qwinsta_cmd = "cmd"
			$Qwinsta_argument = "/k qwinsta /server:$computername"
			Start-Process $Qwinsta_cmd $Qwinsta_argument
		}
		else {
			Add-Logs -text "$ComputerName - QWINSTA (Query Terminal Sessions) - 64 bits"
			$Qwinsta_cmd = "cmd"
			$Qwinsta_argument = "/k $env:SystemRoot\Sysnative\qwinsta /server:$computername"
			Start-Process $Qwinsta_cmd $Qwinsta_argument
		}
		$button_Qwinsta.Enabled = $true
	}
	
	$button_Rwinsta_Click={
		$button_Rwinsta.Enabled = $false
		Get-ComputerTXTBOX
		Add-Logs -text "$ComputerName - RWINSTA (Reset Terminal Sessions)"
		if ($current_OS_caption -notlike "*64*"){
			Add-Logs -text "$ComputerName - RWINSTA (Reset Terminal Sessions) - 32 bits"
			$Rwinsta_ID = Show-Inputbox -message "Enter The Session ID to kill" -title "$ComputerName - Rwinsta (Reset Terminal Session)"
			if ($Rwinsta_ID -ne ""){
				$Rwinsta_cmd = "cmd"
				$Rwinsta_argument = "/k $env:SystemRoot\System32\rwinsta $Rwinsta_ID /server:$computername"
				Start-Process $Rwinsta_cmd $Rwinsta_argument
			}
		}
		else {
			Add-Logs -text "$ComputerName - RWINSTA (Reset Terminal Sessions) - 64 bits"
			$Rwinsta_cmd = "cmd"
			$Rwinsta_ID = Show-Inputbox -message "Enter The Session ID to kill" -title "$ComputerName - Rwinsta (Reset Terminal Session)"
			if ($Rwinsta_ID -ne ""){
				$Rwinsta_argument = "/k $env:SystemRoot\Sysnative\rwinsta $Rwinsta_ID /server:$computername"
				Start-Process $Rwinsta_cmd $Rwinsta_argument
			}
		}
		$button_Rwinsta.Enabled = $true
	}
	
	$button_RebootHistory_Click={
		$button_RebootHistory.Enabled = $false
		Get-ComputerTxtBox
		Add-logs -text "$ComputerName - Reboot History (Scanning Event Logs for ID 6009) - By BSonPosh.com"
		start-sleep -s 1
		$job = get-reboottime -ComputerName $ComputerName |Out-string
		Add-Richtextbox $job
		$button_RebootHistory.Enabled = $true
	}
	
	$button_USBDevices_Click={
		$button_USBDevices.Enabled = $false
		Get-ComputerTxtBox
		Add-Logs "$ComputerName - USB Devices"
		$result = Get-USB -computerName $ComputerName|
			Select-Object SystemName,Manufacturer,Name|
			Sort-Object Manufacturer|
			Format-Table -AutoSize|Out-String
		Add-RichTextBox $result
		$button_USBDevices.Enabled = $true
	}
	
	$button_RDPEnable_Click={
		$button_RDPEnable.Enabled = $false
		Get-ComputerTxtBox
		Add-Logs "$ComputerName - Enable RDP"
		$result = Set-RDPEnable -ComputerName $ComputerName
		$button_RDPEnable.Enabled = $true
	}
	
	$button_RDPDisable_Click={
		$button_RDPDisable.Enabled = $false
		Get-ComputerTxtBox
		Add-Logs "$ComputerName - Disable RDP"
		Set-RDPDisable -ComputerName $ComputerName
		$button_RDPDisable.Enabled = $true
	}
	
	$button_HotFix_Click={
		$button_HotFix.Enabled = $false
		Get-ComputerTxtBox
		Add-logs -text "$ComputerName - Get the Windows Updates Installed"
		$result = Get-HotFix -ComputerName $ComputerName | Sort-Object InstalledOn| Format-Table __SERVER, Description, HotFixID, InstalledBy, InstalledOn,Caption -AutoSize | Out-String -Width $richtextbox_output.Width
		Add-RichTextBox $result
		$button_HotFix.Enabled = $true
	}
	
	$button_Printers_Click={
		$button_Printers.Enabled = $false
		Get-ComputerTxtBox
		Add-Logs -text "$ComputerName - Printers"
		$result = Get-WmiObject Win32_Printer -ComputerName $ComputerName  | Format-table SystemName,Name,Comment,PortName,Location,DriverName -AutoSize | Out-String
		if ($null -ne $result){
			Add-RichTextBox $result}
		else {Add-RichTextBox "$ComputerName - No Printer detected"}
		$button_Printers.Enabled = $true
	}
	
	$button_Restart_Click={
		Get-ComputerTxtBox
		Add-Logs -text "$ComputerName - Restart Computer"
		$Confirmation = Show-MsgBox -Prompt "You want to restart $ComputerName, Are you sure ?" -Title "$ComputerName - Restart Computer" -Icon Exclamation -BoxType YesNo
		if ($Confirmation -eq "YES")
		{ 
			Restart-Computer -ComputerName $ComputerName -Force
			Show-MsgBox -Prompt "$ComputerName - Restart Initialized" -Title "$ComputerName - Restart Computer" -Icon Information -BoxType OKOnly
		}
		else {Show-MsgBox -BoxType "OKOnly" -Title "$ComputerName - Restart" -Prompt "$ComputerName - Restart Cancelled" -Icon "Information"}
	}
	
	$button_Shutdown_Click={
		Get-ComputerTxtBox
		Add-Logs -text "$ComputerName - Shutdown Computer"
		$Confirmation = Show-MsgBox -Prompt "You want to shutdown $ComputerName, Are you sure ?" -Title "$ComputerName - Shutdown Computer" -Icon Exclamation -BoxType YesNo
		if ($Confirmation -eq "YES")
		{ 
			Stop-Computer -ComputerName $ComputerName -Force
			Show-MsgBox -Prompt "$ComputerName - Shutdown Initialized" -Title "$ComputerName - Shutdown Computer" -Icon Information -BoxType OKOnly
		}
		else {Show-MsgBox -BoxType "OKOnly" -Title "$ComputerName - Shutdown" -Prompt "$ComputerName - Shutdown Cancelled" -Icon "Information"}
	}
	
	$buttonCommandLine_Click={
		Get-ComputerTxtBox
		Add-Logs -text "$ComputerName - Get the CommandLine Argument for each process"
		$result = Get-WmiObject Win32_Process -ComputerName $ComputerName | select-Object Name,ProcessID,CommandLine| Format-Table -AutoSize |Out-String -Width $richtextbox_output.Width
		Add-RichTextBox $result
	}
	
	$button_ComputerDescriptionQuery_Click={
		Get-ComputerTxtBox
		Add-Logs -text "$ComputerName - Get the Computer Description"
		$result = Get-ComputerComment -ComputerName $ComputerName
		Add-RichTextBox $result
	}
	
	$button_ComputerDescriptionChange_Click={
		Get-ComputerTxtBox
		Add-Logs -text "$ComputerName - Set the Computer Description"
		$Description = Show-Inputbox -message "Enter a Computer Description" -title "$ComputerName" -default "<Role> - <Owner> - <Ticket#>"
		if ($Description -ne "" ){
			$result = Set-ComputerComment -ComputerName $ComputerName -Description $Description
		}
	}

	$buttonC_Click={
		Get-ComputerTxtBox
		Add-Logs -text "$ComputerName - Open C$ Drive"
		$PathToCDrive = "\\$ComputerName\c$"
		Explorer.exe $PathToCDrive
	}
	
	$buttonRemoteAssistance_Click={
		Get-ComputerTxtBox
		Add-Logs -text "$ComputerName - Remote Assistance"
		MSRA.exe /OfferRA $ComputerName
	}
	$buttonWindowsUpdateLog_Click={
		$buttonWindowsUpdateLog.Enabled = $false
		Get-ComputerTxtBox
		Invoke-RemoteCMD -ComputerName $ComputerName -Command 'Get-WindowsUpdateLog -LogPath C:\windowsupdate.log'
		Move-Item "\\$ComputerName\C$\windowsupdate.log" "C:\windows\temp\$($computername)_windowsupdate.log" -Force
		Start-Process "C:\windows\temp\$($computername)_windowsupdate.log"
		$buttonWindowsUpdateLog.Enabled = $true
	}
	
	$buttonReportingEventslog_Click={
		$buttonReportingEventslog.Enabled = $false
		Get-ComputerTxtBox
		Add-Logs -text "$ComputerName - WSUS Report (ReportingEvents.log)"
		$SystemDriveLetter = ((Get-WmiObject -Class Win32_OperatingSystem -ComputerName $ComputerName).systemdrive).substring(0,1)
		$SystemDriveFolder = ((Get-WmiObject -Class Win32_OperatingSystem -ComputerName $ComputerName).WindowsDirectory).substring(3)
		$pathToWSUSReportLog= "\\$computername\$SystemDriveLetter$\$SystemDriveFolder\SoftwareDistribution\ReportingEvents.log"
		if (Test-Path $pathToWSUSReportLog)
			{Invoke-Item $pathToWSUSReportLog}
		else {Show-MsgBox -BoxType "OKOnly" -Title "$ComputerName - WSUS Report (ReportingEvents.log)" -Prompt "$ComputerName - Can't find the ReportingEvents.log file `rPath:$pathToWSUSReportLog" -Icon "Exclamation"}
		$buttonReportingEventslog.Enabled = $true
	}
	
	$buttonCommandLineGridView_Click={
		Get-ComputerTxtBox
		Add-Logs -text "$ComputerName - Get the CommandLine Argument for each process - Grid View"
		#Get-WmiObject Win32_Process -Filter "Name like '%powershell%'" | select-Object CommandLine
		Get-WmiObject Win32_Process -ComputerName $ComputerName | select-Object Name,ProcessID,CommandLine| Out-GridView
	}
	
	$buttonServices_Click={
		Get-ComputerTxtBox
		Add-logs -text "$ComputerName - Services MMC (services.msc /computer:$ComputerName)"
		$command = "services.msc"
		$arguments = "/computer:$computername"
		Start-Process $command $arguments 
	}
	
	$buttonEventVwr_Click={
		Get-ComputerTxtBox
		Add-logs -text "$ComputerName - Event Viewer MMC (eventvwr $Computername)"
		$command="eventvwr"
		$arguments = "$ComputerName"
		Start-Process $command $arguments
	}
	$button_sendMsg_Click={
		$button_sendMsg.Enabled = $False
		Get-ComputerTxtBox
		$Message = Show-Inputbox -message "Enter a message" -title "$Computername - Send message to user"
		If ($Message) {
			Invoke-RemoteCMD -ComputerName $ComputerName -Command "msg * $Message"
		}

		$button_sendMsg.Enabled = $True
	}
	$button_promptReboot_Click={
		$button_promptReboot.Enabled = $False
		Get-ComputerTxtBox
		$Timeout = Show-Inputbox -message "Enter the amount of time (in minutes) to wait before rebooting.`n`nWARNING: This will RESTART the remote computer immediately when the timeout expires, or when the user closes the message prompt!" -title "$Computername - Send message to user"
		If ($Timeout -match '\D' -or !($Timeout)) {
			Add-Logs -text "Timeout must be an interger"
		}
		Else {
			Add-Logs -text "Rebooting $ComputerName in $Timeout minutes"
			$Time = ([int]$Timeout * 60)
			# This line essentially results in powershell calling powershell calling powershell, but it's an easy way to detach the lazywinadmin process from the remote process that's running
			Invoke-RemoteCMD -ComputerName $ComputerName -Timeout 1 -Command "msg * /TIME:$Time /W 'Your computer will shutdown in $Timeout minutes. Please save your work now.`n`nCLOSE THIS BOX TO RESTART NOW'; shutdown -r -f -t 30"
		}
		$button_promptReboot.Enabled = $True
	}
	$button_scheduleReboot_Click={
		$button_scheduleReboot.Enabled = $False
		Get-ComputerTxtBox
		$Schd = Show-Inputbox -message "Enter the date and time for the reboot to be scheduled (yyyy-MM-dd HH:mm or HH:mm)" -title "$Computername - Schedule a reboot"
		If ($SchdDate = Get-Date $Schd -ErrorAction SilentlyContinue) {
			$Seconds = [math]::round(($SchdDate - (Get-Date)).TotalSeconds)
			If ($Seconds -le 0) {
				Add-Logs -text "$Schd ($SchdDate) is in the past"
			}
			Elseif ($Seconds -ge 315360000) {
				Add-Logs -text "$Schd ($SchdDate) is too far in the future"
			}
			Else {
				Add-Logs -text "Rebooting $ComputerName at $Schd ($SchdDate)"
				# This works shockingly well. Schedules a reboot, and notifies the user when that reboot will occur, conveniently including the date in the notification
				Invoke-RemoteCMD -ComputerName $ComputerName -Command "shutdown -r -f -t $Seconds"
			}
			
		}
		Else {
			Add-Logs -text "Please check the date format, it should be yyyy-MM-dd HH:mm or HH:mm"
			Add-Logs -text "Example: 2020-05-21 23:00"
		}
		$button_scheduleReboot.Enabled = $True
	}
	$buttonShares_Click={
		Get-ComputerTxtBox
		Add-logs -text "$ComputerName - Shared Folders MMC (fsmgmt.msc /computer:$ComputerName"
		$SharesCmd="fsmgmt.msc"
		$SharesParam="/computer:$ComputerName"
		Start-Process $SharesCmd $SharesParam
	}
	
	$buttonSendCommand_Click={
		Get-ComputerTxtBox
		Add-Logs -text "$ComputerName - Run a Remote Command"
		$RemoteCommand = Show-Inputbox -message "Enter a command to run" -title "$Computername - Invoke-RemoteCMD" -default $script:PreviousCommand
		if ($RemoteCommand -ne ""){
			Add-RichTextBox (Invoke-RemoteCMD -ComputerName $ComputerName -Command $RemoteCommand | Out-String -Width $richtextbox_output.Width)
			Add-Logs -text "$ComputerName - Remote Command Sent!"
			$script:PreviousCommand = $RemoteCommand
		}
	}
	
	$button_SystemInfoexe_Click={
		$button_SystemInfoexe.Enabled = $false
		Get-ComputerTxtBox 		#Get the current ComputerName in the TextBox
		$SystemInfo_cmd_command	= "cmd" #Declare Main Command
		$SystemInfo_cmd_Args	= "/k systeminfo /s $Computername" #Declare Arguments
		# Run it
		Start-Process $SystemInfo_cmd_command $SystemInfo_cmd_Args -WorkingDirectory "c:\"
		$button_SystemInfoexe.Enabled = $true
	}
	
	#Runs the query when you press Enter
	$textbox_computername.Add_KeyDown( {
	if ($_.KeyCode -eq "Enter") {
		$button_Check.PerformClick()
	}
})
	
	# --End User Generated Script--
	#----------------------------------------------
	#region Generated Events
	#----------------------------------------------
	
	$Form_StateCorrection_Load=
	{
		#Correct the initial state of the form to prevent the .Net maximized form issue
		$form_MainForm.WindowState = $InitialFormWindowState
	}
	
	$Form_StoreValues_Closing=
	{
		#Store the control values
		$script:MainForm_richtextbox_output = $richtextbox_output.Text
		$script:MainForm_textbox_processName = $textbox_processName.Text
		$script:MainForm_textbox_servicesAction = $textbox_servicesAction.Text
		$script:MainForm_combobox_sydi_format = $combobox_sydi_format.Text
		$script:MainForm_textbox_sydi_arguments = $textbox_sydi_arguments.Text
		$script:MainForm_textbox_networktracertparam = $textbox_networktracertparam.Text
		$script:MainForm_textbox_networkpathpingparam = $textbox_networkpathpingparam.Text
		$script:MainForm_textbox_pingparam = $textbox_pingparam.Text
		$script:MainForm_textbox_computername = $textbox_computername.Text
		$script:MainForm_richtextbox_Logs = $richtextbox_Logs.Text
	}
	#endregion Generated Events

	#----------------------------------------------
	#region Generated Form Code
	#----------------------------------------------
	#
	# form_MainForm
	#
	$form_MainForm.Controls.Add($richtextbox_output)
	$form_MainForm.Controls.Add($panel_RTBButtons)
	$form_MainForm.Controls.Add($tabcontrol_computer)
	$form_MainForm.Controls.Add($groupbox_ComputerName)
	$form_MainForm.Controls.Add($richtextbox_Logs)
	$form_MainForm.Controls.Add($statusbar1)
	$form_MainForm.Controls.Add($menustrip_principal)
	$form_MainForm.AutoScaleMode = 'Inherit'
	$form_MainForm.AutoSize = $True
	$form_MainForm.BackColor = "Black"
	$form_MainForm.ForeColor = "White"
	$form_MainForm.ClientSize = '1170, 719'
	$form_MainForm.Font = "Microsoft Sans Serif, 8.25pt"
	$form_MainForm.MainMenuStrip = $menustrip_principal
	$form_MainForm.MinimumSize = '1178, 746'
	$form_MainForm.Name = "form_MainForm"
	$form_MainForm.Text = "LazyWinAdmin"
	$form_MainForm.add_Load($OnLoadFormEvent)
	#
	# richtextbox_output
	#
	$richtextbox_output.Dock = 'Fill'
	$richtextbox_output.Font = "Consolas, 8.25pt"
	$richtextbox_output.Location = '0, 224'
	$richtextbox_output.Name = "richtextbox_output"
	$richtextbox_output.Size = '1170, 365'
	$richtextbox_output.TabIndex = 3
	$richtextbox_output.Text = ""
	$richtextbox_output.BackColor = "Black"
	$richtextbox_output.ForeColor = "#CCCCCC"
	$tooltipinfo.SetToolTip($richtextbox_output, "Output")
	$richtextbox_output.add_TextChanged($richtextbox_output_TextChanged)
	#
	# panel_RTBButtons
	#
	$panel_RTBButtons.Dock = 'Bottom'
	$panel_RTBButtons.Location = '0, 589'
	$panel_RTBButtons.Name = "panel_RTBButtons"
	$panel_RTBButtons.Size = '1170, 34'
	$panel_RTBButtons.TabIndex = 63
	$arrPanels += $panel_RTBButtons
	#
	# button_outputClear
	#
	$button_outputClear.Image = [System.Drawing.Image]::FromFile("$PSScriptRoot\images\clear.png")
	$button_outputClear.Name = "button_outputClear"
	$tooltipinfo.SetToolTip($button_outputClear, "Clear !")
	$button_outputClear.add_Click($button_outputClear_Click)
	$arrOutputButtons += $button_outputClear
	#
	# button_ExportRTF
	#
	$button_ExportRTF.Image = [System.Drawing.Image]::FromFile("$PSScriptRoot\images\rtf.png")
	$button_ExportRTF.Name = "button_ExportRTF"
	$tooltipinfo.SetToolTip($button_ExportRTF, "Export to Wordpad (RTF)")
	$button_ExportRTF.add_Click($button_ExportRTF_Click)
	$arrOutputButtons += $button_ExportRTF
	#
	# button_outputCopy
	#
	$button_outputCopy.Image = [System.Drawing.Image]::FromFile("$PSScriptRoot\images\copy.png")
	$button_outputCopy.Name = "button_outputCopy"
	$tooltipinfo.SetToolTip($button_outputCopy, "Copy to Clipboard")
	$button_outputCopy.add_Click($button_outputCopy_Click)
	$arrOutputButtons += $button_outputCopy

	#
	# tabcontrol_computer
	#
	# Colouring tab controls is complicated
	# See https://github.com/My-Random-Thoughts/Various-Code/blob/master/Demos/OwnerDraw-TabControl-Demo.ps1
	$tabcontrol_computer.Dock = 'Top'
	$tabcontrol_computer.Location = '0, 87'
	$tabcontrol_computer.Multiline = $False
	$tabcontrol_computer.Name = "tabcontrol_computer"
	$tabcontrol_computer.SelectedIndex = 0
	$tabcontrol_computer.Size = '2170, 137'
	$tabcontrol_computer.TabIndex = 11	
	#
	# tabpage_general
	#
	$tabpage_general.Controls.Add($groupbox_ManagementConsole)
	$tabpage_general.Controls.Add($groupbox_Notify)
	$tabpage_general.Location = '4, 22'
	$tabpage_general.Name = "tabpage_general"
	$tabpage_general.Size = '4162, 111'
	$tabpage_general.TabIndex = 12
	$tabpage_general.Text = "General"
	$arrTabPages += $tabpage_general

	#
	#region groupbox_ManagementConsole
	#
	$groupbox_ManagementConsole.Controls.Add($button_mmcCompmgmt)
	$groupbox_ManagementConsole.Controls.Add($buttonServices)
	$groupbox_ManagementConsole.Controls.Add($buttonShares)
	$groupbox_ManagementConsole.Controls.Add($buttonEventVwr)
	$groupbox_ManagementConsole.Location = '835, 4'
	$groupbox_ManagementConsole.Name = "groupbox_ManagementConsole"
	$groupbox_ManagementConsole.Size = '157, 77'
	$groupbox_ManagementConsole.TabIndex = 49
	$groupbox_ManagementConsole.TabStop = $False
	$groupbox_ManagementConsole.Text = "Management Console"
	$arrGroupboxes += $groupbox_ManagementConsole
	#
	# button_mmcCompmgmt
	#
	$button_mmcCompmgmt.Font = "Trebuchet MS, 8.25pt"
	$button_mmcCompmgmt.Location = '2, 19'
	$button_mmcCompmgmt.Name = "button_mmcCompmgmt"
	$button_mmcCompmgmt.Size = '82, 23'
	$button_mmcCompmgmt.TabIndex = 7
	$button_mmcCompmgmt.Text = "Compmgmt"
	$tooltipinfo.SetToolTip($button_mmcCompmgmt, "Launch Computer Management Console (compmgmt.msc)")
	$button_mmcCompmgmt.add_Click($button_mmcCompmgmt_Click)
	$arrButtons += $button_mmcCompmgmt
	#
	# buttonServices
	#
	$buttonServices.Font = "Trebuchet MS, 8.25pt"
	$buttonServices.Location = '84, 19'
	$buttonServices.Name = "buttonServices"
	$buttonServices.Size = '71, 23'
	$buttonServices.TabIndex = 45
	$buttonServices.Text = "Services"
	$tooltipinfo.SetToolTip($buttonServices, "Launch Services.msc")
	$buttonServices.add_Click($buttonServices_Click)
	$arrButtons += $buttonServices
	#
	# buttonShares
	#
	$buttonShares.Font = "Trebuchet MS, 8.25pt"
	$buttonShares.Location = '84, 48'
	$buttonShares.Name = "buttonShares"
	$buttonShares.Size = '71, 23'
	$buttonShares.TabIndex = 46
	$buttonShares.Text = "Shares"
	$tooltipinfo.SetToolTip($buttonShares, "Launch Fsmgmt.msc")
	$buttonShares.add_Click($buttonShares_Click)
	$arrButtons += $buttonShares
	#
	# buttonEventVwr
	#
	$buttonEventVwr.Font = "Trebuchet MS, 8.25pt"
	$buttonEventVwr.Location = '2, 48'
	$buttonEventVwr.Name = "buttonEventVwr"
	$buttonEventVwr.Size = '82, 23'
	$buttonEventVwr.TabIndex = 47
	$buttonEventVwr.Text = "Event Vwr"
	$tooltipinfo.SetToolTip($buttonEventVwr, "Launch Eventvwr")
	$buttonEventVwr.add_Click($buttonEventVwr_Click)
	$arrButtons += $buttonEventVwr
	#endregion groupbox_ManagementConsole
	#
	#region groupbox_Notify
	#
	$groupbox_Notify.Controls.Add($button_sendMsg)
	$groupbox_Notify.Controls.Add($button_promptReboot)
	$groupbox_Notify.Controls.Add($button_scheduleReboot)
	$groupbox_Notify.Location = '1000, 4'
	$groupbox_Notify.Name = "groupbox_Notify"
	$groupbox_Notify.Size = '157, 77'
	$groupbox_Notify.TabStop = $False
	$groupbox_Notify.Text = "Notifications"
	$arrGroupboxes += $groupbox_Notify
	#
	# button_sendMsg
	#
	$button_sendMsg.Font = "Trebuchet MS, 8.25pt"
	$button_sendMsg.Location = '2, 19'
	$button_sendMsg.Name = "button_sendMsg"
	$button_sendMsg.Size = '82, 23'
	$button_sendMsg.Text = "Send Msg"
	$tooltipinfo.SetToolTip($button_sendMsg, "Send a message to the currently logged in user")
	$button_sendMsg.add_Click($button_sendMsg_Click)
	$arrButtons += $button_sendMsg
	#
	# button_promptReboot
	#
	$button_promptReboot.Font = "Trebuchet MS, 8.25pt"
	$button_promptReboot.Location = '84, 19'
	$button_promptReboot.Name = "button_promptReboot"
	$button_promptReboot.Size = '71, 23'
	$button_promptReboot.Text = "Reboot"
	$tooltipinfo.SetToolTip($button_promptReboot, "Send a prompt to the user informing them of a reboot after a timeout, reboot upon closing the prompt or reaching the designated timeout")
	$button_promptReboot.add_Click($button_promptReboot_Click)
	$arrButtons += $button_promptReboot
	#
	# button_scheduleReboot
	#
	$button_scheduleReboot.Font = "Trebuchet MS, 8.25pt"
	$button_scheduleReboot.Location = '84, 48'
	$button_scheduleReboot.Name = "button_scheduleReboot"
	$button_scheduleReboot.Size = '71, 23'
	$button_scheduleReboot.Text = "Sc Reboot"
	$tooltipinfo.SetToolTip($button_scheduleReboot, "Schedule a reboot")
	$button_scheduleReboot.add_Click($button_scheduleReboot_Click)
	$arrButtons += $button_scheduleReboot
	#endregion groupbox_Notify

	#region General page toolbar buttons
	#
	# button_ping
	#
	$button_ping.Image = [System.Drawing.Image]::FromFile("$PSScriptRoot\images\ping.png")
	$button_ping.Name = "button_ping"
	$button_ping.Text = "Ping"
	$tooltipinfo.SetToolTip($button_ping, "Ping device")
	$button_ping.add_Click{Start-Ping}
	$arrToolbarButtons += $button_ping
	#
	# button_remot
	#
	$button_remot.Image = [System.Drawing.Image]::FromFile("$PSScriptRoot\images\rdp.png")
	$button_remot.Name = "button_remot"
	$button_remot.Text = "RDP"
	$tooltipinfo.SetToolTip($button_remot, "Open a Remote Desktop Connection")
	$button_remot.add_Click($button_remot_Click)
	$arrToolbarButtons += $button_remot
	#
	# button_PsRemoting
	#
	$button_PsRemoting.Image = [System.Drawing.Image]::FromFile("$PSScriptRoot\images\powershell.png")
	$button_PsRemoting.Name = "button_PsRemoting"
	$button_PsRemoting.Text = "PowerShell Remote"
	$tooltipinfo.SetToolTip($button_PsRemoting, "Open a Powershell Remoting Session (PSRemoting)")
	$button_PsRemoting.add_Click($button_PsRemoting_Click)
	$arrToolbarButtons += $button_PsRemoting
	#
	# buttonRemoteAssistance
	#
	$buttonRemoteAssistance.Image = [System.Drawing.Image]::FromFile("$PSScriptRoot\images\msra.png")
	$buttonRemoteAssistance.Name = "buttonRemoteAssistance"
	$buttonRemoteAssistance.Text = "Remote Assistance"
	$tooltipinfo.SetToolTip($buttonRemoteAssistance, "Offer a Remote Assistance")
	$buttonRemoteAssistance.add_Click($buttonRemoteAssistance_Click)
	$arrToolbarButtons += $buttonRemoteAssistance
	#
	# button_networkconfig
	#
	$button_networkconfig.Image = [System.Drawing.Image]::FromFile("$PSScriptRoot\images\ipconfig.png")
	$button_networkconfig.Name = "button_networkconfig"
	$button_networkconfig.Text = "IP Config"
	$tooltipinfo.SetToolTip($button_networkconfig, "Get the ip Configuration")
	$button_networkconfig.add_Click($button_networkIPConfig_Click)
	$arrToolbarButtons += $button_networkconfig
	#
	# buttonC
	#
	$buttonC.Image = [System.Drawing.Image]::FromFile("$PSScriptRoot\images\c.png")
	$buttonC.Name = "buttonC"
	$buttonC.Text = "C$"
	$tooltipinfo.SetToolTip($buttonC, "Open Explorer.exe to the C: Drive")
	$buttonC.add_Click($buttonC_Click)
	$arrToolbarButtons += $buttonC
	#
	# button_GPupdate
	#
	$button_GPupdate.Image = [System.Drawing.Image]::FromFile("$PSScriptRoot\images\gpupdate.png")
	$button_GPupdate.Name = "button_GPupdate"
	$button_GPupdate.Text = "GPupdate"
	$tooltipinfo.SetToolTip($button_GPupdate, "Group Policy update (via WMI)")
	$button_GPupdate.add_Click($button_GPupdate_Click)
	$arrToolbarButtons += $button_GPupdate
	#
	# button_Applications
	#
	$button_Applications.Image = [System.Drawing.Image]::FromFile("$PSScriptRoot\images\arp.png")
	$button_Applications.Name = "button_Applications"
	$button_Applications.Text = "Apps"
	$tooltipinfo.SetToolTip($button_Applications, "List Applications installed")
	$button_Applications.add_Click($button_Applications_Click)
	$arrToolbarButtons += $button_Applications
	#
	# buttonSendCommand
	#
	$buttonSendCommand.Image = [System.Drawing.Image]::FromFile("$PSScriptRoot\images\sendcommand.png")
	$buttonSendCommand.Name = "buttonSendCommand"
	$buttonSendCommand.Text = "Send Command"
	$buttonSendCommand.TextAlign = 'BottomCenter'
	$buttonSendCommand.add_Click($buttonSendCommand_Click)
	$arrToolbarButtons += $buttonSendCommand
	#
	# button_Restart
	#
	$button_Restart.Image = [System.Drawing.Image]::FromFile("$PSScriptRoot\images\restart.png")
	$button_Restart.Name = "button_Restart"
	$button_Restart.Text = "Restart"
	$tooltipinfo.SetToolTip($button_Restart, "Restart Computer")
	$button_Restart.add_Click($button_Restart_Click)
	$arrToolbarButtons += $button_Restart
	#
	# button_Shutdown
	#
	$button_Shutdown.Image = [System.Drawing.Image]::FromFile("$PSScriptRoot\images\shutdown.png")
	$button_Shutdown.Name = "button_Shutdown"
	$button_Shutdown.Text = "Shutdown"
	$tooltipinfo.SetToolTip($button_Shutdown, "Shutdown Computer")
	$button_Shutdown.add_Click($button_Shutdown_Click)
	$arrToolbarButtons += $button_Shutdown
	#endregion General page toolbar buttons

	#
	# tabpage_ComputerOSSystem
	#
	$tabpage_ComputerOSSystem.Controls.Add($groupbox_UsersAndGroups)
	$tabpage_ComputerOSSystem.Controls.Add($groupbox_software)
	$tabpage_ComputerOSSystem.Controls.Add($groupbox_Hardware)
	$tabpage_ComputerOSSystem.Location = '4, 22'
	$tabpage_ComputerOSSystem.Name = "tabpage_ComputerOSSystem"
	$tabpage_ComputerOSSystem.Size = '1166, 111'
	$tabpage_ComputerOSSystem.TabIndex = 13
	$tabpage_ComputerOSSystem.Text = " Computer & Operating System "
	$arrTabPages += $tabpage_ComputerOSSystem

	#
	# groupbox_UsersAndGroups
	#
	$groupbox_UsersAndGroups.Controls.Add($button_UsersGroupLocalUsers)
	$groupbox_UsersAndGroups.Controls.Add($button_UsersGroupDelLocalUsers)
	$groupbox_UsersAndGroups.Controls.Add($button_UsersGroupLocalGroups)
	$groupbox_UsersAndGroups.Controls.Add($button_UserADGroups)
	$groupbox_UsersAndGroups.Controls.Add($button_ComputerADGroups)
	$groupbox_UsersAndGroups.Location = '835, 1'
	$groupbox_UsersAndGroups.Name = "groupbox_UsersAndGroups"
	$groupbox_UsersAndGroups.Size = '246, 102'
	$groupbox_UsersAndGroups.TabIndex = 61
	$groupbox_UsersAndGroups.TabStop = $False
	$groupbox_UsersAndGroups.Text = "Users and Groups"
	$arrGroupboxes += $groupbox_UsersAndGroups
	#
	# button_UsersGroupLocalUsers
	#
	$button_UsersGroupLocalUsers.Location = '14, 16'
	$button_UsersGroupLocalUsers.Name = "button_UsersGroupLocalUsers"
	$button_UsersGroupLocalUsers.Size = '94, 23'
	$button_UsersGroupLocalUsers.TabIndex = 17
	$button_UsersGroupLocalUsers.Text = "Local Users"
	$button_UsersGroupLocalUsers.add_Click($button_UsersGroupLocalUsers_Click)
	$arrButtons += $button_UsersGroupLocalUsers
	#
	# button_UsersGroupLocalUsers
	#
	$button_UsersGroupDelLocalUsers.Location = '14, 45'
	$button_UsersGroupDelLocalUsers.Name = "button_UsersGroupDelLocalUsers"
	$button_UsersGroupDelLocalUsers.Size = '94, 23'
	$button_UsersGroupDelLocalUsers.TabIndex = 17
	$button_UsersGroupDelLocalUsers.Text = "Delete User"
	$button_UsersGroupDelLocalUsers.add_Click($button_UsersGroupDelLocalUsers_Click)
	$arrButtons += $button_UsersGroupDelLocalUsers
	#
	# button_UsersGroupLocalGroups
	#
	$button_UsersGroupLocalGroups.Location = '14, 74'
	$button_UsersGroupLocalGroups.Name = "button_UsersGroupLocalGroups"
	$button_UsersGroupLocalGroups.Size = '94, 23'
	$button_UsersGroupLocalGroups.TabIndex = 18
	$button_UsersGroupLocalGroups.Text = "Local Groups"
	$button_UsersGroupLocalGroups.add_Click($button_UsersGroupLocalGroups_Click)
	$arrButtons += $button_UsersGroupLocalGroups
	#
	# button_UsersGroupLocalUsers
	#
	$button_UserADGroups.Location = '137, 16'
	$button_UserADGroups.Name = "button_UserADGroups"
	$button_UserADGroups.Size = '94, 23'
	$button_UserADGroups.TabIndex = 17
	$button_UserADGroups.Text = "User AD Groups"
	$button_UserADGroups.add_Click($button_UserADGroups_Click)
	$arrButtons += $button_UserADGroups
	#
	# button_UsersGroupLocalGroups
	#
	$button_ComputerADGroups.Location = '137, 45'
	$button_ComputerADGroups.Name = "button_ComputerADGroups"
	$button_ComputerADGroups.Size = '94, 23'
	$button_ComputerADGroups.TabIndex = 18
	$button_ComputerADGroups.Text = "AD Groups"
	$button_ComputerADGroups.add_Click($button_ComputerADGroups_Click)
	$arrButtons += $button_ComputerADGroups
	#
	# groupbox_software
	#
	$groupbox_software.Controls.Add($groupbox_ComputerDescription)
	$groupbox_software.Controls.Add($groupbox2)
	$groupbox_software.Controls.Add($groupbox_RemoteDesktop)
	$groupbox_software.Controls.Add($buttonApplications)
	$groupbox_software.Controls.Add($button_PageFile)
	$groupbox_software.Controls.Add($button_HostsFile)
	$groupbox_software.Controls.Add($button_Pend_Reboot)
	$groupbox_software.Controls.Add($button_StartupCommand)
	$groupbox_software.Location = '212, 1'
	$groupbox_software.Name = "groupbox_software"
	$groupbox_software.Size = '617, 102'
	$groupbox_software.TabIndex = 60
	$groupbox_software.TabStop = $False
	$groupbox_software.Text = "Operating System / Softwares"
	$arrGroupboxes += $groupbox_software
	#
	# groupbox_ComputerDescription
	#
	$groupbox_ComputerDescription.Controls.Add($button_ComputerDescriptionChange)
	$groupbox_ComputerDescription.Controls.Add($button_ComputerDescriptionQuery)
	$groupbox_ComputerDescription.Location = '474, 52'
	$groupbox_ComputerDescription.Name = "groupbox_ComputerDescription"
	$groupbox_ComputerDescription.Size = '138, 48'
	$groupbox_ComputerDescription.TabIndex = 57
	$groupbox_ComputerDescription.TabStop = $False
	$groupbox_ComputerDescription.Text = "Computer Description"
	$arrGroupboxes += $groupbox_ComputerDescription
	#
	# button_ComputerDescriptionChange
	#
	$button_ComputerDescriptionChange.Location = '71, 19'
	$button_ComputerDescriptionChange.Name = "button_ComputerDescriptionChange"
	$button_ComputerDescriptionChange.Size = '59, 23'
	$button_ComputerDescriptionChange.TabIndex = 57
	$button_ComputerDescriptionChange.Text = "Set"
	$button_ComputerDescriptionChange.add_Click($button_ComputerDescriptionChange_Click)
	$arrButtons += $button_ComputerDescriptionChange
	#
	# button_ComputerDescriptionQuery
	#
	$button_ComputerDescriptionQuery.Location = '6, 19'
	$button_ComputerDescriptionQuery.Name = "button_ComputerDescriptionQuery"
	$button_ComputerDescriptionQuery.Size = '59, 23'
	$button_ComputerDescriptionQuery.TabIndex = 56
	$button_ComputerDescriptionQuery.Text = "Query"
	$button_ComputerDescriptionQuery.add_Click($button_ComputerDescriptionQuery_Click)
	$arrButtons += $button_ComputerDescriptionQuery
	#
	# groupbox2
	#
	$groupbox2.Controls.Add($buttonReportingEventslog)
	$groupbox2.Controls.Add($button_HotFix)
	$groupbox2.Controls.Add($buttonWindowsUpdateLog)
	$groupbox2.Location = '217, 7'
	$groupbox2.Name = "groupbox2"
	$groupbox2.Size = '251, 92'
	$groupbox2.TabIndex = 59
	$groupbox2.TabStop = $False
	$groupbox2.Text = "Updates and Deployment"
	$arrGroupboxes += $groupbox2
	#
	# buttonReportingEventslog
	#
	$buttonReportingEventslog.Location = '119, 16'
	$buttonReportingEventslog.Name = "buttonReportingEventslog"
	$buttonReportingEventslog.Size = '123, 23'
	$buttonReportingEventslog.TabIndex = 46
	$buttonReportingEventslog.Text = "ReportingEvents.log"
	$tooltipinfo.SetToolTip($buttonReportingEventslog, "Get WSUS Report")
	$buttonReportingEventslog.add_Click($buttonReportingEventslog_Click)
	$arrButtons += $buttonReportingEventslog
	#
	# button_HotFix
	#
	$button_HotFix.Location = '6, 16'
	$button_HotFix.Name = "button_HotFix"
	$button_HotFix.Size = '107, 23'
	$button_HotFix.TabIndex = 50
	$button_HotFix.Text = "Get-HotFix"
	$button_HotFix.add_Click($button_HotFix_Click)
	$arrButtons += $button_HotFix
	#
	# buttonWindowsUpdateLog
	#
	$buttonWindowsUpdateLog.Location = '119, 41'
	$buttonWindowsUpdateLog.Name = "buttonWindowsUpdateLog"
	$buttonWindowsUpdateLog.Size = '123, 23'
	$buttonWindowsUpdateLog.TabIndex = 45
	$buttonWindowsUpdateLog.Text = "WindowsUpdate.log"
	$tooltipinfo.SetToolTip($buttonWindowsUpdateLog, "Open the WindowsUpdate.log")
	$buttonWindowsUpdateLog.add_Click($buttonWindowsUpdateLog_Click)
	$arrButtons += $buttonWindowsUpdateLog
	#
	# groupbox_RemoteDesktop
	#
	$groupbox_RemoteDesktop.Controls.Add($button_RDPDisable)
	$groupbox_RemoteDesktop.Controls.Add($button_RDPEnable)
	$groupbox_RemoteDesktop.Location = '474, 7'
	$groupbox_RemoteDesktop.Name = "groupbox_RemoteDesktop"
	$groupbox_RemoteDesktop.Size = '138, 42'
	$groupbox_RemoteDesktop.TabIndex = 58
	$groupbox_RemoteDesktop.TabStop = $False
	$groupbox_RemoteDesktop.Text = "Remote Desktop"
	$arrGroupboxes += $groupbox_RemoteDesktop
	#
	# button_RDPDisable
	#
	$button_RDPDisable.Location = '71, 16'
	$button_RDPDisable.Name = "button_RDPDisable"
	$button_RDPDisable.Size = '59, 23'
	$button_RDPDisable.TabIndex = 49
	$button_RDPDisable.Text = "Disable"
	$button_RDPDisable.add_Click($button_RDPDisable_Click)
	$arrButtons += $button_RDPDisable
	#
	# button_RDPEnable
	#
	$button_RDPEnable.Location = '6, 16'
	$button_RDPEnable.Name = "button_RDPEnable"
	$button_RDPEnable.Size = '59, 23'
	$button_RDPEnable.TabIndex = 48
	$button_RDPEnable.Text = "Enable"
	$button_RDPEnable.add_Click($button_RDPEnable_Click)
	$arrButtons += $button_RDPEnable
	#
	# buttonApplications
	#
	$buttonApplications.Location = '6, 19'
	$buttonApplications.Name = "buttonApplications"
	$buttonApplications.Size = '114, 23'
	$buttonApplications.TabIndex = 56
	$buttonApplications.Text = "Applications"
	$buttonApplications.add_Click($button_Applications_Click)
	$arrButtons += $buttonApplications
	#
	# button_PageFile
	#
	$button_PageFile.Location = '6, 48'
	$button_PageFile.Name = "button_PageFile"
	$button_PageFile.Size = '114, 23'
	$button_PageFile.TabIndex = 52
	$button_PageFile.Text = "PageFile"
	$button_PageFile.add_Click($button_PageFile_Click)
	$arrButtons += $button_PageFile
	#
	# button_HostsFile
	#
	$button_HostsFile.Location = '126, 19'
	$button_HostsFile.Name = "button_HostsFile"
	$button_HostsFile.Size = '85, 23'
	$button_HostsFile.TabIndex = 26
	$button_HostsFile.Text = "Hosts File"
	$button_HostsFile.add_Click($button_HostsFile_Click)
	$arrButtons += $button_HostsFile
	#
	# button_Pend_Reboot
	#
	$button_Pend_Reboot.Location = '126, 48'
	$button_Pend_Reboot.Name = "button_Pend_Reboot"
	$button_Pend_Reboot.Size = '85,23'
	$button_Pend_Reboot.Text = "PendReboot"
	$button_Pend_Reboot.add_Click($button_Pend_Reboot_Click)
	$arrButtons += $button_Pend_Reboot
	#
	# button_StartupCommand
	#
	$button_StartupCommand.Location = '6, 76'
	$button_StartupCommand.Name = "button_StartupCommand"
	$button_StartupCommand.Size = '114, 23'
	$button_StartupCommand.TabIndex = 27
	$button_StartupCommand.Text = "Startup Commands"
	$button_StartupCommand.add_Click($button_StartupCommand_Click)
	$arrButtons += $button_StartupCommand
	#
	# groupbox_Hardware
	#
	$groupbox_Hardware.Controls.Add($button_MotherBoard)
	$groupbox_Hardware.Controls.Add($button_Processor)
	$groupbox_Hardware.Controls.Add($button_Memory)
	$groupbox_Hardware.Controls.Add($button_SystemType)
	$groupbox_Hardware.Controls.Add($button_Printers)
	$groupbox_Hardware.Controls.Add($button_USBDevices)
	$groupbox_Hardware.Location = '2, 1'
	$groupbox_Hardware.Name = "groupbox_Hardware"
	$groupbox_Hardware.Size = '204, 102'
	$groupbox_Hardware.TabIndex = 59
	$groupbox_Hardware.TabStop = $False
	$groupbox_Hardware.Text = "Hardware"
	$arrGroupboxes += $groupbox_Hardware
	#
	# button_MotherBoard
	#
	$button_MotherBoard.Location = '6, 19'
	$button_MotherBoard.Name = "button_MotherBoard"
	$button_MotherBoard.Size = '93, 23'
	$button_MotherBoard.TabIndex = 54
	$button_MotherBoard.Text = "MotherBoard"
	$button_MotherBoard.add_Click($button_MotherBoard_Click)
	$arrButtons += $button_MotherBoard
	#
	# button_Processor
	#
	$button_Processor.Location = '6, 48'
	$button_Processor.Name = "button_Processor"
	$button_Processor.Size = '93, 23'
	$button_Processor.TabIndex = 53
	$button_Processor.Text = "Processor"
	$button_Processor.add_Click($button_Processor_Click)
	$arrButtons += $button_Processor
	#
	# button_Memory
	#
	$button_Memory.Font = "Trebuchet MS, 8.25pt"
	$button_Memory.Location = '6, 77'
	$button_Memory.Name = "button_Memory"
	$button_Memory.Size = '93, 23'
	$button_Memory.TabIndex = 22
	$button_Memory.Text = "Memory"
	$button_Memory.add_Click($button_Memory_Click)
	$arrButtons += $button_Memory
	#
	# button_SystemType
	#
	$button_SystemType.Location = '105, 19'
	$button_SystemType.Name = "button_SystemType"
	$button_SystemType.Size = '93, 23'
	$button_SystemType.TabIndex = 55
	$button_SystemType.Text = "System Type"
	$button_SystemType.add_Click($button_SystemType_Click)
	$arrButtons += $button_SystemType
	#
	# button_Printers
	#
	$button_Printers.Location = '105, 77'
	$button_Printers.Name = "button_Printers"
	$button_Printers.Size = '93, 23'
	$button_Printers.TabIndex = 51
	$button_Printers.Text = "Printers"
	$button_Printers.add_Click($button_Printers_Click)
	$arrButtons += $button_Printers
	#
	# button_USBDevices
	#
	$button_USBDevices.Location = '105, 48'
	$button_USBDevices.Name = "button_USBDevices"
	$button_USBDevices.Size = '93, 23'
	$button_USBDevices.TabIndex = 47
	$button_USBDevices.Text = "USB Devices"
	$button_USBDevices.add_Click($button_USBDevices_Click)
	$arrButtons += $button_USBDevices
	#
	# tabpage_network
	#
	$tabpage_network.Location = '4, 22'
	$tabpage_network.Name = "tabpage_network"
	$tabpage_network.Size = '1162, 111'
	$tabpage_network.TabIndex = 6
	$tabpage_network.Text = "Network"
	$arrTabPages += $tabpage_network

	#
	# button_networkIPConfig
	#
	$button_networkIPConfig.Name = "button_networkIPConfig"
	$button_networkIPConfig.Text = "IPConfig"
	$tooltipinfo.SetToolTip($button_networkIPConfig, "Get the ip configuration")
	$arrTabPageNetworkButtons += $button_networkIPConfig
	$button_networkIPConfig.add_Click($button_networkIPConfig_Click)
	#
	# button_ConnectivityTesting
	#
	$button_ConnectivityTesting.Name = "button_ConnectivityTesting"
	$button_ConnectivityTesting.Text = "Connectivity Tests (slow)"
	$tooltipinfo.SetToolTip($button_ConnectivityTesting, "Connectivity Testing")
	$arrTabPageNetworkButtons += $button_ConnectivityTesting
	$button_ConnectivityTesting.add_Click($button_ConnectivityTesting_Click)
	#
	# button_networkTestPort
	#
	$button_networkTestPort.Name = "button_networkTestPort"
	$button_networkTestPort.Text = "Test a Port"
	$tooltipinfo.SetToolTip($button_networkTestPort, "Test a port (default = 80)")
	$arrTabPageNetworkButtons += $button_networkTestPort
	$button_networkTestPort.add_Click($button_networkTestPort_Click)
	#
	# button_NIC
	#
	$button_NIC.Name = "button_NIC"
	$button_NIC.Text = "Network Interface (slow)"
	$tooltipinfo.SetToolTip($button_NIC, "Get the network interface card(s) information")
	$arrTabPageNetworkButtons += $button_NIC
	$button_NIC.add_Click($button_NIC_Click)
	#
	# button_networkRouteTable
	#
	$button_networkRouteTable.Name = "button_networkRouteTable"
	$button_networkRouteTable.Text = "Route Table"
	$tooltipinfo.SetToolTip($button_networkRouteTable, "Get the route table")
	$arrTabPageNetworkButtons += $button_networkRouteTable
	$button_networkRouteTable.add_Click($button_networkRouteTable_Click)	
	#
	# tabpage_processes
	#
	$tabpage_processes.Controls.Add($buttonCommandLineGridView)
	$tabpage_processes.Controls.Add($button_processAll)
	$tabpage_processes.Controls.Add($buttonCommandLine)
	$tabpage_processes.Controls.Add($groupbox1)
	$tabpage_processes.Controls.Add($button_process100MB)
	$tabpage_processes.Controls.Add($button_ProcessGrid)
	$tabpage_processes.Controls.Add($button_processOwners)
	$tabpage_processes.Controls.Add($button_processLastHour)
	$tabpage_processes.Location = '4, 22'
	$tabpage_processes.Name = "tabpage_processes"
	$tabpage_processes.Size = '1162, 111'
	$tabpage_processes.TabIndex = 3
	$tabpage_processes.Text = "Processes"
	$arrTabPages += $tabpage_processes

	#
	# buttonCommandLineGridView
	#
	$buttonCommandLineGridView.Location = '284, 61'
	$buttonCommandLineGridView.Name = "buttonCommandLineGridView"
	$buttonCommandLineGridView.Size = '159, 23'
	$buttonCommandLineGridView.TabIndex = 17
	$buttonCommandLineGridView.Text = "CommandLine - GridView"
	$buttonCommandLineGridView.add_Click($buttonCommandLineGridView_Click)
	$arrButtons += $buttonCommandLineGridView
	#
	# button_processAll
	#
	$button_processAll.Location = '8, 3'
	$button_processAll.Name = "button_processAll"
	$button_processAll.Size = '132, 23'
	$button_processAll.TabIndex = 5
	$button_processAll.Text = "Processes"
	$tooltipinfo.SetToolTip($button_processAll, "Get all the processes")
	$button_processAll.add_Click($button_processAll_Click)
	$arrButtons += $button_processAll
	#
	# buttonCommandLine
	#
	$buttonCommandLine.Location = '146, 61'
	$buttonCommandLine.Name = "buttonCommandLine"
	$buttonCommandLine.Size = '132, 23'
	$buttonCommandLine.TabIndex = 15
	$buttonCommandLine.Text = "CommandLine"
	$tooltipinfo.SetToolTip($buttonCommandLine, "Get the processes CommandLine")
	$buttonCommandLine.add_Click($buttonCommandLine_Click)
	$arrButtons += $buttonCommandLine
	#
	# groupbox1
	#
	$groupbox1.Controls.Add($textbox_processName)
	$groupbox1.Controls.Add($label_processEnterAProcessName)
	$groupbox1.Controls.Add($button_processTerminate)
	$groupbox1.Location = '870, 17'
	$groupbox1.Name = "groupbox1"
	$groupbox1.Size = '231, 83'
	$groupbox1.TabIndex = 16
	$groupbox1.TabStop = $False
	$groupbox1.Text = "Terminate a Process"
	$arrGroupboxes += $groupbox1
	#
	# textbox_processName
	#
	[void]$textbox_processName.AutoCompleteCustomSource.Add("dhcp")
	[void]$textbox_processName.AutoCompleteCustomSource.Add("iisadmin")
	[void]$textbox_processName.AutoCompleteCustomSource.Add("msftpsvc")
	[void]$textbox_processName.AutoCompleteCustomSource.Add("nntpsvc")
	[void]$textbox_processName.AutoCompleteCustomSource.Add("omniinet")
	[void]$textbox_processName.AutoCompleteCustomSource.Add("smtpsvc")
	[void]$textbox_processName.AutoCompleteCustomSource.Add("spooler")
	[void]$textbox_processName.AutoCompleteCustomSource.Add("sql")
	[void]$textbox_processName.AutoCompleteCustomSource.Add("w3svc")
	$textbox_processName.AutoCompleteMode = 'Suggest'
	$textbox_processName.Location = '6, 19'
	$textbox_processName.Name = "textbox_processName"
	$textbox_processName.Size = '116, 20'
	$textbox_processName.TabIndex = 12
	$textbox_processName.Text = "<ProcessName>"
	$tooltipinfo.SetToolTip($textbox_processName, "Enter a process name, example ""notepad.exe""")
	#
	# label_processEnterAProcessName
	#
	$label_processEnterAProcessName.Font = "Trebuchet MS, 6.75pt, style=Italic"
	$label_processEnterAProcessName.Location = '6, 41'
	$label_processEnterAProcessName.Name = "label_processEnterAProcessName"
	$label_processEnterAProcessName.Size = '206, 23'
	$label_processEnterAProcessName.TabIndex = 14
	$label_processEnterAProcessName.Text = "Enter a Process Name (ex:notepad.exe)"
	$label_processEnterAProcessName.TextAlign = 'MiddleCenter'
	#
	# button_processTerminate
	#
	$button_processTerminate.Font = "Microsoft Sans Serif, 8.25pt, style=Bold"
	$button_processTerminate.ForeColor = 'Red'
	$button_processTerminate.Location = '128, 18'
	$button_processTerminate.Name = "button_processTerminate"
	$button_processTerminate.Size = '84, 23'
	$button_processTerminate.TabIndex = 13
	$button_processTerminate.Text = "Terminate"
	$tooltipinfo.SetToolTip($button_processTerminate, "Terminate the process specified")
	$button_processTerminate.add_Click($button_processTerminate_Click)
	$arrButtons += $button_processTerminate
	#
	# button_process100MB
	#
	$button_process100MB.Location = '8, 32'
	$button_process100MB.Name = "button_process100MB"
	$button_process100MB.Size = '132, 23'
	$button_process100MB.TabIndex = 0
	$button_process100MB.Text = "+100MB Memory"
	$tooltipinfo.SetToolTip($button_process100MB, "Get all the processes using more than 100MB of memory")
	$button_process100MB.add_Click($button_process100MB_Click)
	$arrButtons += $button_process100MB
	#
	# button_ProcessGrid
	#
	$button_ProcessGrid.Location = '146, 32'
	$button_ProcessGrid.Name = "button_ProcessGrid"
	$button_ProcessGrid.Size = '132, 23'
	$button_ProcessGrid.TabIndex = 6
	$button_ProcessGrid.Text = "Processes - Grid View"
	$tooltipinfo.SetToolTip($button_ProcessGrid, "Get all the processes in a Grid View form")
	$button_ProcessGrid.add_Click($button_ProcessGrid_Click)
	$arrButtons += $button_processGrid
	#
	# button_processOwners
	#
	$button_processOwners.Location = '8, 61'
	$button_processOwners.Name = "button_processOwners"
	$button_processOwners.Size = '132, 23'
	$button_processOwners.TabIndex = 4
	$button_processOwners.Text = "Owners"
	$tooltipinfo.SetToolTip($button_processOwners, "Get the owners of each processes")
	$button_processOwners.add_Click($button_processOwners_Click)
	$arrButtons += $button_processOwners
	#
	# button_processLastHour
	#
	$button_processLastHour.Location = '146, 3'
	$button_processLastHour.Name = "button_processLastHour"
	$button_processLastHour.Size = '132, 23'
	$button_processLastHour.TabIndex = 7
	$button_processLastHour.Text = "Started in Last hour"
	$tooltipinfo.SetToolTip($button_processLastHour, "Get the processes started in the last hour")
	$button_processLastHour.add_Click($button_processLastHour_Click)
	$arrButtons += $button_processLastHour
	#
	# tabpage_services
	#
	$tabpage_services.Controls.Add($button_servicesNonStandardUser)
	$tabpage_services.Controls.Add($button_mmcServices)
	$tabpage_services.Controls.Add($button_servicesAutoNotStarted)
	$tabpage_services.Controls.Add($groupbox_Service_QueryStartStop)
	$tabpage_services.Controls.Add($button_servicesRunning)
	$tabpage_services.Controls.Add($button_servicesAll)
	$tabpage_services.Controls.Add($button_servicesGridView)
	$tabpage_services.Controls.Add($button_servicesAutomatic)
	$tabpage_services.Location = '4, 22'
	$tabpage_services.Name = "tabpage_services"
	$tabpage_services.Size = '1162, 111'
	$tabpage_services.TabIndex = 2
	$tabpage_services.Text = "Services"
	$tooltipinfo.SetToolTip($tabpage_services, "Services")
	$arrTabPages += $tabpage_services
	#
	# button_servicesNonStandardUser
	#
	$button_servicesNonStandardUser.Location = '285, 3'
	$button_servicesNonStandardUser.Name = "button_servicesNonStandardUser"
	$button_servicesNonStandardUser.Size = '133, 23'
	$button_servicesNonStandardUser.TabIndex = 8
	$button_servicesNonStandardUser.Text = "Non Standard User"
	$tooltipinfo.SetToolTip($button_servicesNonStandardUser, "Services set with a Non-Standard user account")
	$button_servicesNonStandardUser.add_Click($button_servicesNonStandardUser_Click)
	$arrButtons += $button_servicesNonStandardUser
	#
	# button_mmcServices
	#
	$button_mmcServices.Location = '8, 3'
	$button_mmcServices.Name = "button_mmcServices"
	$button_mmcServices.Size = '132, 23'
	$button_mmcServices.TabIndex = 0
	$button_mmcServices.Text = "MMC: Services.msc"
	$tooltipinfo.SetToolTip($button_mmcServices, "Launch Services.msc")
	$button_mmcServices.add_Click($button_mmcServices_Click)
	$arrButtons += $button_mmcServices
	#
	# button_servicesAutoNotStarted
	#
	$button_servicesAutoNotStarted.Location = '146, 61'
	$button_servicesAutoNotStarted.Name = "button_servicesAutoNotStarted"
	$button_servicesAutoNotStarted.Size = '133, 23'
	$button_servicesAutoNotStarted.TabIndex = 9
	$button_servicesAutoNotStarted.Text = "Auto and NOT Running"
	$tooltipinfo.SetToolTip($button_servicesAutoNotStarted, "Services with StartupType ""Automatic"" and Status different of ""Running""")
	$button_servicesAutoNotStarted.add_Click($button_servicesAutoNotStarted_Click)
	$arrButtons += $button_servicesAutoNotStarted
	#
	# groupbox_Service_QueryStartStop
	#
	$groupbox_Service_QueryStartStop.Controls.Add($textbox_servicesAction)
	$groupbox_Service_QueryStartStop.Controls.Add($button_servicesRestart)
	$groupbox_Service_QueryStartStop.Controls.Add($label_servicesEnterAServiceName)
	$groupbox_Service_QueryStartStop.Controls.Add($button_servicesQuery)
	$groupbox_Service_QueryStartStop.Controls.Add($button_servicesStart)
	$groupbox_Service_QueryStartStop.Controls.Add($button_servicesStop)
	$groupbox_Service_QueryStartStop.Location = '872, 10'
	$groupbox_Service_QueryStartStop.Name = "groupbox_Service_QueryStartStop"
	$groupbox_Service_QueryStartStop.Size = '274, 90'
	$groupbox_Service_QueryStartStop.TabIndex = 13
	$groupbox_Service_QueryStartStop.TabStop = $False
	$groupbox_Service_QueryStartStop.Text = "QueryStartStop"
	$arrGroupboxes += $groupbox_Service_QueryStartStop
	#
	# textbox_servicesAction
	#
	[void]$textbox_servicesAction.AutoCompleteCustomSource.Add("dhcp")
	[void]$textbox_servicesAction.AutoCompleteCustomSource.Add("iisadmin")
	[void]$textbox_servicesAction.AutoCompleteCustomSource.Add("msftpsvc")
	[void]$textbox_servicesAction.AutoCompleteCustomSource.Add("nntpsvc")
	[void]$textbox_servicesAction.AutoCompleteCustomSource.Add("omniinet")
	[void]$textbox_servicesAction.AutoCompleteCustomSource.Add("smtpsvc")
	[void]$textbox_servicesAction.AutoCompleteCustomSource.Add("spooler")
	[void]$textbox_servicesAction.AutoCompleteCustomSource.Add("sql")
	[void]$textbox_servicesAction.AutoCompleteCustomSource.Add("w3svc")
	$textbox_servicesAction.AutoCompleteMode = 'Suggest'
	$textbox_servicesAction.Location = '6, 19'
	$textbox_servicesAction.Name = "textbox_servicesAction"
	$textbox_servicesAction.Size = '116, 20'
	$textbox_servicesAction.TabIndex = 11
	$textbox_servicesAction.Text = "<ServiceName>"
	$tooltipinfo.SetToolTip($textbox_servicesAction, "Please Enter a Service Name")
	$textbox_servicesAction.add_Click($textbox_servicesAction_Click)
	#
	# button_servicesRestart
	#
	$button_servicesRestart.Font = "Microsoft Sans Serif, 8.25pt, style=Bold"
	$button_servicesRestart.Location = '200, 15'
	$button_servicesRestart.Name = "button_servicesRestart"
	$button_servicesRestart.Size = '68, 23'
	$button_servicesRestart.TabIndex = 10
	$button_servicesRestart.Text = "Restart"
	$tooltipinfo.SetToolTip($button_servicesRestart, "Restart the service specified")
	$button_servicesRestart.add_Click($button_servicesRestart_Click)
	$arrButtons += $button_servicesRestart
	#
	# label_servicesEnterAServiceName
	#
	$label_servicesEnterAServiceName.Font = "Trebuchet MS, 6.75pt, style=Italic"
	$label_servicesEnterAServiceName.Location = '6, 37'
	$label_servicesEnterAServiceName.Name = "label_servicesEnterAServiceName"
	$label_servicesEnterAServiceName.Size = '116, 15'
	$label_servicesEnterAServiceName.TabIndex = 12
	$label_servicesEnterAServiceName.Text = "Enter a Service Name"
	$label_servicesEnterAServiceName.TextAlign = 'MiddleCenter'
	#
	# button_servicesQuery
	#
	$button_servicesQuery.Font = "Microsoft Sans Serif, 8.25pt, style=Bold"
	$button_servicesQuery.Location = '128, 19'
	$button_servicesQuery.Name = "button_servicesQuery"
	$button_servicesQuery.Size = '59, 23'
	$button_servicesQuery.TabIndex = 4
	$button_servicesQuery.Text = "Query"
	$tooltipinfo.SetToolTip($button_servicesQuery, "Get the service specified information")
	$button_servicesQuery.add_Click($button_servicesQuery_Click)
	$arrButtons += $button_servicesQuery
	#
	# button_servicesStart
	#
	$button_servicesStart.Font = "Microsoft Sans Serif, 8.25pt, style=Bold"
	$button_servicesStart.ForeColor = 'LightBlue'
	$button_servicesStart.Location = '200, 37'
	$button_servicesStart.Name = "button_servicesStart"
	$button_servicesStart.Size = '68, 23'
	$button_servicesStart.TabIndex = 6
	$button_servicesStart.Text = "Start"
	$tooltipinfo.SetToolTip($button_servicesStart, "Start the service specified")
	$button_servicesStart.add_Click($button_servicesStart_Click)
	$arrButtons += $button_servicesStart
	#
	# button_servicesStop
	#
	$button_servicesStop.Font = "Microsoft Sans Serif, 8.25pt, style=Bold"
	$button_servicesStop.ForeColor = 'Red'
	$button_servicesStop.Location = '200, 59'
	$button_servicesStop.Name = "button_servicesStop"
	$button_servicesStop.Size = '68, 23'
	$button_servicesStop.TabIndex = 5
	$button_servicesStop.Text = "Stop"
	$tooltipinfo.SetToolTip($button_servicesStop, "Stop the service specified")
	$button_servicesStop.add_Click($button_servicesStop_Click)
	$arrButtons += $button_servicesStop
	#
	# button_servicesRunning
	#
	$button_servicesRunning.Location = '146, 3'
	$button_servicesRunning.Name = "button_servicesRunning"
	$button_servicesRunning.Size = '133, 23'
	$button_servicesRunning.TabIndex = 1
	$button_servicesRunning.Text = "Running"
	$tooltipinfo.SetToolTip($button_servicesRunning, "Services with Status = Running")
	$button_servicesRunning.add_Click($button_servicesRunning_Click)
	$arrButtons += $button_servicesRunning
	#
	# button_servicesAll
	#
	$button_servicesAll.Location = '8, 32'
	$button_servicesAll.Name = "button_servicesAll"
	$button_servicesAll.Size = '132, 23'
	$button_servicesAll.TabIndex = 3
	$button_servicesAll.Text = "Services"
	$tooltipinfo.SetToolTip($button_servicesAll, "Get all the services")
	$button_servicesAll.add_Click($button_servicesAll_Click)
	$arrButtons += $button_servicesAll
	#
	# button_servicesGridView
	#
	$button_servicesGridView.Location = '8, 61'
	$button_servicesGridView.Name = "button_servicesGridView"
	$button_servicesGridView.Size = '132, 23'
	$button_servicesGridView.TabIndex = 7
	$button_servicesGridView.Text = "Services - GridView"
	$tooltipinfo.SetToolTip($button_servicesGridView, "Get all the services in a Grid View form")
	$button_servicesGridView.add_Click($button_servicesGridView_Click)
	$arrButtons += $button_servicesGridView
	#
	# button_servicesAutomatic
	#
	$button_servicesAutomatic.Location = '146, 32'
	$button_servicesAutomatic.Name = "button_servicesAutomatic"
	$button_servicesAutomatic.Size = '133, 23'
	$button_servicesAutomatic.TabIndex = 2
	$button_servicesAutomatic.Text = "Automatic"
	$tooltipinfo.SetToolTip($button_servicesAutomatic, "Services with StartupType = Automatic")
	$button_servicesAutomatic.add_Click($button_servicesAutomatic_Click)
	$arrButtons += $button_servicesAutomatic
	#
	# tabpage_diskdrives
	#
	$tabpage_diskdrives.Controls.Add($button_DiskUsage)
	$tabpage_diskdrives.Controls.Add($button_DiskPhysical)
	$tabpage_diskdrives.Controls.Add($button_DiskPartition)
	$tabpage_diskdrives.Controls.Add($button_DiskLogical)
	$tabpage_diskdrives.Controls.Add($button_DiskMountPoint)
	$tabpage_diskdrives.Controls.Add($button_DiskRelationship)
	$tabpage_diskdrives.Controls.Add($button_DiskMappedDrive)
	$tabpage_diskdrives.Location = '4, 22'
	$tabpage_diskdrives.Name = "tabpage_diskdrives"
	$tabpage_diskdrives.Size = '1162, 111'
	$tabpage_diskdrives.TabIndex = 10
	$tabpage_diskdrives.Text = "Disk Drives"
	$arrTabPages += $tabpage_diskdrives
	#
	# button_DiskUsage
	#
	$button_DiskUsage.Location = '8, 3'
	$button_DiskUsage.Name = "button_DiskUsage"
	$button_DiskUsage.Size = '112, 23'
	$button_DiskUsage.TabIndex = 22
	$button_DiskUsage.Text = "Disk Usage"
	$tooltipinfo.SetToolTip($button_DiskUsage, "Get the Disk(s) Usage")
	$button_DiskUsage.add_Click($button_DiskUsage_Click)
	$arrButtons += $button_DiskUsage
	#
	# button_DiskPhysical
	#
	$button_DiskPhysical.Location = '126, 3'
	$button_DiskPhysical.Name = "button_DiskPhysical"
	$button_DiskPhysical.Size = '112, 23'
	$button_DiskPhysical.TabIndex = 12
	$button_DiskPhysical.Text = "Physical Disks"
	$tooltipinfo.SetToolTip($button_DiskPhysical, "Get the physical disk(s)")
	$button_DiskPhysical.add_Click($button_DiskPhysical_Click)
	$arrButtons += $button_DiskPhysical
	#
	# button_DiskPartition
	#
	$button_DiskPartition.Location = '244, 3'
	$button_DiskPartition.Name = "button_DiskPartition"
	$button_DiskPartition.Size = '112, 23'
	$button_DiskPartition.TabIndex = 17
	$button_DiskPartition.Text = "Partition"
	$tooltipinfo.SetToolTip($button_DiskPartition, "Get the partition(s)")
	$button_DiskPartition.add_Click($button_DiskPartition_Click)
	$arrButtons += $button_DiskPartition
	#
	# button_DiskLogical
	#
	$button_DiskLogical.Location = '126, 32'
	$button_DiskLogical.Name = "button_DiskLogical"
	$button_DiskLogical.Size = '112, 23'
	$button_DiskLogical.TabIndex = 13
	$button_DiskLogical.Text = "Logical Disk"
	$tooltipinfo.SetToolTip($button_DiskLogical, "Get the logical disk(s)")
	$button_DiskLogical.add_Click($button_DiskLogical_Click)
	$arrButtons += $button_DiskLogical
	#
	# button_DiskMountPoint
	#
	$button_DiskMountPoint.Location = '126, 61'
	$button_DiskMountPoint.Name = "button_DiskMountPoint"
	$button_DiskMountPoint.Size = '112, 23'
	$button_DiskMountPoint.TabIndex = 20
	$button_DiskMountPoint.Text = "MountPoint"
	$tooltipinfo.SetToolTip($button_DiskMountPoint, "Get the mountpoint(s)")
	$button_DiskMountPoint.add_Click($button_DiskMountPoint_Click)
	$arrButtons += $button_DiskMountPoint
	#
	# button_DiskRelationship
	#
	$button_DiskRelationship.Location = '8, 61'
	$button_DiskRelationship.Name = "button_DiskRelationship"
	$button_DiskRelationship.Size = '112, 23'
	$button_DiskRelationship.TabIndex = 19
	$button_DiskRelationship.Text = "Disk Relationship"
	$tooltipinfo.SetToolTip($button_DiskRelationship, "Get the disk(s) relationship")
	$button_DiskRelationship.add_Click($button_DiskRelationship_Click)
	$arrButtons += $button_DiskRelationship
	#
	# button_DiskMappedDrive
	#
	$button_DiskMappedDrive.Location = '8, 32'
	$button_DiskMappedDrive.Name = "button_DiskMappedDrive"
	$button_DiskMappedDrive.Size = '112, 23'
	$button_DiskMappedDrive.TabIndex = 21
	$button_DiskMappedDrive.Text = "Mapped Drive"
	$tooltipinfo.SetToolTip($button_DiskMappedDrive, "Get the Active mapped drive(s)")
	$button_DiskMappedDrive.add_Click($button_DiskMappedDrive_Click)
	$arrButtons += $button_DiskMappedDrive
	#
	# tabpage_shares
	#
	$tabpage_shares.Controls.Add($button_mmcShares)
	$tabpage_shares.Controls.Add($button_SharesGrid)
	$tabpage_shares.Controls.Add($button_Shares)
	$tabpage_shares.Location = '4, 22'
	$tabpage_shares.Name = "tabpage_shares"
	$tabpage_shares.Size = '1162, 111'
	$tabpage_shares.TabIndex = 7
	$tabpage_shares.Text = "Shares"
	$arrTabPages += $tabpage_shares

	#
	# button_mmcShares
	#
	$button_mmcShares.Location = '8, 3'
	$button_mmcShares.Name = "button_mmcShares"
	$button_mmcShares.Size = '140, 23'
	$button_mmcShares.TabIndex = 1
	$button_mmcShares.Text = "MMC: Shares"
	$tooltipinfo.SetToolTip($button_mmcShares, "Launch the shared folders console (fsmgmt.msc)")
	$button_mmcShares.add_Click($button_mmcShares_Click)
	$arrButtons += $button_mmcShares
	#
	# button_SharesGrid
	#
	$button_SharesGrid.Location = '8, 61'
	$button_SharesGrid.Name = "button_SharesGrid"
	$button_SharesGrid.Size = '140, 23'
	$button_SharesGrid.TabIndex = 16
	$button_SharesGrid.Text = "Shares - GridView"
	$tooltipinfo.SetToolTip($button_SharesGrid, "Get a list of all the shares in a Grid View form")
	$button_SharesGrid.add_Click($button_SharesGrid_Click)
	$arrButtons += $button_SharesGrid
	#
	# button_Shares
	#
	$button_Shares.Location = '8, 32'
	$button_Shares.Name = "button_Shares"
	$button_Shares.Size = '140, 23'
	$button_Shares.TabIndex = 0
	$button_Shares.Text = "Shares"
	$tooltipinfo.SetToolTip($button_Shares, "Get a list of all the shares with a local path")
	$button_Shares.add_Click($button_Shares_Click)
	$arrButtons += $button_Shares
	#
	# tabpage_eventlog
	#
	$tabpage_eventlog.Controls.Add($button_RebootHistory)
	$tabpage_eventlog.Controls.Add($button_mmcEvents)
	$tabpage_eventlog.Controls.Add($button_EventsSearch)
	$tabpage_eventlog.Controls.Add($button_EventsLogNames)
	$tabpage_eventlog.Controls.Add($button_EventsLast20)
	$tabpage_eventlog.Location = '4, 22'
	$tabpage_eventlog.Name = "tabpage_eventlog"
	$tabpage_eventlog.Size = '1162, 111'
	$tabpage_eventlog.TabIndex = 4
	$tabpage_eventlog.Text = "Event Log"
	$arrTabPages += $tabpage_eventlog
	#
	# button_RebootHistory
	#
	$button_RebootHistory.Location = '163, 32'
	$button_RebootHistory.Name = "button_RebootHistory"
	$button_RebootHistory.Size = '149, 23'
	$button_RebootHistory.TabIndex = 5
	$button_RebootHistory.Text = "Reboot History (slow)"
	$button_RebootHistory.add_Click($button_RebootHistory_Click)
	$arrButtons += $button_RebootHistory
	#
	# button_mmcEvents
	#
	$button_mmcEvents.Location = '8, 3'
	$button_mmcEvents.Name = "button_mmcEvents"
	$button_mmcEvents.Size = '149, 23'
	$button_mmcEvents.TabIndex = 0
	$button_mmcEvents.Text = "MMC: Event Viewer"
	$button_mmcEvents.add_Click($button_mmcEvents_Click)
	$arrButtons += $button_mmcEvents
	#
	# button_EventsSearch
	#
	$button_EventsSearch.Location = '163, 3'
	$button_EventsSearch.Name = "button_EventsSearch"
	$button_EventsSearch.Size = '149, 23'
	$button_EventsSearch.TabIndex = 3
	$button_EventsSearch.Text = "Search (slow)"
	$button_EventsSearch.add_Click($button_EventsSearch_Click)
	$arrButtons += $button_EventsSearch
	#
	# button_EventsLogNames
	#
	$button_EventsLogNames.Location = '9, 61'
	$button_EventsLogNames.Name = "button_EventsLogNames"
	$button_EventsLogNames.Size = '148, 23'
	$button_EventsLogNames.TabIndex = 4
	$button_EventsLogNames.Text = "LogNames"
	$button_EventsLogNames.add_Click($button_EventsLogNames_Click)
	$arrButtons += $button_EventsLogNames
	#
	# button_EventsLast20
	#
	$button_EventsLast20.Location = '8, 32'
	$button_EventsLast20.Name = "button_EventsLast20"
	$button_EventsLast20.Size = '149, 23'
	$button_EventsLast20.TabIndex = 1
	$button_EventsLast20.Text = "Last20"
	$button_EventsLast20.add_Click($button_EventsLast20_Click)
	$arrButtons += $button_EventsLast20
	#
	# tabpage_ExternalTools
	#
	$tabpage_ExternalTools.Controls.Add($button_Rwinsta)
	$tabpage_ExternalTools.Controls.Add($button_Qwinsta)
	$tabpage_ExternalTools.Controls.Add($button_MsInfo32)
	$tabpage_ExternalTools.Controls.Add($button_Telnet)
	$tabpage_ExternalTools.Controls.Add($button_DriverQuery)
	$tabpage_ExternalTools.Controls.Add($button_SystemInfoexe)
	$tabpage_ExternalTools.Controls.Add($button_PAExec)
	$tabpage_ExternalTools.Controls.Add($button_psexec)
	$tabpage_ExternalTools.Controls.Add($textbox_networktracertparam)
	$tabpage_ExternalTools.Controls.Add($button_networkTracert)
	$tabpage_ExternalTools.Controls.Add($button_networkNsLookup)
	$tabpage_ExternalTools.Controls.Add($button_networkPing)
	$tabpage_ExternalTools.Controls.Add($textbox_networkpathpingparam)
	$tabpage_ExternalTools.Controls.Add($textbox_pingparam)
	$tabpage_ExternalTools.Controls.Add($button_networkPathPing)
	$tabpage_ExternalTools.Location = '4, 22'
	$tabpage_ExternalTools.Name = "tabpage_ExternalTools"
	$tabpage_ExternalTools.Size = '1162, 111'
	$tabpage_ExternalTools.TabIndex = 9
	$tabpage_ExternalTools.Text = "External Tools"
	$arrTabPages += $tabpage_ExternalTools
	#
	# groupbox3
	#
	$groupbox3.Controls.Add($label_SYDI)
	$groupbox3.Controls.Add($combobox_sydi_format)
	$groupbox3.Controls.Add($textbox_sydi_arguments)
	$groupbox3.Controls.Add($button_SYDIGo)
	$groupbox3.Location = '791, 3'
	$groupbox3.Name = "groupbox3"
	$groupbox3.Size = '268, 47'
	$groupbox3.TabIndex = 49
	$groupbox3.TabStop = $False
	$groupbox3.Text = "Script Your Documentation Instantly (.VBS)"
	$arrGroupboxes += $groupbox3
	#
	# label_SYDI
	#
	$label_SYDI.Location = '6, 25'
	$label_SYDI.Name = "label_SYDI"
	$label_SYDI.Size = '34, 19'
	$label_SYDI.TabIndex = 38
	$label_SYDI.Text = "SYDI"
	#
	# combobox_sydi_format
	#
	$combobox_sydi_format.BackColor = 'Info'
	$combobox_sydi_format.FormattingEnabled = $True
	[void]$combobox_sydi_format.Items.Add("DOC")
	[void]$combobox_sydi_format.Items.Add("XML")
	$combobox_sydi_format.Location = '46, 21'
	$combobox_sydi_format.Name = "combobox_sydi_format"
	$combobox_sydi_format.Size = '46, 21'
	$combobox_sydi_format.TabIndex = 37
	#
	# textbox_sydi_arguments
	#
	$textbox_sydi_arguments.BackColor = 'Info'
	$textbox_sydi_arguments.Font = "Microsoft Sans Serif, 8.25pt"
	$textbox_sydi_arguments.Location = '98, 21'
	$textbox_sydi_arguments.Name = "textbox_sydi_arguments"
	$textbox_sydi_arguments.Size = '106, 20'
	$textbox_sydi_arguments.TabIndex = 27
	$textbox_sydi_arguments.Text = "-wabefghipPqrsu -racdklp"
	#
	# button_SYDIGo
	#
	$button_SYDIGo.Location = '210, 20'
	$button_SYDIGo.Name = "button_SYDIGo"
	$button_SYDIGo.Size = '30, 23'
	$button_SYDIGo.TabIndex = 26
	$button_SYDIGo.Text = "Go"
	$button_SYDIGo.add_Click($button_SYDIGo_Click)
	$arrButtons += $button_SYDIGo
	#
	# button_Rwinsta
	#
	$button_Rwinsta.Location = '289, 27'
	$button_Rwinsta.Name = "button_Rwinsta"
	$button_Rwinsta.Size = '75, 23'
	$button_Rwinsta.TabIndex = 48
	$button_Rwinsta.Text = "Rwinsta"
	$button_Rwinsta.add_Click($button_Rwinsta_Click)
	$arrButtons += $button_Rwinsta
	#
	# button_Qwinsta
	#
	$button_Qwinsta.Location = '289, 3'
	$button_Qwinsta.Name = "button_Qwinsta"
	$button_Qwinsta.Size = '75, 23'
	$button_Qwinsta.TabIndex = 47
	$button_Qwinsta.Text = "Qwinsta"
	$button_Qwinsta.add_Click($button_Qwinsta_Click)
	$arrButtons += $button_Qwinsta
	#
	# button_MsInfo32
	#
	$button_MsInfo32.Location = '122, 52'
	$button_MsInfo32.Name = "button_MsInfo32"
	$button_MsInfo32.Size = '75, 23'
	$button_MsInfo32.TabIndex = 46
	$button_MsInfo32.Text = "MsInfo32"
	$button_MsInfo32.add_Click($button_MsInfo32_Click)
	$arrButtons += $button_MsInfo32
	#
	# button_Telnet
	#
	$button_Telnet.Location = '122, 76'
	$button_Telnet.Name = "button_Telnet"
	$button_Telnet.Size = '75, 23'
	$button_Telnet.TabIndex = 39
	$button_Telnet.Text = "Telnet"
	$button_Telnet.add_Click($button_Telnet_Click)
	$arrButtons += $button_Telnet
	#
	# button_DriverQuery
	#
	$button_DriverQuery.Location = '122, 27'
	$button_DriverQuery.Name = "button_DriverQuery"
	$button_DriverQuery.Size = '75, 23'
	$button_DriverQuery.TabIndex = 45
	$button_DriverQuery.Text = "DriverQuery"
	$button_DriverQuery.add_Click($button_DriverQuery_Click)
	$arrButtons += $button_DriverQuery
	#
	# button_SystemInfoexe
	#
	$button_SystemInfoexe.Location = '122, 3'
	$button_SystemInfoexe.Name = "button_SystemInfoexe"
	$button_SystemInfoexe.Size = '75, 23'
	$button_SystemInfoexe.TabIndex = 1
	$button_SystemInfoexe.Text = "SystemInfo"
	$button_SystemInfoexe.add_Click($button_SystemInfoexe_Click)
	$arrButtons += $button_SystemInfoexe
	#
	# button_PAExec
	#
	$button_PAExec.Location = '203, 27'
	$button_PAExec.Name = "button_PAExec"
	$button_PAExec.Size = '75, 23'
	$button_PAExec.TabIndex = 44
	$button_PAExec.Text = "PAExec"
	$button_PAExec.add_Click($button_PAExec_Click)
	$arrButtons += $button_PAExec
	#
	# button_psexec
	#
	$button_psexec.Location = '203, 3'
	$button_psexec.Name = "button_psexec"
	$button_psexec.Size = '75, 23'
	$button_psexec.TabIndex = 43
	$button_psexec.Text = "PsExec"
	$button_psexec.add_Click($button_psexec_Click)
	$arrButtons += $button_psexec
	#
	# textbox_networktracertparam
	#
	$textbox_networktracertparam.Location = '84, 78'
	$textbox_networktracertparam.Name = "textbox_networktracertparam"
	$textbox_networktracertparam.Size = '34, 20'
	$textbox_networktracertparam.TabIndex = 7
	$textbox_networktracertparam.Text = "-d"
	#
	# button_networkTracert
	#
	$button_networkTracert.Location = '3, 76'
	$button_networkTracert.Name = "button_networkTracert"
	$button_networkTracert.Size = '75, 23'
	$button_networkTracert.TabIndex = 6
	$button_networkTracert.Text = "Tracert"
	$button_networkTracert.add_Click($button_networkTracert_Click)
	$arrButtons += $button_networkTracert
	#
	# button_networkNsLookup
	#
	$button_networkNsLookup.Location = '3, 3'
	$button_networkNsLookup.Name = "button_networkNsLookup"
	$button_networkNsLookup.Size = '75, 23'
	$button_networkNsLookup.TabIndex = 5
	$button_networkNsLookup.Text = "NsLookup"
	$button_networkNsLookup.add_Click($button_networkNsLookup_Click)
	$arrButtons += $button_networkNsLookup
	#
	# button_networkPing
	#
	$button_networkPing.Location = '3, 27'
	$button_networkPing.Name = "button_networkPing"
	$button_networkPing.Size = '75, 23'
	$button_networkPing.TabIndex = 0
	$button_networkPing.Text = "Ping"
	$button_networkPing.add_Click($button_networkPing_Click)
	$arrButtons += $button_networkPing
	#
	# textbox_networkpathpingparam
	#
	$textbox_networkpathpingparam.Location = '84, 54'
	$textbox_networkpathpingparam.Name = "textbox_networkpathpingparam"
	$textbox_networkpathpingparam.Size = '34, 20'
	$textbox_networkpathpingparam.TabIndex = 3
	$textbox_networkpathpingparam.Text = "-n"
	#
	# textbox_pingparam
	#
	$textbox_pingparam.Location = '84, 29'
	$textbox_pingparam.Name = "textbox_pingparam"
	$textbox_pingparam.Size = '34, 20'
	$textbox_pingparam.TabIndex = 1
	$textbox_pingparam.Text = "-t"
	#
	# button_networkPathPing
	#
	$button_networkPathPing.Location = '3, 52'
	$button_networkPathPing.Name = "button_networkPathPing"
	$button_networkPathPing.Size = '75, 23'
	$button_networkPathPing.TabIndex = 2
	$button_networkPathPing.Text = "PathPing"
	$button_networkPathPing.add_Click($button_networkPathPing_Click)
	$arrButtons += $button_networkPathPing
	#
	# groupbox_ComputerName
	#
	$groupbox_ComputerName.Controls.Add($label_UptimeStatus)
    $groupbox_ComputerName.Controls.Add($label_CurrentUser)
	$groupbox_ComputerName.Controls.Add($textbox_computername)
	$groupbox_ComputerName.Controls.Add($label_OSStatus)
	$groupbox_ComputerName.Controls.Add($button_Check)
	$groupbox_ComputerName.Controls.Add($label_PingStatus)
	$groupbox_ComputerName.Controls.Add($label_Ping)
	$groupbox_ComputerName.Controls.Add($label_PSRemotingStatus)
	$groupbox_ComputerName.Controls.Add($label_Uptime)
	$groupbox_ComputerName.Controls.Add($label_User)
	$groupbox_ComputerName.Controls.Add($label_RDPStatus)
	$groupbox_ComputerName.Controls.Add($label_OS)
	$groupbox_ComputerName.Controls.Add($label_PermissionStatus)
	$groupbox_ComputerName.Controls.Add($label_Permission)
	$groupbox_ComputerName.Controls.Add($label_PSRemoting)
	$groupbox_ComputerName.Controls.Add($label_RDP)
	$groupbox_ComputerName.Dock = 'Top'
	$groupbox_ComputerName.Location = '0, 26'
	$groupbox_ComputerName.Name = "groupbox_ComputerName"
	$groupbox_ComputerName.Size = '1170, 61'
	$groupbox_ComputerName.TabIndex = 62
	$groupbox_ComputerName.TabStop = $False
	$arrGroupboxes += $groupbox_ComputerName
    #
	# textbox_computername
	#
	$textbox_computername.AutoCompleteMode = 'SuggestAppend'
	$textbox_computername.AutoCompleteSource = 'CustomSource'
	$textbox_computername.BackColor = 'LemonChiffon'
	$textbox_computername.BorderStyle = 'FixedSingle'
	$textbox_computername.CharacterCasing = 'Upper'
	$textbox_computername.Font = "Consolas, 18pt"
	$textbox_computername.ForeColor = 'WindowText'
	$textbox_computername.Location = '6, 14'
	$textbox_computername.Name = "textbox_computername"
	$textbox_computername.Size = '209, 36'
	$textbox_computername.TabIndex = 2
	$textbox_computername.Text = "LOCALHOST"
	$textbox_computername.TextAlign = 'Center'
	$tooltipinfo.SetToolTip($textbox_computername, "Please enter a Computer name")
	$textbox_computername.add_TextChanged($textbox_computername_TextChanged)
	$textbox_computername.add_KeyPress($textbox_computername_KeyPress)
	#
	# button_Check
	#
	$button_Check.Font = "Microsoft Sans Serif, 8.25pt, style=Bold"
	$button_Check.Image = [System.Drawing.Image]::FromFile("$PSScriptRoot\images\check.png")
	$button_Check.ImageAlign = 'MiddleLeft'
	$button_Check.Location = '220, 15'
	$button_Check.Name = "button_Check"
	$button_Check.Size = '88, 35'
	$button_Check.TabIndex = 51
	$button_Check.Text = "Check"
	$tooltipinfo.SetToolTip($button_Check, "Check the connectivity and basic information")
	$button_Check.add_Click{Get-ComputerInfo}
	$arrButtons += $button_Check
	#
	# label_Ping
	#
	$label_Ping.Font = "Trebuchet MS, 8.25pt, style=Underline"
	$label_Ping.Location = '315, 16'
	$label_Ping.Name = "label_Ping"
	$label_Ping.Size = '33, 16'
	$label_Ping.TabIndex = 49
	$label_Ping.Text = "Ping:"
	#
	# label_PingStatus
	#
	$label_PingStatus.Location = '380, 17'
	$label_PingStatus.Name = "label_PingStatus"
	$label_PingStatus.Size = '33, 16'
	$label_PingStatus.TabIndex = 50
	#
	# label_Permission
	#
	$label_Permission.Font = "Trebuchet MS, 8.25pt, style=Underline"
	$label_Permission.Location = '315, 32'
	$label_Permission.Name = "label_Permission"
	$label_Permission.Size = '72, 20'
	$label_Permission.TabIndex = 52
	$label_Permission.Text = "Permission:"
	#
	# label_PermissionStatus
	#
	$label_PermissionStatus.Location = '380, 33'
	$label_PermissionStatus.Name = "label_PermissionStatus"
	$label_PermissionStatus.Size = '33, 16'
	$label_PermissionStatus.TabIndex = 53
	#
	# label_RDP
	#
	$label_RDP.Font = "Trebuchet MS, 8.25pt, style=Underline"
	$label_RDP.Location = '410, 16'
	$label_RDP.Name = "label_RDP"
	$label_RDP.Size = '37, 20'
	$label_RDP.TabIndex = 54
	$label_RDP.Text = "RDP:"
	#
	# label_RDPStatus
	#
	$label_RDPStatus.Location = '470, 16'
	$label_RDPStatus.Name = "label_RDPStatus"
	$label_RDPStatus.Size = '69, 19'
	$label_RDPStatus.TabIndex = 56
	#
	# label_PSRemoting
	#
	$label_PSRemoting.Font = "Trebuchet MS, 8.25pt, style=Underline"
	$label_PSRemoting.Location = '410, 32'
	$label_PSRemoting.Name = "label_PSRemoting"
	$label_PSRemoting.Size = '75, 20'
	$label_PSRemoting.TabIndex = 55
	$label_PSRemoting.Text = "PSRemote:"
	#
	# label_PSRemotingStatus
	#
	$label_PSRemotingStatus.Location = '470, 33'
	$label_PSRemotingStatus.Name = "label_PSRemotingStatus"
	$label_PSRemotingStatus.Size = '69, 14'
	$label_PSRemotingStatus.TabIndex = 57
	#
	# label_OS
	#
	$label_OS.Font = "Trebuchet MS, 8.25pt, style=Underline"
	$label_OS.Location = '540, 16'
	$label_OS.Name = "label_OS"
	$label_OS.Size = '37, 20'
	$label_OS.TabIndex = 58
	$label_OS.Text = "OS:"
	#
	# label_OSStatus
	#
	$label_OSStatus.Location = '585, 16'
	$label_OSStatus.ForeColor = "LightBlue"
	$label_OSStatus.Name = "label_OSStatus"
	$label_OSStatus.Size = '509, 16'
	$label_OSStatus.TabIndex = 60
	#
	# label_Uptime
	#
	$label_Uptime.Font = "Trebuchet MS, 8.25pt, style=Underline"
	$label_Uptime.Location = '540, 32'
	$label_Uptime.Name = "label_Uptime"
	$label_Uptime.Size = '50, 20'
	$label_Uptime.TabIndex = 59
	$label_Uptime.Text = "Uptime:"
	#
	# label_UptimeStatus
	#
	$label_UptimeStatus.Location = '585, 33'
	$label_UptimeStatus.ForeColor = "LightBlue"
	$label_UptimeStatus.Name = "label_UptimeStatus"
	$label_UptimeStatus.Size = '255, 19'
	$label_UptimeStatus.TabIndex = 61
	#
    # label_User
    #
	$label_User.Font = "Trebuchet MS, 8.25pt, style=Underline"
	$label_User.Location = '840, 32'
	$label_User.Name = "label_User"
	$label_User.Size = '33, 16'
	$label_User.TabIndex = 64
	$label_User.Text = "User:"
	#
    # label_CurrentUser
    #
	$label_CurrentUser.Location = '870,33'
	$label_CurrentUser.ForeColor = "LightBlue"
    $label_CurrentUser.Name = "label_CurrentUser"
    $label_CurrentUser.Size = '239, 19'
	$label_CurrentUser.TabIndex = 65
	#
	# richtextbox_Logs
	#
	$richtextbox_Logs.BackColor = 'Black'
	$richtextbox_Logs.Dock = 'Bottom'
	$richtextbox_Logs.Font = "Consolas, 8.25pt"
	$richtextbox_Logs.ForeColor = '#88FF88'
	$richtextbox_Logs.Location = '0, 623'
	$richtextbox_Logs.Name = "richtextbox_Logs"
	$richtextbox_Logs.ReadOnly = $True
	$richtextbox_Logs.Size = '1170, 70'
	$richtextbox_Logs.TabIndex = 35
	$richtextbox_Logs.Text = ""
	$richtextbox_Logs.add_TextChanged($richtextbox_Logs_TextChanged)
	#
	# statusbar1
	#
	$statusbar1.Location = '0, 693'
	$statusbar1.Name = "statusbar1"
	$statusbar1.Size = '1170, 26'
	$statusbar1.TabIndex = 16
	#
	# menustrip_principal
	#
	$menustrip_principal.Font = "Trebuchet MS, 9pt"
	[void]$menustrip_principal.Items.Add($ToolStripMenuItem_AdminArsenal)
	[void]$menustrip_principal.Items.Add($ToolStripMenuItem_localhost)
	[void]$menustrip_principal.Items.Add($ToolStripMenuItem_scripts)
	[void]$menustrip_principal.Items.Add($ToolStripMenuItem_about)
	$menustrip_principal.Location = '0, 0'
	$menustrip_principal.Name = "menustrip_principal"
	$menustrip_principal.Size = '1170, 26'
	$menustrip_principal.TabIndex = 1
	$menustrip_principal.Text = "menustrip1"
	$arrToolStripItems += $menustrip_principal
	#
	# ToolStripMenuItem_AdminArsenal
	#
	[void]$ToolStripMenuItem_AdminArsenal.DropDownItems.Add($ToolStripMenuItem_ADSearchDialog)
	[void]$ToolStripMenuItem_AdminArsenal.DropDownItems.Add($ToolStripMenuItem_ADPrinters)
	[void]$ToolStripMenuItem_AdminArsenal.DropDownItems.Add($toolstripseparator4)
	[void]$ToolStripMenuItem_AdminArsenal.DropDownItems.Add($ToolStripMenuItem_CommandPrompt)
	[void]$ToolStripMenuItem_AdminArsenal.DropDownItems.Add($ToolStripMenuItem_Powershell)
	[void]$ToolStripMenuItem_AdminArsenal.DropDownItems.Add($ToolStripMenuItem_PowershellISE)
	[void]$ToolStripMenuItem_AdminArsenal.DropDownItems.Add($toolstripseparator5)
	[void]$ToolStripMenuItem_AdminArsenal.DropDownItems.Add($ToolStripMenuItem_RemoteDesktopConnection)
	[void]$ToolStripMenuItem_AdminArsenal.DropDownItems.Add($ToolStripMenuItem_shutdownGui)
	[void]$ToolStripMenuItem_AdminArsenal.DropDownItems.Add($ToolStripMenuItem_InternetExplorer)
	[void]$ToolStripMenuItem_AdminArsenal.DropDownItems.Add($ToolStripMenuItem_TerminalAdmin)
	[void]$ToolStripMenuItem_AdminArsenal.DropDownItems.Add($ToolStripMenuItem_Notepad)
	[void]$ToolStripMenuItem_AdminArsenal.DropDownItems.Add($ToolStripMenuItem_Wordpad)
	[void]$ToolStripMenuItem_AdminArsenal.DropDownItems.Add($ToolStripMenuItem_sysInternals)
	[void]$ToolStripMenuItem_AdminArsenal.DropDownItems.Add($ToolStripMenuItem_GeneratePassword)
	$ToolStripMenuItem_AdminArsenal.Image = [System.Drawing.Image]::FromFile("$PSScriptRoot\images\adminarsenal.png")
	$ToolStripMenuItem_AdminArsenal.Name = "ToolStripMenuItem_AdminArsenal"
	$ToolStripMenuItem_AdminArsenal.Size = '109, 22'
	$ToolStripMenuItem_AdminArsenal.Text = "AdminArsenal"
	$arrToolStripItems += $ToolStripMenuItem_AdminArsenal
	#
	# ToolStripMenuItem_CommandPrompt
	#
	$ToolStripMenuItem_CommandPrompt.Image = [System.Drawing.Image]::FromFile("$PSScriptRoot\images\menu_cmd.png")
	$ToolStripMenuItem_CommandPrompt.Name = "ToolStripMenuItem_CommandPrompt"
	$ToolStripMenuItem_CommandPrompt.Size = '290, 22'
	$ToolStripMenuItem_CommandPrompt.Text = "Command Prompt"
	$ToolStripMenuItem_CommandPrompt.add_Click{Start-Process cmd.exe}
	$arrToolStripItems += $ToolStripMenuItem_CommandPrompt
	#
	# ToolStripMenuItem_Powershell
	#
	$ToolStripMenuItem_Powershell.Image = [System.Drawing.Image]::FromFile("$PSScriptRoot\images\menu_powershell.png")
	$ToolStripMenuItem_Powershell.Name = "ToolStripMenuItem_Powershell"
	$ToolStripMenuItem_Powershell.Size = '290, 22'
	$ToolStripMenuItem_Powershell.Text = "Powershell"
	$ToolStripMenuItem_Powershell.add_Click{start-process powershell.exe -verb runas}
	$arrToolStripItems += $ToolStripMenuItem_Powershell
	#
	# ToolStripMenuItem_Notepad
	#
	$ToolStripMenuItem_Notepad.Image = [System.Drawing.Image]::FromFile("$PSScriptRoot\images\notepad.png")
	$ToolStripMenuItem_Notepad.Name = "ToolStripMenuItem_Notepad"
	$ToolStripMenuItem_Notepad.Size = '290, 22'
	$ToolStripMenuItem_Notepad.Text = "Notepad"
	$ToolStripMenuItem_Notepad.add_Click{Start-Process notepad.exe}
	$arrToolStripItems += $ToolStripMenuItem_Notepad
	#
	# ToolStripMenuItem_RemoteDesktopConnection
	#
	$ToolStripMenuItem_RemoteDesktopConnection.Image = [System.Drawing.Image]::FromFile("$PSScriptRoot\images\menu_rdp.png")
	$ToolStripMenuItem_RemoteDesktopConnection.Name = "ToolStripMenuItem_RemoteDesktopConnection"
	$ToolStripMenuItem_RemoteDesktopConnection.Size = '290, 22'
	$ToolStripMenuItem_RemoteDesktopConnection.Text = "Remote Desktop Connection"
	$ToolStripMenuItem_RemoteDesktopConnection.add_Click{Start-Process mstsc.exe}
	$arrToolStripItems += $ToolStripMenuItem_RemoteDesktopConnection
	#
	# ToolStripMenuItem_localhost
	#
	[void]$ToolStripMenuItem_localhost.DropDownItems.Add($ToolStripMenuItem_netstatsListening)
	[void]$ToolStripMenuItem_localhost.DropDownItems.Add($ToolStripMenuItem_registeredSnappins)
	[void]$ToolStripMenuItem_localhost.DropDownItems.Add($toolstripseparator1)
	[void]$ToolStripMenuItem_localhost.DropDownItems.Add($ToolStripMenuItem_mmc)
	[void]$ToolStripMenuItem_localhost.DropDownItems.Add($ToolStripMenuItem_compmgmt)
	[void]$ToolStripMenuItem_localhost.DropDownItems.Add($ToolStripMenuItem_services)
	[void]$ToolStripMenuItem_localhost.DropDownItems.Add($toolstripseparator3)
	[void]$ToolStripMenuItem_localhost.DropDownItems.Add($ToolStripMenuItem_systemproperties)
	[void]$ToolStripMenuItem_localhost.DropDownItems.Add($ToolStripMenuItem_devicemanager)
	[void]$ToolStripMenuItem_localhost.DropDownItems.Add($ToolStripMenuItem_taskManager)
	[void]$ToolStripMenuItem_localhost.DropDownItems.Add($ToolStripMenuItem_regedit)
	[void]$ToolStripMenuItem_localhost.DropDownItems.Add($ToolStripMenuItem_systemInformationMSinfo32exe)
	[void]$ToolStripMenuItem_localhost.DropDownItems.Add($ToolStripMenuItem_hostsFile)
	[void]$ToolStripMenuItem_localhost.DropDownItems.Add($ToolStripMenuItem_hostsFileGetContent)
	[void]$ToolStripMenuItem_localhost.DropDownItems.Add($ToolStripMenuItem_netstat)
	[void]$ToolStripMenuItem_localhost.DropDownItems.Add($ToolStripMenuItem_otherLocalTools)
	$ToolStripMenuItem_localhost.Image = [System.Drawing.Image]::FromFile("$PSScriptRoot\images\menu_computer.png")
	$ToolStripMenuItem_localhost.Name = "ToolStripMenuItem_localhost"
	$ToolStripMenuItem_localhost.Size = '90, 22'
	$ToolStripMenuItem_localhost.Text = "LocalHost"
	$arrToolStripItems += $ToolStripMenuItem_localhost
	#
	# ToolStripMenuItem_compmgmt
	#
	$ToolStripMenuItem_compmgmt.Name = "ToolStripMenuItem_compmgmt"
	$ToolStripMenuItem_compmgmt.Size = '278, 22'
	$ToolStripMenuItem_compmgmt.Text = "MMC - Computer Management"
	$ToolStripMenuItem_compmgmt.add_Click{compmgmt.msc}
	$arrToolStripItems += $ToolStripMenuItem_compmgmt
	#
	# ToolStripMenuItem_taskManager
	#
	$ToolStripMenuItem_taskManager.Name = "ToolStripMenuItem_taskManager"
	$ToolStripMenuItem_taskManager.Size = '278, 22'
	$ToolStripMenuItem_taskManager.Text = "Task Manager"
	$ToolStripMenuItem_taskManager.add_Click{Taskmgr}
	$arrToolStripItems += $ToolStripMenuItem_taskManager
	#
	# ToolStripMenuItem_services
	#
	$ToolStripMenuItem_services.Name = "ToolStripMenuItem_services"
	$ToolStripMenuItem_services.Size = '278, 22'
	$ToolStripMenuItem_services.Text = "MMC - Services"
	$ToolStripMenuItem_services.Image = [System.Drawing.Image]::FromFile("$PSScriptRoot\images\services.png")
	$ToolStripMenuItem_services.add_Click{services.msc}
	$arrToolStripItems += $ToolStripMenuItem_services
	#
	# ToolStripMenuItem_regedit
	#
	$ToolStripMenuItem_regedit.Name = "ToolStripMenuItem_regedit"
	$ToolStripMenuItem_regedit.Size = '278, 22'
	$ToolStripMenuItem_regedit.Text = "Regedit"
	$ToolStripMenuItem_regedit.add_Click{regedit}
	$arrToolStripItems += $ToolStripMenuItem_regedit
	#
	# ToolStripMenuItem_mmc
	#
	$ToolStripMenuItem_mmc.Name = "ToolStripMenuItem_mmc"
	$ToolStripMenuItem_mmc.Size = '278, 22'
	$ToolStripMenuItem_mmc.Text = "MMC.exe"
	$ToolStripMenuItem_mmc.Image = [System.Drawing.Image]::FromFile("$PSScriptRoot\images\toolbox.png")
	$ToolStripMenuItem_mmc.add_Click{mmc}
	$arrToolStripItems += $ToolStripMenuItem_mmc
	#
	# ToolStripMenuItem_shutdownGui
	#
	$ToolStripMenuItem_shutdownGui.Image = [System.Drawing.Image]::FromFile("$PSScriptRoot\images\menu_shutdown.png")
	$ToolStripMenuItem_shutdownGui.Name = "ToolStripMenuItem_shutdownGui"
	$ToolStripMenuItem_shutdownGui.Size = '290, 22'
	$ToolStripMenuItem_shutdownGui.Text = "Shutdown Gui"
	$ToolStripMenuItem_shutdownGui.add_Click{Start-Process shutdown.exe -ArgumentList /i}
	$arrToolStripItems += $ToolStripMenuItem_shutdownGui
	#
	# ToolStripMenuItem_registeredSnappins
	#
	$ToolStripMenuItem_registeredSnappins.Name = "ToolStripMenuItem_registeredSnappins"
	$ToolStripMenuItem_registeredSnappins.Size = '278, 22'
	$ToolStripMenuItem_registeredSnappins.Text = "Powershell - Get Registered Snappin"
	$ToolStripMenuItem_registeredSnappins.add_Click($ToolStripMenuItem_registeredSnappins_Click)
	$arrToolStripItems += $ToolStripMenuItem_registeredSnappins
	#
	# ToolStripMenuItem_about
	#
	[void]$ToolStripMenuItem_about.DropDownItems.Add($ToolStripMenuItem_AboutInfo)
	$ToolStripMenuItem_about.Image = [System.Drawing.Image]::FromFile("$PSScriptRoot\images\info.png")
	$ToolStripMenuItem_about.Name = "ToolStripMenuItem_about"
	$ToolStripMenuItem_about.Size = '69, 22'
	$ToolStripMenuItem_about.Text = "About"
	$arrToolStripItems += $ToolStripMenuItem_about
	#
	# ToolStripMenuItem_AboutInfo
	#
	$ToolStripMenuItem_AboutInfo.Name = "ToolStripMenuItem_AboutInfo"
	$ToolStripMenuItem_AboutInfo.Size = '211, 22'
	$ToolStripMenuItem_AboutInfo.Text = "About $ApplicationName"
	$ToolStripMenuItem_AboutInfo.add_Click($ToolStripMenuItem_AboutInfo_Click)
	$arrToolStripItems += $ToolStripMenuItem_AboutInfo
	#
	# contextmenustripServer
	#
	[void]$contextmenustripServer.Items.Add($ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_Tools)
	[void]$contextmenustripServer.Items.Add($ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_ConsolesMMC)
	$contextmenustripServer.Name = "contextmenustripServer"
	$contextmenustripServer.ShowImageMargin = $False
	$contextmenustripServer.Size = '79, 26'
	#
	# ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_Tools
	#
	[void]$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_Tools.DropDownItems.Add($ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_Ping)
	[void]$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_Tools.DropDownItems.Add($ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_RDP)
	[void]$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_Tools.DropDownItems.Add($ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_Qwinsta)
	[void]$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_Tools.DropDownItems.Add($ToolStripMenuItem_rwinsta)
	$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_Tools.Name = "ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_Tools"
	$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_Tools.Size = '78, 22'
	$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_Tools.Text = "Tools"
	#
	# ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_ConsolesMMC
	#
	[void]$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_ConsolesMMC.DropDownItems.Add($ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_compmgmt)
	[void]$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_ConsolesMMC.DropDownItems.Add($ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_services)
	[void]$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_ConsolesMMC.DropDownItems.Add($ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_eventvwr)
	$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_ConsolesMMC.Name = "ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_ConsolesMMC"
	$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_ConsolesMMC.Size = '130, 22'
	$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_ConsolesMMC.Text = "Consoles MMC"
	#
	# ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_Ping
	#
	$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_Ping.Name = "ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_Ping"
	$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_Ping.Size = '117, 22'
	$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_Ping.Text = "Ping"
	$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_Ping.add_Click{Start-Ping}
	#
	# ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_RDP
	#
	$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_RDP.Name = "ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_RDP"
	$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_RDP.Size = '117, 22'
	$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_RDP.Text = "RDP"
	$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_RDP.add_Click($button_remot_Click)
	#
	# ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_compmgmt
	#
	$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_compmgmt.Name = "ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_compmgmt"
	$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_compmgmt.Size = '202, 22'
	$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_compmgmt.Text = "Computer Management"
	$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_compmgmt.add_Click($button_mmcCompmgmt_Click)
	#
	# ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_services
	#
	$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_services.Name = "ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_services"
	$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_services.Size = '202, 22'
	$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_services.Text = "Services"
	$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_services.add_Click($button_mmcServices_Click)
	#
	# ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_eventvwr
	#
	$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_eventvwr.Name = "ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_eventvwr"
	$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_eventvwr.Size = '197, 22'
	$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_eventvwr.Text = "Events Viewer"
	$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_eventvwr.add_Click($button_mmcEvents_Click)
	#
	# ToolStripMenuItem_InternetExplorer
	#
	$ToolStripMenuItem_InternetExplorer.Image = [System.Drawing.Image]::FromFile("$PSScriptRoot\images\menu_ie.png")
	$ToolStripMenuItem_InternetExplorer.Name = "ToolStripMenuItem_InternetExplorer"
	$ToolStripMenuItem_InternetExplorer.Size = '290, 22'
	$ToolStripMenuItem_InternetExplorer.Text = "Internet Explorer"
	$ToolStripMenuItem_InternetExplorer.add_Click{Start-Process iexplore.exe}
	$arrToolStripItems += $ToolStripMenuItem_InternetExplorer
	#
	# ToolStripMenuItem_TerminalAdmin
	#
	$ToolStripMenuItem_TerminalAdmin.Image = [System.Drawing.Image]::FromFile("$PSScriptRoot\images\menu_tsadmin.png")
	$ToolStripMenuItem_TerminalAdmin.Name = "ToolStripMenuItem_TerminalAdmin"
	$ToolStripMenuItem_TerminalAdmin.Size = '290, 22'
	$ToolStripMenuItem_TerminalAdmin.Text = "Terminal Admin (TsAdmin)"
	$ToolStripMenuItem_TerminalAdmin.add_Click($ToolStripMenuItem_TerminalAdmin_Click)
	$arrToolStripItems += $ToolStripMenuItem_TerminalAdmin
	#
	# ToolStripMenuItem_ADSearchDialog
	#
	$ToolStripMenuItem_ADSearchDialog.Image = [System.Drawing.Image]::FromFile("$PSScriptRoot\images\menu_ad.png") #todo: replace this horrible icon
	$ToolStripMenuItem_ADSearchDialog.Name = "ToolStripMenuItem_ADSearchDialog"
	$ToolStripMenuItem_ADSearchDialog.Size = '290, 22'
	$ToolStripMenuItem_ADSearchDialog.Text = "Active Directory Query - Search Dialog"
	$ToolStripMenuItem_ADSearchDialog.add_Click($ToolStripMenuItem_ADSearchDialog_Click)
	$arrToolStripItems += $ToolStripMenuItem_ADSearchDialog
	#
	# ToolStripMenuItem_ADPrinters
	#
	$ToolStripMenuItem_ADPrinters.Image = [System.Drawing.Image]::FromFile("$PSScriptRoot\images\menu_printer.png")
	$ToolStripMenuItem_ADPrinters.Name = "ToolStripMenuItem_ADPrinters"
	$ToolStripMenuItem_ADPrinters.Size = '290, 22'
	$ToolStripMenuItem_ADPrinters.Text = "Active Directory Query - Printers"
	$ToolStripMenuItem_ADPrinters.add_Click($ToolStripMenuItem_ADPrinters_Click)
	$arrToolStripItems += $ToolStripMenuItem_ADPrinters
	#
	# ToolStripMenuItem_netstatsListening
	#
	$ToolStripMenuItem_netstatsListening.Name = "ToolStripMenuItem_netstatsListening"
	$ToolStripMenuItem_netstatsListening.Size = '278, 22'
	$ToolStripMenuItem_netstatsListening.Text = "Netstats | Listening ports"
	$ToolStripMenuItem_netstatsListening.add_Click($ToolStripMenuItem_netstatsListening_Click)
	$arrToolStripItems += $ToolStripMenuItem_netstatsListening
	#
	# ToolStripMenuItem_systemInformationMSinfo32exe
	#
	$ToolStripMenuItem_systemInformationMSinfo32exe.Name = "ToolStripMenuItem_systemInformationMSinfo32exe"
	$ToolStripMenuItem_systemInformationMSinfo32exe.Size = '278, 22'
	$ToolStripMenuItem_systemInformationMSinfo32exe.Text = "System Information (MSinfo32.exe)"
	$ToolStripMenuItem_systemInformationMSinfo32exe.add_Click($ToolStripMenuItem_systemInformationMSinfo32exe_Click)
	$arrToolStripItems += $ToolStripMenuItem_systemInformationMSinfo32exe
	#
	# ToolStripMenuItem_otherLocalTools
	#
	[void]$ToolStripMenuItem_otherLocalTools.DropDownItems.Add($ToolStripMenuItem_addRemovePrograms)
	[void]$ToolStripMenuItem_otherLocalTools.DropDownItems.Add($ToolStripMenuItem_addRemoveProgramsWindowsFeatures)
	[void]$ToolStripMenuItem_otherLocalTools.DropDownItems.Add($ToolStripMenuItem_administrativeTools)
	[void]$ToolStripMenuItem_otherLocalTools.DropDownItems.Add($ToolStripMenuItem_authprizationManager)
	[void]$ToolStripMenuItem_otherLocalTools.DropDownItems.Add($ToolStripMenuItem_certificateManager)
	[void]$ToolStripMenuItem_otherLocalTools.DropDownItems.Add($ToolStripMenuItem_componentServices)
	[void]$ToolStripMenuItem_otherLocalTools.DropDownItems.Add($ToolStripMenuItem_diskManagement)
	[void]$ToolStripMenuItem_otherLocalTools.DropDownItems.Add($ToolStripMenuItem_groupPolicyEditor)
	[void]$ToolStripMenuItem_otherLocalTools.DropDownItems.Add($ToolStripMenuItem_localSecuritySettings)
	[void]$ToolStripMenuItem_otherLocalTools.DropDownItems.Add($ToolStripMenuItem_localUsersAndGroups)
	[void]$ToolStripMenuItem_otherLocalTools.DropDownItems.Add($ToolStripMenuItem_networkConnections)
	[void]$ToolStripMenuItem_otherLocalTools.DropDownItems.Add($ToolStripMenuItem_performanceMonitor)
	[void]$ToolStripMenuItem_otherLocalTools.DropDownItems.Add($ToolStripMenuItem_sharedFolders)
	[void]$ToolStripMenuItem_otherLocalTools.DropDownItems.Add($ToolStripMenuItem_scheduledTasks)
	$ToolStripMenuItem_otherLocalTools.Name = "ToolStripMenuItem_otherLocalTools"
	$ToolStripMenuItem_otherLocalTools.Size = '278, 22'
	$ToolStripMenuItem_otherLocalTools.Text = "Other Windows Apps"
	$arrToolStripItems += $ToolStripMenuItem_otherLocalTools
	#
	# ToolStripMenuItem_addRemovePrograms
	#
	$ToolStripMenuItem_addRemovePrograms.Name = "ToolStripMenuItem_addRemovePrograms"
	$ToolStripMenuItem_addRemovePrograms.Size = '311, 22'
	$ToolStripMenuItem_addRemovePrograms.Text = "Add/Remove Programs"
	$ToolStripMenuItem_addRemovePrograms.add_Click($ToolStripMenuItem_addRemovePrograms_Click)
	$arrToolStripItems += $ToolStripMenuItem_addRemovePrograms
	#
	# ToolStripMenuItem_administrativeTools
	#
	$ToolStripMenuItem_administrativeTools.Name = "ToolStripMenuItem_administrativeTools"
	$ToolStripMenuItem_administrativeTools.Size = '311, 22'
	$ToolStripMenuItem_administrativeTools.Text = "Administrative Tools"
	$ToolStripMenuItem_administrativeTools.add_Click($ToolStripMenuItem_administrativeTools_Click)
	$arrToolStripItems += $ToolStripMenuItem_administrativeTools
	#
	# ToolStripMenuItem_authprizationManager
	#
	$ToolStripMenuItem_authprizationManager.Name = "ToolStripMenuItem_authprizationManager"
	$ToolStripMenuItem_authprizationManager.Size = '311, 22'
	$ToolStripMenuItem_authprizationManager.Text = "Authorization Manager"
	$arrToolStripItems += $ToolStripMenuItem_authprizationManager
	#
	# ToolStripMenuItem_certificateManager
	#
	$ToolStripMenuItem_certificateManager.Name = "ToolStripMenuItem_certificateManager"
	$ToolStripMenuItem_certificateManager.Size = '311, 22'
	$ToolStripMenuItem_certificateManager.Text = "Certificate Manager"
	$ToolStripMenuItem_certificateManager.add_Click($ToolStripMenuItem_certificateManager_Click)
	$arrToolStripItems += $ToolStripMenuItem_certificateManager
	#
	# ToolStripMenuItem_devicemanager
	#
	$ToolStripMenuItem_devicemanager.Name = "ToolStripMenuItem_devicemanager"
	$ToolStripMenuItem_devicemanager.Size = '278, 22'
	$ToolStripMenuItem_devicemanager.Text = "Device Manager"
	$ToolStripMenuItem_devicemanager.add_Click($ToolStripMenuItem_devicemanager_Click)
	$arrToolStripItems += $ToolStripMenuItem_devicemanager
	#
	# ToolStripMenuItem_addRemoveProgramsWindowsFeatures
	#
	$ToolStripMenuItem_addRemoveProgramsWindowsFeatures.Name = "ToolStripMenuItem_addRemoveProgramsWindowsFeatures"
	$ToolStripMenuItem_addRemoveProgramsWindowsFeatures.Size = '311, 22'
	$ToolStripMenuItem_addRemoveProgramsWindowsFeatures.Text = "Add/Remove Programs - Windows Features"
	$ToolStripMenuItem_addRemoveProgramsWindowsFeatures.add_Click($ToolStripMenuItem_addRemoveProgramsWindowsFeatures_Click)
	$arrToolStripItems += $ToolStripMenuItem_addRemoveProgramsWindowsFeatures
	#
	# toolstripseparator1
	#
	$toolstripseparator1.Name = "toolstripseparator1"
	$toolstripseparator1.Size = '275, 6'
	$arrToolStripItems += $toolstripseparator1
	#
	# toolstripseparator3
	#
	$toolstripseparator3.Name = "toolstripseparator3"
	$toolstripseparator3.Size = '275, 6'
	$arrToolStripItems += $toolstripseparator3
	#
	# ToolStripMenuItem_systemproperties
	#
	$ToolStripMenuItem_systemproperties.Name = "ToolStripMenuItem_systemproperties"
	$ToolStripMenuItem_systemproperties.Size = '278, 22'
	$ToolStripMenuItem_systemproperties.Text = "System Properties"
	$ToolStripMenuItem_systemproperties.add_Click($ToolStripMenuItem_systemproperties_Click)
	$arrToolStripItems += $ToolStripMenuItem_systemproperties
	#
	# toolstripseparator4
	#
	$toolstripseparator4.Name = "toolstripseparator4"
	$toolstripseparator4.Size = '287, 6'
	$arrToolStripItems += $toolstripseparator4
	#
	# toolstripseparator5
	#
	$toolstripseparator5.Name = "toolstripseparator5"
	$toolstripseparator5.Size = '287, 6'
	$arrToolStripItems += $toolstripseparator5
	#
	# ToolStripMenuItem_Wordpad
	#
	$ToolStripMenuItem_Wordpad.Image = [System.Drawing.Image]::FromFile("$PSScriptRoot\images\rtf.png")
	$ToolStripMenuItem_Wordpad.Name = "ToolStripMenuItem_Wordpad"
	$ToolStripMenuItem_Wordpad.Size = '290, 22'
	$ToolStripMenuItem_Wordpad.Text = "Wordpad"
	$ToolStripMenuItem_Wordpad.add_Click($ToolStripMenuItem_Wordpad_Click)
	$arrToolStripItems += $ToolStripMenuItem_Wordpad
	#
	# ToolStripMenuItem_sharedFolders
	#
	$ToolStripMenuItem_sharedFolders.Name = "ToolStripMenuItem_sharedFolders"
	$ToolStripMenuItem_sharedFolders.Size = '311, 22'
	$ToolStripMenuItem_sharedFolders.Text = "Shared Folders"
	$ToolStripMenuItem_sharedFolders.add_Click($ToolStripMenuItem_sharedFolders_Click)
	$arrToolStripItems += $ToolStripMenuItem_sharedFolders
	#
	# ToolStripMenuItem_performanceMonitor
	#
	$ToolStripMenuItem_performanceMonitor.Name = "ToolStripMenuItem_performanceMonitor"
	$ToolStripMenuItem_performanceMonitor.Size = '311, 22'
	$ToolStripMenuItem_performanceMonitor.Text = "Performance Monitor"
	$ToolStripMenuItem_performanceMonitor.add_Click($ToolStripMenuItem_performanceMonitor_Click)
	$arrToolStripItems += $ToolStripMenuItem_performanceMonitor
	#
	# ToolStripMenuItem_networkConnections
	#
	$ToolStripMenuItem_networkConnections.Name = "ToolStripMenuItem_networkConnections"
	$ToolStripMenuItem_networkConnections.Size = '311, 22'
	$ToolStripMenuItem_networkConnections.Text = "Network Connections"
	$ToolStripMenuItem_networkConnections.add_Click($ToolStripMenuItem_networkConnections_Click)
	$arrToolStripItems += $ToolStripMenuItem_networkConnections
	#
	# ToolStripMenuItem_groupPolicyEditor
	#
	$ToolStripMenuItem_groupPolicyEditor.Name = "ToolStripMenuItem_groupPolicyEditor"
	$ToolStripMenuItem_groupPolicyEditor.Size = '311, 22'
	$ToolStripMenuItem_groupPolicyEditor.Text = "Group Policy Editor (local)"
	$ToolStripMenuItem_groupPolicyEditor.add_Click($ToolStripMenuItem_groupPolicyEditor_Click)
	$arrToolStripItems += $ToolStripMenuItem_groupPolicyEditor
	#
	# ToolStripMenuItem_localUsersAndGroups
	#
	$ToolStripMenuItem_localUsersAndGroups.Name = "ToolStripMenuItem_localUsersAndGroups"
	$ToolStripMenuItem_localUsersAndGroups.Size = '311, 22'
	$ToolStripMenuItem_localUsersAndGroups.Text = "Local Users and Groups"
	$ToolStripMenuItem_localUsersAndGroups.add_Click($ToolStripMenuItem_localUsersAndGroups_Click)
	$arrToolStripItems += $ToolStripMenuItem_localUsersAndGroups
	#
	# ToolStripMenuItem_diskManagement
	#
	$ToolStripMenuItem_diskManagement.Name = "ToolStripMenuItem_diskManagement"
	$ToolStripMenuItem_diskManagement.Size = '311, 22'
	$ToolStripMenuItem_diskManagement.Text = "Disk Management"
	$ToolStripMenuItem_diskManagement.add_Click($ToolStripMenuItem_diskManagement_Click)
	$arrToolStripItems += $ToolStripMenuItem_diskManagement
	#
	# ToolStripMenuItem_localSecuritySettings
	#
	$ToolStripMenuItem_localSecuritySettings.Name = "ToolStripMenuItem_localSecuritySettings"
	$ToolStripMenuItem_localSecuritySettings.Size = '311, 22'
	$ToolStripMenuItem_localSecuritySettings.Text = "Local Security Settings"
	$ToolStripMenuItem_localSecuritySettings.add_Click($ToolStripMenuItem_localSecuritySettings_Click)
	$arrToolStripItems += $ToolStripMenuItem_localSecuritySettings
	#
	# ToolStripMenuItem_componentServices
	#
	$ToolStripMenuItem_componentServices.Name = "ToolStripMenuItem_componentServices"
	$ToolStripMenuItem_componentServices.Size = '311, 22'
	$ToolStripMenuItem_componentServices.Text = "Component Services"
	$ToolStripMenuItem_componentServices.add_Click($ToolStripMenuItem_componentServices_Click)
	$arrToolStripItems += $ToolStripMenuItem_componentServices
	#
	# ToolStripMenuItem_scheduledTasks
	#
	$ToolStripMenuItem_scheduledTasks.Name = "ToolStripMenuItem_scheduledTasks"
	$ToolStripMenuItem_scheduledTasks.Size = '311, 22'
	$ToolStripMenuItem_scheduledTasks.Text = "Scheduled Tasks"
	$ToolStripMenuItem_scheduledTasks.add_Click($ToolStripMenuItem_scheduledTasks_Click)
	$arrToolStripItems += $ToolStripMenuItem_scheduledTasks
	#
	# ToolStripMenuItem_PowershellISE
	#
	$ToolStripMenuItem_PowershellISE.Image = [System.Drawing.Image]::FromFile("$PSScriptRoot\images\menu_powershell_ise.png")
	$ToolStripMenuItem_PowershellISE.Name = "ToolStripMenuItem_PowershellISE"
	$ToolStripMenuItem_PowershellISE.Size = '290, 22'
	$ToolStripMenuItem_PowershellISE.Text = "Powershell ISE"
	$ToolStripMenuItem_PowershellISE.add_Click($ToolStripMenuItem_PowershellISE_Click)
	$arrToolStripItems += $ToolStripMenuItem_PowershellISE
	#
	# ToolStripMenuItem_hostsFile
	#
	$ToolStripMenuItem_hostsFile.Name = "ToolStripMenuItem_hostsFile"
	$ToolStripMenuItem_hostsFile.Size = '278, 22'
	$ToolStripMenuItem_hostsFile.Text = "Hosts File (Open)"
	$ToolStripMenuItem_hostsFile.add_Click($ToolStripMenuItem_hostsFile_Click)
	$arrToolStripItems += $ToolStripMenuItem_hostsFile
	#
	# ToolStripMenuItem_netstat
	#
	$ToolStripMenuItem_netstat.Name = "ToolStripMenuItem_netstat"
	$ToolStripMenuItem_netstat.Size = '278, 22'
	$ToolStripMenuItem_netstat.Text = "NetStat"
	$arrToolStripItems += $ToolStripMenuItem_netstat
	#
	# errorprovider1
	#
	$errorprovider1.ContainerControl = $form_MainForm
	#
	# tooltipinfo
	#
	#
	# ToolStripMenuItem_sysInternals
	#
	[void]$ToolStripMenuItem_sysInternals.DropDownItems.Add($ToolStripMenuItem_adExplorer)
	$ToolStripMenuItem_sysInternals.Image = [System.Drawing.Image]::FromFile("$PSScriptRoot\images\toolbox.png")
	$ToolStripMenuItem_sysInternals.Name = "ToolStripMenuItem_sysInternals"
	$ToolStripMenuItem_sysInternals.Size = '290, 22'
	$ToolStripMenuItem_sysInternals.Text = "SysInternals"
	$arrToolStripItems += $ToolStripMenuItem_sysInternals
	#
	# ToolStripMenuItem_adExplorer
	#
	$ToolStripMenuItem_adExplorer.Name = "ToolStripMenuItem_adExplorer"
	$ToolStripMenuItem_adExplorer.Size = '152, 22'
	$ToolStripMenuItem_adExplorer.Text = "AdExplorer"
	$ToolStripMenuItem_adExplorer.add_Click($ToolStripMenuItem_adExplorer_Click)
	$arrToolStripItems += $ToolStripMenuItem_adExplorer
	#
	# ToolStripMenuItem_hostsFileGetContent
	#
	$ToolStripMenuItem_hostsFileGetContent.Name = "ToolStripMenuItem_hostsFileGetContent"
	$ToolStripMenuItem_hostsFileGetContent.Size = '278, 22'
	$ToolStripMenuItem_hostsFileGetContent.Text = "Hosts File (Get content)"
	$ToolStripMenuItem_hostsFileGetContent.add_Click($ToolStripMenuItem_hostsFileGetContent_Click)
	$arrToolStripItems += $ToolStripMenuItem_hostsFileGetContent
	#
	# ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_Qwinsta
	#
	$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_Qwinsta.Name = "ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_Qwinsta"
	$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_Qwinsta.Size = '117, 22'
	$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_Qwinsta.Text = "Qwinsta"
	$ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_Qwinsta.add_Click($button_Qwinsta_Click)
	$arrToolStripItems += $ContextMenuStripItem_consoleToolStripMenuItem_ComputerName_Qwinsta
	#
	# ToolStripMenuItem_rwinsta
	#
	$ToolStripMenuItem_rwinsta.Name = "ToolStripMenuItem_rwinsta"
	$ToolStripMenuItem_rwinsta.Size = '152, 22'
	$ToolStripMenuItem_rwinsta.Text = "Rwinsta"
	$ToolStripMenuItem_rwinsta.add_Click($button_Rwinsta_Click)
	$arrToolStripItems += $ToolStripMenuItem_rwinsta
	#
	# ToolStripMenuItem_GeneratePassword
	#
	$ToolStripMenuItem_GeneratePassword.Image = [System.Drawing.Image]::FromFile("$PSScriptRoot\images\menu_password.png")
	$ToolStripMenuItem_GeneratePassword.Name = "ToolStripMenuItem_GeneratePassword"
	$ToolStripMenuItem_GeneratePassword.Size = '290, 22'
	$ToolStripMenuItem_GeneratePassword.Text = "Generate a password"
	$ToolStripMenuItem_GeneratePassword.add_Click($button_PasswordGen_Click)
	$arrToolStripItems += $ToolStripMenuItem_GeneratePassword
	#
	# ToolStripMenuItem_scripts
	#
	[void]$ToolStripMenuItem_scripts.DropDownItems.Add($ToolStripMenuItem_WMIExplorer)
    [void]$ToolStripMenuItem_scripts.DropDownItems.Add($ToolStripMenuItem_ADUSERUnlocker)
	$ToolStripMenuItem_scripts.Image = [System.Drawing.Image]::FromFile("$PSScriptRoot\images\menu_script.png")
	$ToolStripMenuItem_scripts.Name = "ToolStripMenuItem_scripts"
	$ToolStripMenuItem_scripts.Size = '74, 22'
	$ToolStripMenuItem_scripts.Text = "Scripts"
	$arrToolStripItems += $ToolStripMenuItem_scripts
	#
	# ToolStripMenuItem_WMIExplorer
	#
	$ToolStripMenuItem_WMIExplorer.Name = "ToolStripMenuItem_WMIExplorer"
	$ToolStripMenuItem_WMIExplorer.Size = '152, 22'
	$ToolStripMenuItem_WMIExplorer.Text = "WMI Explorer"
	$ToolStripMenuItem_WMIExplorer.add_Click($ToolStripMenuItem_WMIExplorer_Click)
	$arrToolStripItems += $ToolStripMenuItem_WMIExplorer
    #
	# ToolStripMenuItem_ADUSERUnlocker
	#
	$ToolStripMenuItem_ADUSERUnlocker.Name = "ToolStripMenuItem_ADUSERUnlocker"
	$ToolStripMenuItem_ADUSERUnlocker.Size = '152, 22'
	$ToolStripMenuItem_ADUSERUnlocker.Text = "ADUSERUnlocker"
	$ToolStripMenuItem_ADUSERUnlocker.add_Click($ToolStripMenuItem_ADUSERUnlocker_Click)
	$arrToolStripItems += $ToolStripMenuItem_ADUSERUnlocker
	#
	# timerCheckJob
	#
	$timerCheckJob.add_Tick($timerCheckJob_Tick2)
	#endregion Generated Form Code
	
	#
	#region Theme
	#
	$Colour_Background = "#1d1d1d"
	$Colour_Text = "White"
	
	# Configure the buttons that live in the toolbar on the general page
	$i = 0
	Foreach ($Button in $arrToolbarButtons) {
		# Determine the X coordinate for the button
		$x = 3 + $i * 74
		$Button.ImageAlign = 'TopCenter'
		$Button.TextAlign = 'BottomCenter'
		$Button.TabIndex = Get-TabIndex
		$Button.Location = "$x, 4"
		$Button.Size = '74, 77'
		$Button.Font = "Microsoft Sans Serif, 8.25pt"
		# Add buttons to the correct panel
		$tabpage_general.Controls.Add($Button)
		# Add to Parent button array
		$arrButtons += $Button
		$i++
	}

	# Configure the buttons that appear just below the output window
	$i = 0
	Foreach ($Button in $arrOutputButtons) {
		$x = 1 + $i * 45
		$Button.TabIndex = Get-TabIndex
		$Button.Location = "$x, 0"
		$Button.Size = '44, 31'
		# Add buttons to the correct panel
		$panel_RTBButtons.Controls.Add($Button)
		# Add to Parent button array
		$arrButtons += $Button
		$i++
	}

	# Buttons on the network page
	# Avoid dividing by zero
	$i = 1
	foreach ($Button in $arrTabPageNetworkButtons) {
		# Create two columns, for each button alternate between them, setting size and position
		switch ($i % 2) {
			#even
			0 {
				$s_x = 145
				$l_x = 187
				$l_y = 3 + ($i / 2 - 1) * 29
			}
			#odd
			default {
				$s_x = 123
				$l_x = 8
				$l_y = 3 + (($i - 1) / 2) * 29
			}
		}
		$Button.TabIndex = Get-TabIndex
		$Button.Location = "$l_x, $l_y"
		$Button.Size = "$s_x, 23"
		$tabpage_network.Controls.Add($Button)
		$arrButtons += $Button
		$i++
	}
	
	# Applying style to all buttons
	Foreach ($Button in $arrButtons) {
		$Button.FlatStyle = [System.Windows.Forms.FlatStyle]::Flat
		$Button.FlatAppearance.BorderColor = "#666666"
		$Button.FlatAppearance.MouseOverBackColor = "#666666"
		$Button.BackColor = $Colour_BackGround
		$Button.ForeColor = $Colour_Text
	}
	#endregion Theme

	# Applying styling to all tabpages
	$tabcontrol_computer.ForeColor = "White"
	Foreach ($Page in $arrTabPages) {
		$Page.BackColor = "Black"
		$Page.ForeColor = "White"
		$tabcontrol_computer.Controls.Add($Page)
	}

	# Applying styling to all panels
	Foreach ($Panel in $arrPanels) {
		$Panel.BackColor = "Black"
		$Panel.ForeColor = "White"
	}

	# Applying Styling to all Group Boxes
	Foreach ($Groupbox in $arrGroupboxes) {
		$Groupbox.BackColor = "Black"
		$Groupbox.ForeColor = "White"
	}

	# Applying Styling to all toolstrips

	Foreach ($item in $arrToolStripItems) {
		$item.BackColor = $Colour_Background
		$item.ForeColor = $Colour_Text
	}
	#----------------------------------------------

	#Save the initial state of the form
	$InitialFormWindowState = $form_MainForm.WindowState
	#Init the OnLoad event to correct the initial state of the form
	$form_MainForm.add_Load($Form_StateCorrection_Load)
	#Clean up the control events
	$form_MainForm.add_FormClosed($Form_Cleanup_FormClosed)
	#Store the control values when form is closing
	$form_MainForm.add_Closing($Form_StoreValues_Closing)
	#Show the Form
	return $form_MainForm.ShowDialog()

}
#endregion

#Start the application
Main ($CommandLine)